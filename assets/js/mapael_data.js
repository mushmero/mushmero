function loadmap(){
    var countries = {"AF":"Afghanistan","ZA":"South Africa","AL":"Albania","DZ":"Algeria","DE":"Germany","AD":"Andorra","AO":"Angola","AG":"Antigua And Barbuda","SA":"Saudi Arabia","AR":"Argentina","AM":"Armenia","AU":"Australia","AT":"Austria","AZ":"Azerbaijan","BS":"Bahamas","BH":"Bahrain","BD":"Bangladesh","BB":"Barbados","BE":"Belgium","BZ":"Belize","BJ":"Benin","BT":"Bhutan","BY":"Belarus","MM":"Myanmar","BO":"Bolivia, Plurinational State Of","BA":"Bosnia And Herzegovina","BW":"Botswana","BR":"Brazil","BN":"Brunei Darussalam","BG":"Bulgaria","BF":"Burkina Faso","BI":"Burundi","KH":"Cambodia","CM":"Cameroon","CA":"Canada","CV":"Cape Verde","CF":"Central African Republic","CL":"Chile","CN":"China","CY":"Cyprus","CO":"Colombia","KM":"Comoros","CG":"Congo","CD":"Congo, The Democratic Republic Of The","KP":"Korea, Democratic People's Republic Of","KR":"Korea, Republic Of","CR":"Costa Rica","CI":"C\u00d4te D'ivoire","HR":"Croatia","CU":"Cuba","DK":"Denmark","DJ":"Djibouti","DM":"Dominica","EG":"Egypt","AE":"United Arab Emirates","EC":"Ecuador","ER":"Eritrea","ES":"Spain","EE":"Estonia","US":"United States","ET":"Ethiopia","FJ":"Fiji","FI":"Finland","FR":"France","GA":"Gabon","GM":"Gambia","GE":"Georgia","GH":"Ghana","GR":"Greece","GD":"Grenada","GT":"Guatemala","GN":"Guinea","GQ":"Equatorial Guinea","GW":"Guinea-bissau","GY":"Guyana","HT":"Haiti","HN":"Honduras","HU":"Hungary","JM":"Jamaica","JP":"Japan","MH":"Marshall Islands","PW":"Palau","SB":"Solomon Islands","IN":"India","ID":"Indonesia","JO":"Jordan","IR":"Iran, Islamic Republic Of","IQ":"Iraq","IE":"Ireland","IS":"Iceland","IL":"Israel","IT":"Italy","KZ":"Kazakhstan","KE":"Kenya","KG":"Kyrgyzstan","KI":"Kiribati","KW":"Kuwait","LA":"Lao People's Democratic Republic","LS":"Lesotho","LV":"Latvia","LB":"Lebanon","LR":"Liberia","LY":"Libya","LI":"Liechtenstein","LT":"Lithuania","LU":"Luxembourg","MK":"Macedonia, The Former Yugoslav Republic Of","MG":"Madagascar","MY":"Malaysia","MW":"Malawi","MV":"Maldives","ML":"Mali","MT":"Malta","MA":"Morocco","MU":"Mauritius","MR":"Mauritania","MX":"Mexico","FM":"Micronesia, Federated States Of","MD":"Moldova, Republic Of","MC":"Monaco","MN":"Mongolia","ME":"Montenegro","MZ":"Mozambique","NA":"Namibia","NP":"Nepal","NI":"Nicaragua","NE":"Niger","NG":"Nigeria","NO":"Norway","NZ":"New Zealand","OM":"Oman","UG":"Uganda","UZ":"Uzbekistan","PK":"Pakistan","PS":"Palestine, State Of","PA":"Panama","PG":"Papua New Guinea","PY":"Paraguay","NL":"Netherlands","PE":"Peru","PH":"Philippines","PL":"Poland","PT":"Portugal","QA":"Qatar","DO":"Dominican Republic","RO":"Romania","GB":"United Kingdom","RU":"Russian Federation","RW":"Rwanda","KN":"Saint Kitts And Nevis","SM":"San Marino","VC":"Saint Vincent And The Grenadines","LC":"Saint Lucia","SV":"El Salvador","WS":"Samoa","ST":"Sao Tome And Principe","SN":"Senegal","RS":"Serbia","SC":"Seychelles","SL":"Sierra Leone","SG":"Singapore","SK":"Slovakia","SI":"Slovenia","SO":"Somalia","SD":"Sudan","SS":"South Sudan","LK":"Sri Lanka","SE":"Sweden","CH":"Switzerland","SR":"Suriname","SZ":"Swaziland","SY":"Syrian Arab Republic","TJ":"Tajikistan","TZ":"Tanzania, United Republic Of","TD":"Chad","CZ":"Czech Republic","TH":"Thailand","TL":"Timor-leste","TG":"Togo","TO":"Tonga","TT":"Trinidad And Tobago","TN":"Tunisia","TM":"Turkmenistan","TR":"Turkey","TV":"Tuvalu","TW":"Taiwan","VU":"Vanuatu","VE":"Venezuela, Bolivarian Republic Of","VN":"Viet Nam","UA":"Ukraine","UY":"Uruguay","YE":"Yemen","ZM":"Zambia","ZW":"Zimbabwe","GL":"Greenland","NC":"New Caledonia","FK":"Falklan Island","PR":"Puerto Rico","EH":"Western Sahara"};

    var areas = {};
    var visitordataurl = "admin/visitordata";
    var visitorinfo = "admin/visitorinfo";    
    var plots = {};
    var plotdata;
    var info;
    var plotslices = {};
    var areaslices = {};
    
    $.ajax({
        url: visitordataurl,
        async: false,
        datatype: 'json',
        success: function(data){
            plotdata = JSON.parse(data);
        }
    });
    $.ajax({
        url: visitorinfo,
        async: false,
        datatype: 'json',
        success: function(data){
            info = JSON.parse(data);
        }
    });
    $.each(plotdata, function(id,elem){
        var plot = {};
        plot.latitude = elem.latitude;
        plot.longitude = elem.longitude;
        plot.value = elem.visitor;
        plot.tooltip = {
            content: elem.country+"<br>Visitor: "+elem.visitor
        }
        plots[elem.countrycode] = plot;
    });
    // $.each(plotdata, function(id,elem){
    //     var area = {};
    //     area.value = elem.visitor;
    //     area.tooltip = {
    //         content: elem.country+"<br>Visitor: "+elem.visitor
    //     };
    //     areas[elem.countrycode] = area;
    // });
    var visitorvalue = info[0]['visitor'];
    // var minplotslice = {};
    // var betweenplotslice = {};
    // var maxsplotlice = {};
    //     minplotslice.max = 50;
    //     minplotslice.attrs = {fill: "rgba(255,48,0,0.2)"};
    //     minplotslice.attrsHover = {transform: "s1.5","stroke-width":1};
    //     minplotslice.label = "< 50";
    //     minplotslice.size = 10;

    //     betweenplotslice.min = 50;
    //     betweenplotslice.max = 100;
    //     betweenplotslice.attrs = {fill: "rgba(255,48,0,0.5)"};
    //     betweenplotslice.attrsHover = {transform: "s1.5", "stroke-width": 1};
    //     betweenplotslice.label = "> 50 and < 100";
    //     betweenplotslice.size = 20;

    //     maxsplotlice.min = 100;
    //     maxsplotlice.attrs = {fill: "rgba(255,48,0,0.8)"};
    //     maxsplotlice.attrsHover = {transform: "s1.5","stroke-width":1};
    //     maxsplotlice.label = "> 100";
    //     maxsplotlice.size = 30;
    //     plotslices = [minplotslice,betweenplotslice,maxsplotlice];

var nullareaslice = {};
var minareaslice = {};
var betweenareaslice = {};
var maxareaslice = {};
    nullareaslice.max = 0;
    nullareaslice.attrs = {fill: "rgba(0,0,0,0.5)"};
    nullareaslice.label = "0";

    minareaslice.mix = 1;
    minareaslice.max = 500;
    minareaslice.attrs = {fill: "#00E5FF"};
    minareaslice.label = "< 500";

    betweenareaslice.min = 500;
    betweenareaslice.max = 1500;
    betweenareaslice.attrs = {fill: "#FFC400"};
    betweenareaslice.label = "> 500 and < 1500";

    maxareaslice.min = 1500;
    maxareaslice.attrs = {fill: "#64DD17"};
    maxareaslice.label = "> 1500";

    areaslices = [nullareaslice, minareaslice, betweenareaslice, maxareaslice];
var plotlegend = {
    mode: "horizontal",
    title: "Visitors",
    labelAttrs: {
        "font-size": 10
    },
    marginLeft: 5,
    marginLeftLabel: 5,
    slices: plotslices,
};

var arealegend = {
    display: true,
    mode: "horizontal",
    title: "Visitors",
    labelAttrs: {
        "font-size": 10
    },
    marginLeft: 5,
    marginLeftLabel: 5,
    slices: areaslices,
};
var pdata = {};
$.each(plotdata, function(key,data){
    var pd = {};
    pd.country = data.country;
    pd.value = data.visitor;
    pdata[data.countrycode] = pd;
});
$.each(countries, function(id, elem){
    if(id in pdata){
        var v = pdata[id].value;
        var c = pdata[id].country;
        areas[id] = {
            value: v,
            tooltip : {
                content: c+"<br>Visitor: "+v,
            },
        }; 
    }else{
        areas[id] = {
            value: 0,
            tooltip : {
                content: elem+"<br>Visitor: 0",
            },
        }
    }
});
    $(".worldmap").mapael({
        map: {
            // Set the name of the map to display
            name: "world_countries",
            zoom: {
                enabled: true,
                maxLevel: 10
            },
        },
        areas: areas,
        legend: {
            area : arealegend
        },
        // plots: plots,
    });
}

$(document).ready(function(){
    $.ajax({
        url: 'login/checklogin',
        success: function(data){
            if(data == 1){
                loadmap();
                setInterval(function(){
                    loadmap();
                },300000);
            }
        },
    });    
});
