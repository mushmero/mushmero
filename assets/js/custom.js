/* Social Add Button */
/*$(function() {
    var max = 5;
    var x = 1;
    $('.add-btn').click(function(e) {
        e.preventDefault();
        if (x < max) {
            x++;
            var html = "<div class=\"row additional\"> <div class=\"col-lg-3\"> <label for=\"socialname\"> Socials </label> <input type=\"text\" class=\"form-control\" id=\"socialname\" name=\"socialname[]\" placeholder=\"Enter socials name\"> </div> <div class=\"col-lg-3\"> <label for=\"sociallink\"> Socials Link </label> <input type=\"text\" class=\"form-control\" id=\"sociallink\" name=\"sociallink[]\" placeholder=\"Enter socials link including http:// or https://\"> </div> <div class=\"col-lg-3\"> <label for=\"socialicon\"> Socials Icon </label> <select class=\"form-control\" id=\"socialicon\" name=\"socialicon[]\"> <option value=\"-\" class=\"selected\"> Select Icon </option>"'<?php if(!empty($socialicon)){ ?> <?php foreach($socialicon as $si){ ?>'" <option value=\""'<?php echo $si->id; ?>'"\"> "'<?php echo $si->iconname; ?>'" </option> "'<?php } ?> <?php } ?>'" </select> </div> <div class=\"col-lg-1 mt-4 pt-2\"> <div class=\"float-right\"> <a href=\"#\" class=\"btn btn-danger btn-flat remove-btn\"> <i class=\"fas fa-times\"> </i></a> </div> </div> </div>";
            $('.social-wrapper').append(html);
        }
    });
    $('.social-wrapper').on("click", ".remove-btn", function(e) {
        e.preventDefault();
        $(this).closest('div.additional').remove();
        x--;
    })
});*/
/* Education Add Button */
/*$(function() {
    var max = 5;
    var x = 1;
    $('.add-btn').click(function(e) {
        e.preventDefault();
        if (x < max) {
            x++;
            $('.edu-wrapper').append(`<div class="row additional">
                      <div class="col-lg-2">
                          <label for="eduname">Education Name</label>
                          <input type="text" class="form-control" id="eduname" name="eduname[]" placeholder="Enter education name">
                      </div>
                      <div class="col-lg-3">
                          <label for="eduplace">Institution Name</label>
                          <input type="text" class="form-control" id="eduplace" name="eduplace[]" placeholder="Enter institution name">
                      </div>
                      <div class="col-lg-2">
                          <label for="eduyearstart">Year Start</label>
                          <input type="text" class="form-control" id="eduyearstart" name="eduyearstart[]" placeholder="Enter year start">
                      </div>
                      <div class="col-lg-2">
                          <label for="eduyearend">Year End</label>
                          <input type="text" class="form-control" id="eduyearend" name="eduyearend[]" placeholder="Enter year end">
                      </div>
                      <div class="col-lg-2">
                          <label for="edulevel">Level</label>
                          <select class="form-control" id="edulevel" name="edulevel[]">
                            <option value="-" class="selected">Select level</option>
                          </select>
                      </div>
                      <div class="col-lg-1 mt-4 pt-2">                    
                        <div class="float-right"><a href="#" class="btn btn-danger btn-flat remove-btn"><i class="fas fa-times"></i></a></div>
                      </div>
                    </div>`);
        }
    });
    $('.edu-wrapper').on("click", ".remove-btn", function(e) {
        e.preventDefault();
        $(this).closest('div.additional').remove();
        x--;
    })
});*/
/* Language Add Button */
/*$(function() {
    var max = 5;
    var x = 1;
    $('.add-btn').click(function(e) {
        e.preventDefault();
        if (x < max) {
            x++;
            $('.lang-wrapper').append(`<div class="row additional">
                      <div class="col-lg-5">
                          <label for="langname">Language Name</label>
                          <input type="text" class="form-control" id="langname" name="langname[]" placeholder="Enter language name">
                      </div>
                      <div class="col-lg-5">
                          <label for="langproficiency">Language Proficiency</label>
                          <select class="form-control" id="langproficiency" name="langproficiency[]">
                            <option value="-" class="selected">Select Proficiency</option>
                          </select>
                      </div>
                      <div class="col-lg-2 mt-4 pt-2">                    
                        <div class="float-right"><a href="#" class="btn btn-danger btn-flat remove-btn"><i class="fas fa-times"></i></a></div>
                      </div>
                    </div>`);
        }
    });
    $('.lang-wrapper').on("click", ".remove-btn", function(e) {
        e.preventDefault();
        $(this).closest('div.additional').remove();
        x--;
    })
});*/
/* Interest Add Button */
/*$(function() {
    var max = 5;
    var x = 1;
    $('.add-btn').click(function(e) {
        e.preventDefault();
        if (x < max) {
            x++;
            $('.interest-wrapper').append(`<div class="row additional">
                      <div class="col-lg-5">
                          <label for="interestname">Interest Name</label>
                          <input type="text" class="form-control" id="interestname" name="interestname[]" placeholder="Enter interest name">
                      </div>
                      <div class="col-lg-2 mt-4 pt-2">                    
                        <div class="float-right"><a href="#" class="btn btn-danger btn-flat remove-btn"><i class="fas fa-times"></i></a></div>
                      </div>
                    </div>`);
        }
    });
    $('.interest-wrapper').on("click", ".remove-btn", function(e) {
        e.preventDefault();
        $(this).closest('div.additional').remove();
        x--;
    })
});*/
/* Skill Add Button */
/*$(function() {
    var max = 5;
    var x = 1;
    $('.add-btn').click(function(e) {
        e.preventDefault();
        if (x < max) {
            x++;
            $('.skill-wrapper').append(`<div class="row additional">
                      <div class="col-lg-6">
                          <label for="skillname">Skill</label>
                          <input type="text" class="form-control" id="skillname" name="skillname[]" placeholder="Enter skill">
                      </div>
                      <div class="col-lg-5">
                          <label for="skillpercentage">Percentage</label>
                          <input type="text" class="form-control" id="skillpercentage" name="skillpercentage[]" placeholder="Enter skill percentage">
                      </div>
                      <div class="col-lg-1 mt-4 pt-2">                    
                        <div class="float-right"><a href="#" class="btn btn-danger btn-flat remove-btn"><i class="fas fa-times"></i></a></div>
                      </div>
                    </div>`);
        }
    });
    $('.skill-wrapper').on("click", ".remove-btn", function(e) {
        e.preventDefault();
        $(this).closest('div.additional').remove();
        x--;
    })
});*/
setTimeout(function(){
    $("#alert").hide();
}, 2000);
$(function() {
  /* custom file input */
  bsCustomFileInput.init();
  /* tooltip universal */
  $('[data-toggle="tooltip"]').tooltip();
});
$(document).ready(function() {
$.fn.select2.defaults.set( "theme", "bootstrap" );  
  /* select2 js */
  $('.select2').select2({
    templateResult: function (data, container) {
        if (data.element) {
          $(container).addClass($(data.element).attr("class"));
        }
        return data.text;
      }
  });
});
$(function() {
    /* ToDo lsit sortable */
    $('.todo-list').sortable({
        placeholder: 'sort-highlight',
        handle: '.handle',
        forcePlaceholderSize: true,
        zIndex: 999999
    });
    /* Summernote editor*/
    $('#expdesc').summernote({
        height: 150,
        placeholder: "Share your experiences in detail....",
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['style','bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript','fontsize','fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['insert', ['table', 'hr', 'link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']],
        ],
        codemirror: { // codemirror options
            theme: 'monokai'
        }
    });
    $('#projdesc').summernote({
        height: 150,
        placeholder: "Share your past or current projects in detail....",
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['style','bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript','fontsize','fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['insert', ['table', 'hr', 'link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']],
        ],
        codemirror: { // codemirror options
            theme: 'monokai'
        }
    });   
    $('#templatedesign').summernote({
        height: 150,
        placeholder: "Design your own template",
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['style','bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript','fontsize','fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['insert', ['table', 'hr', 'link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']],
        ],
        codemirror: { // codemirror options
            theme: 'monokai'
        }
    });   
    $('#templatedesignedit').summernote({
        height: 300,
        placeholder: "Design your own template",
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['style','bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript','fontsize','fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['insert', ['table', 'hr', 'link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']],
        ],
        codemirror: { // codemirror options
            theme: 'monokai'
        }
    });
    /*Datepicker*/
    // $('#explength').daterangepicker({
    //     locale: {
    //         format: 'DD/MM/YYYY',
    //     },
    //   showDropdowns: true,
    //   drops: "auto",
    // });
    $('#explengthstart').on('change.datetimepicker', function(e){
        $('input#explengthstart').val(e.date.format('MM/YYYY'));
    }).datetimepicker({
        viewMode: 'months',
        format: 'MM/YYYY'
    });
    $('#explengthend').on('change.datetimepicker', function(e){
        $('input#explengthend').val(e.date.format('MM/YYYY'));
    }).datetimepicker({
        viewMode: 'months',
        format: 'MM/YYYY'
    });
    $('#tododuedate').daterangepicker({
       locale: {
            format: 'DD/MM/YYYY',
        },
      singleDatePicker: true,
      drops: "auto",
      applyButtonClasses: "bg-navy",
      cancelClass: "bg-danger",
    });
    /* Datatable*/
    $('#socialtable').DataTable({
        pageLength: 6,
        lengthChange: false,
        searching: true,
    });
    $('#edutable').DataTable({
        lengthChange: false,
        searching: true,
    });
    $('#langtable').DataTable({
        lengthChange: false,
        searching: true,
    });
    $('#interesttable').DataTable({
        lengthChange: false,
        searching: true,
    });
    $('#exptable').DataTable({
        pageLength: 3,
        lengthChange: false,
        searching: true,
        ordering: false,
    });
    $('#projtable').DataTable({
        pageLength: 3,
        lengthChange: false,
        searching: true,
    });
    $('#skilltable').DataTable({
        lengthChange: false,
        searching: true,
    });
    $('#socialsetting').DataTable({
        pageLength: 10,
        lengthChange: false,
        searching: true,
    });
    $('#educationsetting').DataTable({
        pageLength: 10,
        lengthChange: false,
        searching: true,
    });
    $('#languagesetting').DataTable({
        pageLength: 10,
        lengthChange: false,
        searching: true,
    });
    $('#projectsetting').DataTable({
        pageLength: 10,
        lengthChange: false,
        searching: true,
    });
    $('#todosetting').DataTable({
        lengthChange: false,
        searching: true,
    });
    $('#personalTable').DataTable({
        pageLength: 2,
        lengthChange: false,
        searching: true,
        order: [
            [1, 'asc']
        ],
    });
});
/* ajax form submit */
$(document).ready(function() {
    var Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });
    /* social */
    var socialform = $('#socialsettingform');
    var socialurl = 'setting/social';
    socialform.submit(function(e) {
        e.preventDefault();
        var data = socialform.serializeArray();
        var iconname = $('#inputIconName').val();
        var icontype = $('#inputType').val();
        if (iconname == '' && icontype == '') {
            Toast.fire({
                icon: 'error',
                title: 'All fields empty. Please enter something',
            });
        } else {
            $.ajax({
                type: 'POST',
                url: socialurl,
                data: data,
                dataType: 'json',
            }).done(function(data) {
                Toast.fire({
                    icon: data.icon,
                    title: data.msg,
                });
                setTimeout(function() {
                    location.reload();
                }, 3000);
            });
        }
    });
    /* education */
    var eduform = $('#educationsettingform');
    var eduurl = 'setting/education';
    eduform.submit(function(e) {
        e.preventDefault();
        var data = eduform.serializeArray();
        var level = $('#inputLevel').val();
        if (level == '') {
            Toast.fire({
                icon: 'error',
                title: 'Fields empty. Please enter something',
            });
        } else {
            $.ajax({
                type: 'POST',
                url: eduurl,
                data: data,
                dataType: 'json',
            }).done(function(data) {
                Toast.fire({
                    icon: data.icon,
                    title: data.msg,
                });
                setTimeout(function() {
                    location.reload();
                }, 3000);
            });
        }
    });
    /* language */
    var langform = $('#languagesettingform');
    var langurl = 'setting/language';
    langform.submit(function(e) {
        e.preventDefault();
        var data = langform.serializeArray();
        var proficiency = $('#inputProficiency').val();
        if (proficiency == '') {
            Toast.fire({
                icon: 'error',
                title: 'Fields empty. Please enter something',
            });
        } else {
            $.ajax({
                type: 'POST',
                url: langurl,
                data: data,
                dataType: 'json',
            }).done(function(data) {
                Toast.fire({
                    icon: data.icon,
                    title: data.msg,
                });
                setTimeout(function() {
                    location.reload();
                }, 3000);
            });
        }
    });
    /* project */
    var projform = $('#projectsettingform');
    var projurl = 'setting/project';
    projform.submit(function(e) {
        e.preventDefault();
        var data = projform.serializeArray();
        var proficiency = $('#inputStatus').val();
        if (proficiency == '') {
            Toast.fire({
                icon: 'error',
                title: 'Fields empty. Please enter something',
            });
        } else {
            $.ajax({
                type: 'POST',
                url: projurl,
                data: data,
                dataType: 'json',
            }).done(function(data) {
                Toast.fire({
                    icon: data.icon,
                    title: data.msg,
                });
                setTimeout(function() {
                    location.reload();
                }, 3000);
            });
        }
    });
    /* social module */
    var social = $('#socialform');
    var socurl = 'socials/save';
    social.submit(function(e) {
        e.preventDefault();
        var data = social.serializeArray();
        var socialname = $('#socialname').val();
        var sociallink = $('#sociallink').val();
        var socialicon = $('#socialicon').val();
        if (socialname == '' && sociallink == '' && socialicon == '') {
            Toast.fire({
                icon: 'error',
                title: 'Fields empty. Please enter something',
            });
        } else {
            $.ajax({
                type: 'POST',
                url: socurl,
                data: data,
                dataType: 'json',
            }).done(function(data) {
                Toast.fire({
                    icon: data.icon,
                    title: data.msg,
                });
                setTimeout(function() {
                    location.reload();
                }, 3000);
            });
        }
    });
    /* education module */
    var education = $('#educationform');
    var educationurl = 'educations/save';
    education.submit(function(e) {
        e.preventDefault();
        var data = education.serializeArray();
        var eduname = $('#eduname').val();
        var eduplace = $('#eduplace').val();
        var eduyearstart = $('#eduyearstart').val();
        var eduyearend = $('#eduyearend').val();
        var edulevel = $('#edulevel').val();
        if (eduname == '' && eduplace == '' && eduyearstart == '' && eduyearend == '' && edulevel == '') {
            Toast.fire({
                icon: 'error',
                title: 'Fields empty. Please enter something',
            });
        } else {
            $.ajax({
                type: 'POST',
                url: educationurl,
                data: data,
                dataType: 'json'
            }).done(function(data) {
                Toast.fire({
                    icon: data.icon,
                    title: data.msg,
                });
                setTimeout(function() {
                    location.reload();
                }, 3000);
            });
        }
    });
    /* language module */
    var language = $('#languageform');
    var languageurl = 'languages/save';
    language.submit(function(e) {
        e.preventDefault();
        var data = language.serializeArray();
        var langname = $('#langname').val();
        var langproficiency = $('#langproficiency').val();
        if (langname == '' && langproficiency == '') {
            Toast.fire({
                icon: 'error',
                title: 'Fields empty. Please enter something',
            });
        } else {
            $.ajax({
                type: 'POST',
                url: languageurl,
                data: data,
                dataType: 'json'
            }).done(function(data) {
                Toast.fire({
                    icon: data.icon,
                    title: data.msg,
                });
                setTimeout(function() {
                    location.reload();
                }, 3000);
            });
        }
    });
    /* interest module */
    var interest = $('#interestform');
    var interesturl = 'interests/save';
    interest.submit(function(e) {
        e.preventDefault();
        var data = interest.serializeArray();
        var interestname = $('#interestname').val();
        if (interestname == '') {
            Toast.fire({
                icon: 'error',
                title: 'Fields empty. Please enter something',
            });
        } else {
            $.ajax({
                type: 'POST',
                url: interesturl,
                data: data,
                dataType: 'json'
            }).done(function(data) {
                Toast.fire({
                    icon: data.icon,
                    title: data.msg,
                });
                setTimeout(function() {
                    location.reload();
                }, 3000);
            });
        }
    });
    /* objective module */
    var objective = $('#objectiveform');
    var objectiveurl = 'objectives/save';
    objective.submit(function(e) {
        e.preventDefault();
        var data = objective.serializeArray();
        var objectivedata = $('#objective').val();
        if (objectivedata == '') {
            Toast.fire({
                icon: 'error',
                title: 'Fields empty. Please enter something',
            });
        } else {
            $.ajax({
                type: 'POST',
                url: objectiveurl,
                data: data,
                dataType: 'json'
            }).done(function(data) {
                Toast.fire({
                    icon: data.icon,
                    title: data.msg,
                });
                setTimeout(function() {
                    location.reload();
                }, 3000);
            });
        }
    });
    /* skill module */
    var skill = $('#skillform');
    var skillurl = 'skills/save';
    skill.submit(function(e) {
        e.preventDefault();
        var data = skill.serializeArray();
        var skillname = $('#skillname').val();
        var skillpercentage = $('#skillpercentage').val();
        if (skillname == '' && skillpercentage == '') {
            Toast.fire({
                icon: 'error',
                title: 'Fields empty. Please enter something',
            });
        } else {
            $.ajax({
                type: 'POST',
                url: skillurl,
                data: data,
                dataType: 'json'
            }).done(function(data) {
                Toast.fire({
                    icon: data.icon,
                    title: data.msg,
                });
                setTimeout(function() {
                    location.reload();
                }, 3000);
            });
        }
    });
    /* experience module */
    var experience = $('#expform');
    var expurl = 'experiences/save';
    var currentEmp = $('#currentEmp');
    currentEmp.on('click', function(){
        if($(this).prop('checked') == true){
            $('div#explengthend').hide();
        }else if($(this).prop('checked') == false){
            $('div#explengthend').show();
        }
    });
    experience.submit(function(e){
        e.preventDefault();
        var expname = $('#expname').val();
        var expstart = $('input#explengthstart').val();
        var expend;
        if($('input#explengthend').val() != ''){
            expend = $('input#explengthend').val();
        }else{
            expend = 'current';
        }
        var expemployer = $('#expemployer').val();
        var expdesc = $('#expdesc').val();
        var data = {
            'expname' : expname,
            'expemployer' : expemployer,
            'expdesc' : expdesc,
            'expstart' : expstart,
            'expend' : expend,
        };
        if (expname == '' && expstart == '' && expend == '' && expemployer == '' && expdesc == '') {
          Toast.fire({
              icon: 'error',
              title: 'Fields empty. Please enter something',
          });
        } else {
          $.ajax({
              type: 'POST',
              url: expurl,
              data: data,
              dataType: 'json'
          }).done(function(data) {
              Toast.fire({
                  icon: data.icon,
                  title: data.msg,
              });
              setTimeout(function() {
                  location.reload();
              }, 3000);
          });
        }
    });
    
    /* project module */
    var project = $('#projectform');
    var projecturl = 'projects/save';
    project.submit(function(e) {
        e.preventDefault();
        var data = project.serializeArray();
        var projname = $('#projname').val();
        var projstatus = $('#projstatus').val();
        var projdesc = $('#projdesc').val();
        if (projname == '' && projstatus == '' && projdesc == '') {
            Toast.fire({
                icon: 'error',
                title: 'Fields empty. Please enter something',
            });
        } else {
            $.ajax({
                type: 'POST',
                url: projecturl,
                data: data,
                dataType: 'json'
            }).done(function(data) {
                Toast.fire({
                    icon: data.icon,
                    title: data.msg,
                });
                setTimeout(function() {
                    location.reload();
                }, 3000);
            });
        }
    });
    /* personal module */
    var personal = $('#profileform');
    var personalurl = 'personal/save';
    personal.validate();
    personal.submit(function(e) {
        e.preventDefault();
        var data = new FormData(this);
        var fullname = $('#fullname').val();
        var tagline = $('#tagline').val();
        var email = $('#email').val();
        var phone = $('#phone').val();
        var image = $('#uploadprofilephoto').val();
        if (fullname == '' && tagline == '' && email == '' && phone == '' && image == '') {
            Toast.fire({
                icon: 'error',
                title: 'Fields empty. Please enter something',
            });
        } else {
            $.ajax({
                type: 'POST',
                url: personalurl,
                data: data,
                dataType: 'json',
                processData: false,
                contentType: false,
            }).done(function(data) {
                Toast.fire({
                    icon: data.icon,
                    title: data.msg,
                });
                setTimeout(function() {
                    location.reload();
                }, 3000);
            });
        }
    });
    /* todo module */
    var activity = $('#todoformsetting');
    var activityurl = 'setting/activity';
    activity.submit(function(e) {
        e.preventDefault();
        var data = activity.serializeArray();
          console.log(data);
        var inputToDo = $('#inputToDo').val();
        var tododuedate = $('#tododuedate').val();
        if (inputToDo == '' && tododuedate == '') {
            Toast.fire({
                icon: 'error',
                title: 'All fields empty. Please enter something',
            });
        } else {
            $.ajax({
                type: 'POST',
                url: activityurl,
                data: data,
                dataType: 'json',
            }).done(function(data) {
                Toast.fire({
                    icon: data.icon,
                    title: data.msg,
                });
                setTimeout(function() {
                    location.reload();
                }, 3000);
            });
        }
    });
    /* background image form */
    var bgform = $('#bgform');
    var bgurl = 'setting/uploadbg';
    bgform.submit(function(e){
      e.preventDefault();
        var data = new FormData(this);
        var bginput = $('#uploadbg').val();
          if(bginput == ''){
            Toast.fire({
                icon: 'error',
                title: 'Fields empty. Please enter something',
            });
          }else{
            $.ajax({
                type: 'POST',
                url: bgurl,
                data: data,
                dataType: 'json',
                processData: false,
                contentType: false,
            }).done(function(data) {
                Toast.fire({
                    icon: data.icon,
                    title: data.msg,
                });
                setTimeout(function() {
                    location.reload();
                }, 3000);
            });
          }
    });
    /* Announcement form */
    var announcementsform = $('#announcementsform');
    var announcementurl = 'setting/announcement';
    announcementsform.submit(function(e) {
        e.preventDefault();
        var data = announcementsform.serializeArray();
        var announcements = $('#announcements').val();
        if (announcements == '') {
            Toast.fire({
                icon: 'error',
                title: 'Fields empty. Please enter something',
            });
        } else {
            $.ajax({
                type: 'POST',
                url: announcementurl,
                data: data,
                dataType: 'json'
            }).done(function(data) {
                Toast.fire({
                    icon: data.icon,
                    title: data.msg,
                });
                setTimeout(function() {
                    location.reload();
                }, 3000);
            });
        }
    });
    /* template section */
    var templateform = $('#templateform');
    var templateurl = 'setting/savetemplate';
    templateform.submit(function(e){
        e.preventDefault();
        var data = templateform.serializeArray();
        var templatename = $('#templatename').val();
        var templatedesign = $('#templatedesign').val();
        if(templatename == '' && templatedesign == ''){
            Toast.fire({
                icon: 'error',
                title: 'Fields empty. Please enter something',
            });
        }else{
            $.ajax({
                type: 'POST',
                url: templateurl,
                data: data,
                dataType: 'json',
            }).done(function(data) {
                Toast.fire({
                    icon: data.icon,
                    title: data.msg,
                });
                setTimeout(function() {
                    location.reload();
                }, 3000);
            });
        }
    });
    var selecttemplate = $('#selecttemplate');    
    selecttemplate.change(function(e){
        e.preventDefault();
        var templateid = selecttemplate.val();
        $.ajax({
            url: 'setting/getactivetemplateid/'+templateid,            
        }).then(function(data){
            var d = $.parseJSON(data);
            var templateactivate = $('#templateactivate');
            if(d != 0 || d != ''){
                templateactivate.removeClass("bg-success").addClass("bg-danger");
                templateactivate.tooltip('hide').attr('data-original-title','Deactivate').tooltip('show');
                templateactivate.click(function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    Swal.fire({
                        title: 'Are you sure to deactivate?',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        cancelButtonText: 'No',
                        reverseButtons: true
                    }).then((result) => {
                      if (result.isConfirmed) {
                          $.ajax({
                              type: 'POST',
                              url: 'setting/setactivetemplate/'+templateid,
                              success: function() {
                                  Swal.fire({
                                    icon: 'success',
                                    title: 'Template deactivated. Please activate new template.'
                                  });
                                  setTimeout(function() {
                                      location.reload();
                                  }, 3000);
                              },
                          });
                      } else if (result.dismiss === Swal.DismissReason.cancel) {
                          Swal.fire('Cancelled', 'Record not updated', 'error');
                      }
                  });
                });
            }else{
                templateactivate.removeClass("bg-danger").addClass("bg-success");
                templateactivate.tooltip('hide').attr('data-original-title','Activate').tooltip('show');
                templateactivate.click(function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    $.ajax({
                        type: 'POST',
                        url: 'setting/setactivetemplate/'+templateid,
                        datatype: 'JSON',
                        success: function(){
                            Swal.fire({
                                icon: 'success',
                                title: 'Template activate!',
                            });
                            setTimeout(function() {
                                location.reload();
                            }, 3000);
                        },
                    });
                });
            }
        });
    });
    var fieldoption = $('#fieldoption');
    fieldoption.change(function(e){
        var fieldoptionvalue = fieldoption.val();
        var templatedesign = $('#templatedesign');
        if(fieldoptionvalue != '' || fieldoptionvalue != '-'){
            templatedesign.summernote('editor.saveRange');
            templatedesign.summernote('editor.restoreRange');
            templatedesign.summernote('editor.focus');
            templatedesign.summernote('editor.insertText', fieldoptionvalue);
        }
    });
    /* PDFSetting */
    var pdfsettingform = $('#pdfsettings');
    var pdfsettingurl = 'setting/pdfsetting';
    pdfsettingform.validate();
    pdfsettingform.submit(function(e){
        e.preventDefault();
        var data = pdfsettingform.serializeArray();
        var mtop = $('#margin-top').val();
        var mbottom = $('#margin-bottom').val();
        var mleft = $('#margin-left').val();
        var mright = $('#margin-right').val();
        var header = $('#header').val();
        var footer = $('#footer').val();
        if(mtop == '' && mbottom == '' && mright  == '' && mleft  == '' && header  == '' && footer  ==''){
            Toast.fire({
                icon: 'error',
                title: 'Fields empty. Please enter value',
            });
        }else{
             $.ajax({
                type: 'POST',
                url: pdfsettingurl,
                data: data,
                dataType: 'json'
            }).done(function(data) {
                Toast.fire({
                    icon: data.icon,
                    title: data.msg,
                });
                setTimeout(function() {
                    location.reload();
                }, 3000);
            });
        }
    });
    
});
/* delete action alert */
var deleteToast = Swal.mixin({
    customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
});
/* social */
function deleteAlertSocial(id) {
    var removeurl = "setting/removesocial/" + id;
    deleteToast.fire({
        title: 'Are you sure?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: removeurl,
                data: id,
                success: function() {
                    deleteToast.fire('Deleted!', 'Your record has been deleted.', 'success');
                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                },
                error: function(err) {
                    console.log(err);
                }
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            deleteToast.fire('Cancelled', 'Record not deleted', 'error');
        }
    });
}

function editAlertSocial(id) {
    var dataurl = 'setting/getsocial/' + id;
    $.ajax({
        url: dataurl,
        dataType: 'json',
        success: function(resp) {
            Swal.fire({
                title: 'Edit record ' + resp.iconname,
                html: '<input type="text" class="swal2-input" id="inputIconName" placeholder="Icon Name" value="' + resp.iconname + '"> <input type="text" class="swal2-input" id="inputType" placeholder="Type" value="' + resp.icontype + '">',
                confirmButtonText: 'Update',
                focusConfirm: false,
                preConfirm: () => {
                    const iconname = Swal.getPopup().querySelector('#inputIconName').value
                    const icontype = Swal.getPopup().querySelector('#inputType').value
                    if (!iconname || !icontype) {
                        Swal.showValidationMessage(`Please enter value`)
                    }
                    return {
                        iconname: iconname,
                        icontype: icontype
                    }
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    var updateurl = 'setting/updatesocial/' + resp.id;
                    $.ajax({
                        type: 'POST',
                        url: updateurl,
                        data: result.value,
                        success: function() {
                            Swal.fire({
                                icon: 'success',
                                title: 'Record updated!',
                            });
                            setTimeout(function() {
                                location.reload();
                            }, 3000);
                        }
                    });
                }
            });
        }
    });
}
/* education */
function deleteAlertEdu(id) {
    var removeurl = "setting/removeeducation/" + id;
    deleteToast.fire({
        title: 'Are you sure?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: removeurl,
                data: id,
                success: function() {
                    deleteToast.fire('Deleted!', 'Your record has been deleted.', 'success');
                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                },
                error: function(err) {
                    console.log(err);
                }
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            deleteToast.fire('Cancelled', 'Record not deleted', 'error');
        }
    });
}

function editAlertEdu(id) {
    var dataurl = 'setting/geteducation/' + id;
    $.ajax({
        url: dataurl,
        dataType: 'json',
        success: function(resp) {
            Swal.fire({
                title: 'Edit record ' + resp.level,
                html: '<input type="text" class="swal2-input" id="inputLevel" placeholder="Education level" value="' + resp.level + '">',
                confirmButtonText: 'Update',
                focusConfirm: false,
                preConfirm: () => {
                    const level = Swal.getPopup().querySelector('#inputLevel').value
                    if (!level) {
                        Swal.showValidationMessage(`Please enter value`)
                    }
                    return {
                        level: level
                    }
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    var updateurl = 'setting/updateeducation/' + resp.id;
                    $.ajax({
                        type: 'POST',
                        url: updateurl,
                        data: result.value,
                        success: function() {
                            Swal.fire({
                                icon: 'success',
                                title: 'Record updated!',
                            });
                            setTimeout(function() {
                                location.reload();
                            }, 3000);
                        }
                    });
                }
            });
        }
    });
}
/* language */
function deleteAlertLang(id) {
    var removeurl = "setting/removelanguage/" + id;
    deleteToast.fire({
        title: 'Are you sure?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: removeurl,
                data: id,
                success: function() {
                    deleteToast.fire('Deleted!', 'Your record has been deleted.', 'success');
                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                },
                error: function(err) {
                    console.log(err);
                }
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            deleteToast.fire('Cancelled', 'Record not deleted', 'error');
        }
    });
}

function editAlertLang(id) {
    var dataurl = 'setting/getlanguage/' + id;
    $.ajax({
        url: dataurl,
        dataType: 'json',
        success: function(resp) {
            Swal.fire({
                title: 'Edit record ' + resp.proficiency,
                html: '<input type="text" class="swal2-input" id="inputProficiency" placeholder="Language Proficiency" value="' + resp.proficiency + '">',
                confirmButtonText: 'Update',
                focusConfirm: false,
                preConfirm: () => {
                    const proficiency = Swal.getPopup().querySelector('#inputProficiency').value
                    if (!proficiency) {
                        Swal.showValidationMessage(`Please enter value`)
                    }
                    return {
                        proficiency: proficiency
                    }
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    var updateurl = 'setting/updatelanguage/' + resp.id;
                    $.ajax({
                        type: 'POST',
                        url: updateurl,
                        data: result.value,
                        success: function() {
                            Swal.fire({
                                icon: 'success',
                                title: 'Record updated!',
                            });
                            setTimeout(function() {
                                location.reload();
                            }, 3000);
                        }
                    });
                }
            });
        }
    });
}
/* project */
function deleteAlertProj(id) {
    var removeurl = "setting/removeproject/" + id;
    deleteToast.fire({
        title: 'Are you sure?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: removeurl,
                data: id,
                success: function() {
                    deleteToast.fire('Deleted!', 'Your record has been deleted.', 'success');
                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                },
                error: function(err) {
                    console.log(err);
                }
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            deleteToast.fire('Cancelled', 'Record not deleted', 'error');
        }
    });
}

function editAlertProj(id) {
    var dataurl = 'setting/getproject/' + id;
    $.ajax({
        url: dataurl,
        dataType: 'json',
        success: function(resp) {
            Swal.fire({
                title: 'Edit record ' + resp.status,
                html: '<input type="text" class="swal2-input" id="inputStatus" placeholder="Project Status" value="' + resp.status + '">',
                confirmButtonText: 'Update',
                focusConfirm: false,
                preConfirm: () => {
                    const status = Swal.getPopup().querySelector('#inputStatus').value
                    if (!status) {
                        Swal.showValidationMessage(`Please enter value`)
                    }
                    return {
                        status: status
                    }
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    var updateurl = 'setting/updateproject/' + resp.id;
                    $.ajax({
                        type: 'POST',
                        url: updateurl,
                        data: result.value,
                        success: function() {
                            Swal.fire({
                                icon: 'success',
                                title: 'Record updated!',
                            });
                            setTimeout(function() {
                                location.reload();
                            }, 3000);
                        }
                    });
                }
            });
        }
    });
}
/* social module */
function deleteSocial(id) {
    var removeurl = "socials/remove/" + id;
    deleteToast.fire({
        title: 'Are you sure?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: removeurl,
                data: id,
                success: function() {
                    deleteToast.fire('Deleted!', 'Your record has been deleted.', 'success');
                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                },
                error: function(err) {
                    console.log(err);
                }
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            deleteToast.fire('Cancelled', 'Record not deleted', 'error');
        }
    });
}

function editSocial(id) {
    $('#modalsocial' + id).modal('hide');
    var updateurl = 'socials/update/' + id;
    var socialeditform = $('#socialformedit' + id);
    var params = socialeditform.serializeArray();
    $.ajax({
        type: 'POST',
        url: updateurl,
        data: params,
        success: function() {
            Swal.fire({
                icon: 'success',
                title: 'Record updated!',
            });
            setTimeout(function() {
                location.reload();
            }, 3000);
        }
    });
}
/* education module */
function deleteEdu(id) {
    var removeurl = "educations/remove/" + id;
    deleteToast.fire({
        title: 'Are you sure?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: removeurl,
                data: id,
                success: function() {
                    deleteToast.fire('Deleted!', 'Your record has been deleted.', 'success');
                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                },
                error: function(err) {
                    console.log(err);
                }
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            deleteToast.fire('Cancelled', 'Record not deleted', 'error');
        }
    });
}

function editEdu(id) {
    $('#modaledu' + id).modal('hide');
    var updateurl = 'educations/update/' + id;
    var edueditform = $('#educationformedit' + id);
    var params = edueditform.serializeArray();
    $.ajax({
        type: 'POST',
        url: updateurl,
        data: params,
        success: function() {
            Swal.fire({
                icon: 'success',
                title: 'Record updated!',
            });
            setTimeout(function() {
                location.reload();
            }, 3000);
        }
    });
}
/* language module */
function deleteLang(id) {
    var removeurl = "languages/remove/" + id;
    deleteToast.fire({
        title: 'Are you sure?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: removeurl,
                data: id,
                success: function() {
                    deleteToast.fire('Deleted!', 'Your record has been deleted.', 'success');
                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                },
                error: function(err) {
                    console.log(err);
                }
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            deleteToast.fire('Cancelled', 'Record not deleted', 'error');
        }
    });
}

function editLang(id) {
    $('#modallang' + id).modal('hide');
    var updateurl = 'languages/update/' + id;
    var languageformedit = $('#languageformedit' + id);
    var params = languageformedit.serializeArray();
    $.ajax({
        type: 'POST',
        url: updateurl,
        data: params,
        success: function() {
            Swal.fire({
                icon: 'success',
                title: 'Record updated!',
            });
            setTimeout(function() {
                location.reload();
            }, 3000);
        }
    });
}
/* interest module */
function deleteInterest(id) {
    var removeurl = "interests/remove/" + id;
    deleteToast.fire({
        title: 'Are you sure?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: removeurl,
                data: id,
                success: function() {
                    deleteToast.fire('Deleted!', 'Your record has been deleted.', 'success');
                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                },
                error: function(err) {
                    console.log(err);
                }
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            deleteToast.fire('Cancelled', 'Record not deleted', 'error');
        }
    });
}

function editInterest(id) {
    $('#modalinterest' + id).modal('hide');
    var updateurl = 'interests/update/' + id;
    var interestformedit = $('#interestformedit' + id);
    var params = interestformedit.serializeArray();
    $.ajax({
        type: 'POST',
        url: updateurl,
        data: params,
        success: function() {
            Swal.fire({
                icon: 'success',
                title: 'Record updated!',
            });
            setTimeout(function() {
                location.reload();
            }, 3000);
        }
    });
}
/* skill module */
function deleteSkill(id) {
    var removeurl = "skills/remove/" + id;
    deleteToast.fire({
        title: 'Are you sure?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: removeurl,
                data: id,
                success: function() {
                    deleteToast.fire('Deleted!', 'Your record has been deleted.', 'success');
                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                },
                error: function(err) {
                    console.log(err);
                }
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            deleteToast.fire('Cancelled', 'Record not deleted', 'error');
        }
    });
}

function editSkill(id) {
    $('#modalskill' + id).modal('hide');
    var updateurl = 'skills/update/' + id;
    var skillformedit = $('#skillformedit' + id);
    var params = skillformedit.serializeArray();
    $.ajax({
        type: 'POST',
        url: updateurl,
        data: params,
        success: function() {
            Swal.fire({
                icon: 'success',
                title: 'Record updated!',
            });
            setTimeout(function() {
                location.reload();
            }, 3000);
        }
    });
}
/* experience module */
function deleteExperience(id) {
    var removeurl = "experiences/remove/" + id;
    deleteToast.fire({
        title: 'Are you sure?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: removeurl,
                data: id,
                success: function() {
                    deleteToast.fire('Deleted!', 'Your record has been deleted.', 'success');
                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                },
                error: function(err) {
                    console.log(err);
                }
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            deleteToast.fire('Cancelled', 'Record not deleted', 'error');
        }
    });
}

function popupExp(id) {
    var expstart;
    var expend;
    // $('#explengthedit' + id).daterangepicker({
    //     locale: {
    //         format: 'DD/MM/YYYY',
    //     }
    // },function(start, end){
    //   $('#explengthedit'+ id).html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
    //     expstart = start;
    //     expend = end;
    // });
    var currentEmp = $('#currentEmpedit'+id);
    currentEmp.on('click', function(){
        if($(this).prop('checked') == true){
            $('div#explengthendedit'+id).hide();
        }else if($(this).prop('checked') == false){
            $('div#explengthendedit'+id).show();
        }
    });
    // $('#explengthstartedit' + id).datetimepicker({
    //     viewMode: 'months',
    //     format: 'MM/YYYY'
    // });
    $('#explengthstartedit' + id).on('change.datetimepicker', function(e){
        var newdate = e.date;
         $('input#explengthstartedit' + id).val(newdate.format('MM/YYYY'));
    }).datetimepicker({
        viewMode: 'months',
        format: 'MM/YYYY'
    });
    $('#explengthendedit' + id).on('change.datetimepicker', function(e){
        var newdate = e.date;
         $('input#explengthendedit' + id).val(newdate.format('MM/YYYY'));
    }).datetimepicker({
        viewMode: 'months',
        format: 'MM/YYYY'
    });
    $('#expmodalsubmit'+id).click(function(){
      $('#modalexp' + id).modal('hide');
      var datepicker = $('#explengthedit'+id);
      var updateurl = 'experiences/update/' + id;
      var expformedit = $('#expformedit' + id);
      var expnameedit = $('#expnameedit' + id).val();
      var expemployeredit = $('#expemployeredit' + id).val();
      var expdescedit = $('#expdescedit' + id).val();
      var expstart =  $('input#explengthstartedit' + id).val();
      var expend = $('input#explengthendedit' + id).val();
      var params;
      if (typeof expstart == 'undefined' && typeof expend == 'undefined'){
        params = {
          'expnameedit' : expnameedit,
          'expemployeredit' : expemployeredit,
          'expdescedit' : expdescedit,
        };
      }else{
        params = {
          'expnameedit' : expnameedit,
          'expstart' : expstart,
          'expend' : expend,
          'expemployeredit' : expemployeredit,
          'expdescedit' : expdescedit,
        };
      }
      $.ajax({
          type: 'POST',
          url: updateurl,
          data: params,
          success: function() {
              Swal.fire({
                  icon: 'success',
                  title: 'Record updated!',
              });
              setTimeout(function() {
                  location.reload();
              }, 3000);
          }
      });
    });
    $('#expdescedit' + id).summernote({
        height: 150,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['style','bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript','fontsize','fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['insert', ['table', 'hr', 'link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']],
        ],
        codemirror: { // codemirror options
            theme: 'monokai'
        }
    });
}

/* project module */
function deleteProject(id) {
    var removeurl = "projects/remove/" + id;
    deleteToast.fire({
        title: 'Are you sure?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: removeurl,
                data: id,
                success: function() {
                    deleteToast.fire('Deleted!', 'Your record has been deleted.', 'success');
                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                },
                error: function(err) {
                    console.log(err);
                }
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            deleteToast.fire('Cancelled', 'Record not deleted', 'error');
        }
    });
}

function popupProj(id) {
    $('#projdescedit' + id).summernote({
        height: 150,
       toolbar: [
            // [groupName, [list of button]]
            ['style', ['style','bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript','fontsize','fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['insert', ['table', 'hr', 'link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']],
        ],
        codemirror: { // codemirror options
            theme: 'monokai'
        }
    });
}

function editProject(id) {
    $('#modalproj' + id).modal('hide');
    var updateurl = 'projects/update/' + id;
    var projectformedit = $('#projectformedit' + id);
    var params = projectformedit.serializeArray();
    $.ajax({
        type: 'POST',
        url: updateurl,
        data: params,
        success: function() {
            Swal.fire({
                icon: 'success',
                title: 'Record updated!',
            });
            setTimeout(function() {
                location.reload();
            }, 3000);
        }
    });
}
/* profile module */
function deleteProfile(id) {
    var removeurl = "personal/remove/" + id;
    deleteToast.fire({
        title: 'Are you sure?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: removeurl,
                data: id,
                success: function() {
                    deleteToast.fire('Deleted!', 'Your record has been deleted.', 'success');
                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                },
                error: function(err) {
                    console.log(err);
                }
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            deleteToast.fire('Cancelled', 'Record not deleted', 'error');
        }
    });
}
  
function editProfile(id) {
    var updateurl = 'personal/update/' + id;
    var profileformedit = $('#profileformedit' + id);
    profileformedit.submit(function(e){
      e.preventDefault();
      profileformedit.validate();
      $('#modalProfile' + id).modal('hide');
      var params = new FormData(this);
      $.ajax({
          type: 'POST',
          url: updateurl,
          data: params,
          dataType: 'json',
          processData: false,
          contentType: false,
      }).done(function(data){
        Swal.fire({
            icon: data.icon,
            title: data.msg,
        });
        setTimeout(function() {
            location.reload();
        }, 3000);
      });
    });    
}

function switchAjax(id) {
    var toggle = $('#activeSwitch' + id);
    $.ajax({
        url: 'personal/countStatus',
        success: function(count){
          if(count > 0){
            $.ajax({
              url: 'personal/checkStatusById/'+id,
              success: function(data) {
                if(data == 1){
                  Swal.fire({
                      title: 'Are you sure to deactivate?',
                      icon: 'warning',
                      showCancelButton: true,
                      confirmButtonText: 'Yes',
                      cancelButtonText: 'No',
                      reverseButtons: true
                  }).then((result) => {
                      if (result.isConfirmed) {
                          $.ajax({
                              type: 'POST',
                              url: 'personal/updateStatusById/'+id,
                              data: {'status':0},
                              success: function() {
                                  Swal.fire({
                                    icon: 'success',
                                    title: 'Profile updated. Please activate new profile'
                                  });
                                  setTimeout(function() {
                                      location.reload();
                                  }, 3000);
                              },
                          });
                      } else if (result.dismiss === Swal.DismissReason.cancel) {
                          Swal.fire('Cancelled', 'Record not updated', 'error');
                          toggle.prop('checked',true);
                      }
                  });
                }else{
                  Swal.fire({
                    'icon': 'warning',
                    'title': 'You have active profile. Please deactivate first.',
                  });
                  toggle.prop('checked',false);
                }
              }
            });
          }else{
            Swal.fire({
                title: 'Are you sure to activate?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                      type: 'POST',
                        url: 'personal/updateStatusById/'+id,
                        data: {'status': 1},
                        success: function() {
                            Swal.fire({
                              icon: 'success',
                              title: 'Profile updated.'
                            });
                            setTimeout(function() {
                                location.reload();
                            }, 3000);
                        },
                    });
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire('Cancelled', 'Record not updated', 'error');
                    toggle.prop('checked',false);
                }
            });
          }
          
        }
    });
}

/* todo */
function deleteAlertToDo(id) {
    var removeurl = "setting/removeactivity/" + id;
    deleteToast.fire({
        title: 'Are you sure?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: removeurl,
                data: id,
                success: function() {
                    deleteToast.fire('Deleted!', 'Your record has been deleted.', 'success');
                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                },
                error: function(err) {
                    console.log(err);
                }
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            deleteToast.fire('Cancelled', 'Record not deleted', 'error');
        }
    });
}

function popupTodo(id) {    
    $('#tododuedateedit'+id).daterangepicker({
       locale: {
            format: 'DD/MM/YYYY',
        },
      singleDatePicker: true,
      drops: "auto",
      applyButtonClasses: "bg-navy",
      cancelClass: "bg-danger",
    });
}

function editTodo(id) {
    $('#modaltodo' + id).modal('hide');
    var updateurl = 'setting/updateactivity/' + id;
    var todoformsettingedit = $('#todoformsettingedit' + id);
    var params = todoformsettingedit.serializeArray();
    $.ajax({
        type: 'POST',
        url: updateurl,
        data: params,
        success: function() {
            Swal.fire({
                icon: 'success',
                title: 'Record updated!',
            });
            setTimeout(function() {
                location.reload();
            }, 3000);
        }
    });
}

function popConfirm(id){
    var todoCheck = $('#todoCheck'+id);
    if(todoCheck.length > 0){
        var url = 'admin/updateToDo/'+id;
        $.ajax({
          type: 'POST',
          url: url,
          success: function() {
              Swal.fire({
                  icon: 'success',
                  title: 'Record updated!',
              });
              setTimeout(function() {
                  location.reload();
              }, 3000);
          },
        });
    }
}
/* template section */
function deleteTemplate(){
    var templateid = $('#selecttemplate').val();
    if(templateid == '-' || templateid == ''){
        Swal.fire({
            icon: 'error',
            title: 'No template to remove. Please select a template',
        });
    }else{
        var removeurl = "setting/removetemplate/"+templateid;
        deleteToast.fire({
            title: 'Are you sure?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: removeurl,
                    data: templateid,
                    success: function() {
                        deleteToast.fire('Deleted!', 'Your record has been deleted.', 'success');
                        setTimeout(function() {
                            location.reload();
                        }, 3000);
                    },
                    error: function(err) {
                        console.log(err);
                    }
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                deleteToast.fire('Cancelled', 'Record not deleted', 'error');
            }
        });
    }
}
function popupTemplate(){
    var templateid = $('#selecttemplate').val();
    if(templateid == '-' || templateid == ''){
        Swal.fire({
            icon: 'error',
            title: 'No template to edit. Please select a template',
        });
    }else{
        $.ajax({
            type: 'POST',
            url: 'setting/gettemplatename/'+templateid,
            datatype: 'JSON',
            success: function(data){
                var d = $.parseJSON(data);
                $('#modaltemplate').modal('show');
                $('#modaltitle').text('Edit Template '+d.name);
                var templatenameedit = $('#templatenameedit');
                var templatedesignedit = $('#templatedesignedit');
                var bgcoloredit = $('#bgcoloredit');
                var accentcoloredit = $('#accentcoloredit');
                templatenameedit.val(d.name);
                templatedesignedit.summernote('code',d.design);
                bgcoloredit.val(d.maincolor);
                accentcoloredit.val(d.accentcolor);
                bgcoloredit.select2({
                    templateResult: function (data, container) {
                        if (data.element) {
                          $(container).addClass($(data.element).attr("class"));
                        }
                        return data.text;
                      }
                  });
                accentcoloredit.select2({
                    templateResult: function (data, container) {
                        if (data.element) {
                          $(container).addClass($(data.element).attr("class"));
                        }
                        return data.text;
                      }
                  });
                var fieldoptionedit = $('#fieldoptionedit');
                fieldoptionedit.change(function(e){
                    var fieldoptionvalue = fieldoptionedit.val();
                    var templatedesignedit = $('#templatedesignedit');
                    if(fieldoptionvalue != '' || fieldoptionvalue != '-'){
                        templatedesignedit.summernote('editor.insertText', fieldoptionvalue);
                    }
                });
                var updatebtn = $('#templateupdatebtn');
                updatebtn.click(function(e){
                    e.preventDefault();
                    $('#modaltemplate').modal('hide');
                    if(templatenameedit.val() != '' && templatedesignedit.val() != ''){
                        var params = {
                            'name' : templatenameedit.val(),
                            'design' : templatedesignedit.val(),
                            'maincolor' : bgcoloredit.val(),
                            'accentcolor' : accentcoloredit.val(),
                        };
                        $.ajax({
                            type: 'POST',
                            url: 'setting/updatetemplate/'+templateid,
                            data: params,
                            success: function() {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Record updated!',
                                });
                                setTimeout(function() {
                                    location.reload();
                                }, 3000);
                            }
                        });
                    }else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Fields empty. Please enter value',
                        });
                    }
                });                
            }          
        });
        
    }
}
function activateTemplate(){
    var templateid = $('#selecttemplate').val();
    if(templateid == '-' || templateid == ''){
        Swal.fire({
            icon: 'error',
            title: 'No template to activate. Please select a template',
        });
    }
}
function formatExpDate(dateObject){
    var d = new Date(dateObject);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = month + "/" + year;

    return date;
}