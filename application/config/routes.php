<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'template';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['admin'] = 'admin';
$route['setting'] = 'setting';
$route['setting/removesocial/(:num)'] = 'setting/removesocial/$1';
$route['setting/getsocial/(:num)'] = 'setting/getsocial/$1';
$route['setting/updatesocial/(:num)'] = 'setting/updatesocial/$1';
$route['setting/removeeducation/(:num)'] = 'setting/removeeducation/$1';
$route['setting/geteducation/(:num)'] = 'setting/geteducation/$1';
$route['setting/updateeducation/(:num)'] = 'setting/updateeducation/$1';
$route['setting/removelanguage/(:num)'] = 'setting/removelanguage/$1';
$route['setting/getlanguage/(:num)'] = 'setting/getlanguage/$1';
$route['setting/updatelanguage/(:num)'] = 'setting/updatelanguage/$1';
$route['setting/removeproject/(:num)'] = 'setting/removeproject/$1';
$route['setting/getproject/(:num)'] = 'setting/getproject/$1';
$route['setting/updateproject/(:num)'] = 'setting/updateproject/$1';
$route['personal'] = 'personal';
$route['socials'] = 'socials';
$route['educations'] = 'educations';
$route['languages'] = 'languages';
$route['interests'] = 'interests';
$route['objectives'] = 'objectives';
$route['experiences'] = 'experiences';
$route['projects'] = 'projects';
$route['skills'] = 'skills';
$route['login'] = 'login';
$route['register'] = 'register';
$route['logout'] = 'login/logout';
$route['noaccess'] = 'login/noaccess';
