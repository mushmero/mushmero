<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo getenv("APP_NAME")." | Resume of Amirul Hakim"; ?></title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Resume of Muhd Amirul Hakim Zailan.">
    <meta name="author" content="Amirul Hakim">
    <link rel="shortcut icon" href="<?php echo getenv("APP__URL")."/".("assets/images/favicon.ico"); ?>">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,400italic,300italic,300,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <!-- Global CSS -->
    <link rel="stylesheet" href="<?php echo getenv("APP_URL")."/".("vendor/twbs/bootstrap/dist/css/bootstrap.min.css"); ?>" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />

    <!-- Theme CSS -->
    <link type="text/css" id="theme-style" rel="stylesheet" href="<?php echo getenv("APP_URL")."/".("assets/css/styles.css"); ?>">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <div class="wrapper">
        <div class="sidebar-wrapper">
            <div class="profile-container">
                <img class="profile" src="<?php echo $profilepic; ?>" alt=""/>
                <h1 class="name"><?php echo $fullname; ?></h1>
                <h3 class="tagline"><?php echo $tagline; ?></h3>
            </div><!--//profile-container-->

            <div class="contact-container container-block">
                <ul class="list-unstyled contact-list">
                    <li class="email"><i class="fa fa-envelope"></i><a href="mailto: <?php echo $email; ?>"><?php echo $email; ?></a></li>
                    <li class="phone"><i class="fa fa-phone"></i><a href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a></li>
                    <?php if (!empty($social)) {?>
                        <?php for ($i = 0; $i < count($social); $i++) {?>
                            <li class="<?php echo $social[$i]; ?>"><i class="<?php echo $socialIcon[$i]; ?>"></i><a href="<?php echo $socialLinks[$i]; ?>" target="_blank"> <?php echo $socialText[$i]; ?></a></li>
                        <?php }?>
                    <?php }?>
                </ul>
            </div><!--//contact-container-->
            <div class="education-container container-block">
                <h2 class="container-block-title">Education</h2>
                <?php if (!empty($eduTitle)) {?>
                    <?php for ($i = 0; $i < count($eduTitle); $i++) {?>
                        <div class="item">
                            <div class="degree"><?php echo $eduTitle[$i]; ?></div>
                            <div class="meta"><?php echo $eduPlace[$i]; ?></div>
                            <div class="time"><?php echo $eduTime[$i]; ?></div>
                        </div>
                    <?php }?>
                <?php }?>
            </div><!--//education-container-->

            <div class="languages-container container-block">
                <h2 class="container-block-title">Languages</h2>
                <ul class="list-unstyled $interests-list">
                    <?php if (!empty($langTitle)) {?>
                        <?php for ($i = 0; $i < count($langTitle); $i++) {?>
                            <li><?php echo $langTitle[$i]; ?> <span class="lang-desc"><?php echo $langDesc[$i]; ?></span></li>
                        <?php }?>
                    <?php }?>
                </ul>
            </div><!--//$interests-->

            <div class="$interests-container container-block">
                <h2 class="container-block-title">Interest</h2>
                <ul class="list-unstyled $interests-list">
                    <?php if (!empty($interest)) {?>
                        <?php for ($i = 0; $i < count($interest); $i++) {?>
                            <li><?php echo $interest[$i]; ?></li>
                        <?php }?>
                    <?php }?>
                </ul>
            </div><!--//$interests-->

        </div><!--//sidebar-wrapper-->

        <div class="main-wrapper">

            <section class="section summary-section">
                <h2 class="section-title"><i class="fa fa-bullseye"></i>Objective</h2>
                <div class="summary">
                    <p align="justify"><?php echo $objective; ?></p>
                </div><!--//summary-->
            </section><!--//section-->

            <section class="section experiences-section">
                <h2 class="section-title"><i class="fa fa-briefcase"></i>Experiences</h2>

                <?php if (!empty($jobTitle)) {?>
                    <?php for ($i = 0; $i < count($jobTitle); $i++) {?>
                        <div class="item">
                            <div class="meta">
                                <div class="upper-row">
                                    <h3 class="job-title"><?php echo $jobTitle[$i]; ?></h3>
                                    <div class="time"><?php echo $jobTime[$i]; ?></div>
                                </div><!--//upper-row-->
                                <div class="company"><?php echo $jobCompany[$i]; ?></div>
                            </div><!--//meta-->
                            <div class="details">
                                <p><?php for ($j = 0; $j < count($jobDesc[$i]); $j++) {?>
                                    <li><?php echo $jobDesc[$i][$j]; ?></li>
                                    <?php }?>
                                </p>
                            </div><!--//details-->
                        </div><!--//item-->
                    <?php }?>
                <?php }?>
            </section><!--//section-->

            <section class="section projects-section">
                <h2 class="section-title"><i class="fa fa-archive"></i>Projects</h2>
                <div class="intro">
                    <p>Below is list of my current & past project that have been done personally</p>
                </div><!--//intro-->
                <?php if (!empty($projectName)) {?>
                    <?php for ($i = 0; $i < count($projectLink); $i++) {?>
                        <div class="item">
                            <span class="project-title"><a href="<?php echo $projectLink[$i]; ?>" target="_blank"><?php echo $projectName[$i]; ?></a></span> - <span class="project-tagline"><?php echo $projectDesc[$i]; ?></span>
                        </div><!--//item-->
                    <?php }?>
                <?php }?>
            </section><!--//section-->

            <section class="skills-section section">
                <h2 class="section-title"><i class="fa fa-rocket"></i>Skills &amp; Proficiency</h2>
                <div class="skillset">
                    <?php if (!empty($skillsTitle)) {?>
                        <?php for ($i = 0; $i < count($skillsTitle); $i++) {?>
                            <div class="item">
                                <h3 class="level-title"><?php echo $skillsTitle[$i]; ?></h3>
                                <div class="level-bar">
                                    <div class="level-bar-inner" data-level="<?php echo $skillsLevel[$i]; ?>">
                                    </div>
                                </div><!--//level-bar-->
                            </div><!--//item-->
                        <?php }?>
                    <?php }?>
                </div>
            </section><!--//skills-section-->

        </div><!--//main-body-->
    </div>

    <footer class="footer">
        <div class="text-center">
                <small class="copyright">Designed with <i class="fa fa-heart"></i> by <a href="http://themes.3rdwavemedia.com" target="_blank">Xiaoying Riley</a> for developers. Edited by <a href='https://mushmero.com' target='_blank'>myself</a></small>
        </div><!--//container-->
    </footer><!--//footer-->

    <!-- jQuery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo getenv("APP_URL")."/".('vendor/twbs/bootstrap/dist/js/bootstrap.bundle.js'); ?>"></script>
    <!-- custom js -->
    <script type="text/javascript" src="<?php echo getenv("APP_URL")."/".("assets/js/main.js"); ?>"></script>
</body>
</html>