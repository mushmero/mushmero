<?php $this->load->view("admin/templates/header"); ?>
<div class="login-box">
  <div class="card">
    <div class="card-header text-center bg-navy">
      <a href="<?php echo getenv("APP_URL"); ?>" class="h1"><b><?php echo getenv("APP_NAME") ?></b></a>
    </div>
    <div class="card-body">
      <p class="login-box-msg"><i> Your resume center </i></p>
      <?php echo (isset($success)) ? "<div class=\"alert alert-success\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$success</strong></div>" : ''; ?>
      <?php echo (isset($error)) ? "<div class=\"alert alert-danger\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$error</strong></div>" : ''; ?>

      <form action="<?php echo getenv("APP_URL").('login'); ?>" method="post">
        <div class="input-group mb-3 has-feedback">
          <input type="text" class="form-control" placeholder="Username" name="username" id="username">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>        
        <?php echo form_error('username', '<p class="help-inline">', '</p>'); ?>
        <div class="input-group mb-3 has-feedback">
          <input type="password" class="form-control" placeholder="Password" name="password" id="password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <?php echo form_error('password', '<p class="help-inline">', '</p>'); ?>
        <div class="row">
          <div class="col-8">
          </div>
          <div class="col-4">
            <button type="submit" class="btn bg-navy btn-block"><i class="fas fa-sign-in-alt"></i> Sign In</button>
          </div>
        </div>
      </form>
      <!-- <p class="mb-1">
        <a class="text-navy" href="forgot-password.html">Forgot password</a>
      </p> -->
    </div>
    <div class="card-footer bg-white">    	
      <p class="mb-1 text-center">
      	<strong> Version <?php echo getenv("APP_VERSION"); ?> </strong> | <?php echo date("Y"); ?><a class="text-navy" href="https://mushmero.com" target="_blank"> <i class="fas fa-heart"></i> Mushmero</a>
      </p>
    </div>
  </div>
</div>
<?php $this->load->view("admin/templates/footer"); ?>