<?php $this->load->view("admin/templates/header"); ?>
<?php $this->load->view("admin/templates/navigation"); ?>
<?php $this->load->view("admin/templates/sidebar"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Languages</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Languages</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6">
            <div class="card card-default">
              <div class="card-body">
                <table id="langtable" class="table table-bordered">
                  <thead class="bg-navy">
                  <tr>
                    <th>Language Name</th>
                    <th>Language Proficiency</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php if(!empty($language)){ ?>
                      <?php foreach($language as $l){ ?>
                        <tr>
                          <td><?php echo $l->name; ?></td>
                          <td><?php echo $l->proficiency; ?></td>
                          <td align="center"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#modallang<?php echo $l->id ?>"><i class="fas fa-edit"></i></button> <button type="button" class="btn btn-danger" onclick="deleteLang(<?php echo $l->id; ?>)"><i class="fas fa-trash"></i></button></td>
                        </tr>
                        <div class="modal fade" id="modallang<?php echo $l->id ?>">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                              <div class="modal-header bg-navy">
                                <h4 class="modal-title">Edit <?php echo $l->name; ?></h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true" class="text-white">&times;</span>
                                </button>
                              </div>
                              <form class="form-horizontal" autocomplete="on" id="languageformedit<?php echo $l->id ?>" name="languageformedit<?php echo $l->id ?>">
                                <div class="modal-body">
                                  <div class="row">
                                    <div class="col-lg-6">
                                        <label for="langname">Language Name</label>
                                        <input type="text" class="form-control" id="langnameedit<?php echo $l->id ?>" name="langnameedit<?php echo $l->id ?>" placeholder="Enter language name" value="<?php echo $l->name; ?>">
                                    </div>
                                    <div class="col-lg-6">
                                        <label for="langproficiency">Language Proficiency</label>
                                        <select class="form-control select2" id="langproficiencyedit<?php echo $l->id ?>" name="langproficiencyedit<?php echo $l->id ?>">
                                          <option value="-" class="selected">Select Proficiency</option>
                                          <?php if(!empty($proficiencylevel)){ ?>
                                            <?php foreach($proficiencylevel as $pl){ ?>
                                              <option value="<?php echo $pl->id; ?>" <?php if($pl->id == $l->proficiencyid){ echo "selected"; } ?>><?php echo $pl->proficiency; ?></option>
                                            <?php } ?>
                                          <?php } ?>
                                        </select>
                                    </div>
                                    <!-- <div class="col-lg-2 mt-4 pt-2">                    
                                      <div class="float-right"><a href="#" class="btn btn-success btn-flat add-btn"><i class="fas fa-plus"></i></a></div>
                                    </div> -->
                                  </div> <!-- /end row -->
                                </div>
                                <div class="modal-footer justify-content-between">
                                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn bg-navy" onclick="editLang(<?php echo $l->id ?>)">Update</button>
                                </div>
                              </form>
                            </div>
                            <!-- /.modal-content -->
                          </div>
                          <!-- /.modal-dialog -->
                        </div>
                      <?php } ?>
                    <?php } else { ?>
                      <tr>
                        <td colspan="3" align="center">Uh Ohh. Please insert 1 language first.</td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                      </tr>
                    <?php } ?>                    
                  </tbody>
                </table>
              </div>
            </div>          
          </div>
          <div class="col-lg-6">
            <div class="card card-navy">
              <div class="card-header">
                <h3 class="card-title">Languages</h3>
              </div>
              <form class="form-horizontal" autocomplete="on" id="languageform" name="languageform">
                <div class="card-body">
                  <div class="lang-wrapper">
                    <div class="row">
                      <div class="col-lg-6">
                          <label for="langname">Language Name</label>
                          <input type="text" class="form-control" id="langname" name="langname" placeholder="Enter language name">
                      </div>
                      <div class="col-lg-6">
                          <label for="langproficiency">Language Proficiency</label>
                          <select class="form-control select2" id="langproficiency" name="langproficiency">
                            <option value="-" class="selected">Select Proficiency</option>
                            <?php if(!empty($proficiencylevel)){ ?>
                              <?php foreach($proficiencylevel as $pl){ ?>
                                <option value="<?php echo $pl->id; ?>"><?php echo $pl->proficiency; ?></option>
                              <?php } ?>
                            <?php } ?>
                          </select>
                      </div>
                      <!-- <div class="col-lg-2 mt-4 pt-2">                    
                        <div class="float-right"><a href="#" class="btn btn-success btn-flat add-btn"><i class="fas fa-plus"></i></a></div>
                      </div> -->
                    </div> <!-- /end row -->
                  </div> <!-- /end lang wrapper -->
                </div> <!-- /end card body -->
                <div class="card-footer">
                  <button type="submit" class="btn bg-navy btn-flat">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div> <!-- /end row -->
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/templates/footer"); ?>