<?php $this->load->view("admin/templates/header"); ?>
<?php $this->load->view("admin/templates/navigation"); ?>
<?php $this->load->view("admin/templates/sidebar"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Educations</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Educations</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card card-default">
              <div class="card-body">
                <table id="edutable" class="table table-bordered">
                  <thead class="bg-navy">
                  <tr>
                    <th>Education Name</th>
                    <th>Institution Name</th>
                    <th>Year</th>
                    <th>Level</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php if(!empty($education)){ ?>
                      <?php foreach($education as $e){ ?>
                        <tr>
                          <td><?php echo $e->name; ?></td>
                          <td><?php echo $e->institution; ?></td>
                          <td align="center"><?php echo $e->yearstart." - ".$e->yearend; ?></td>
                          <td><?php echo $e->level; ?></td>
                          <td align="center"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#modaledu<?php echo $e->id ?>"><i class="fas fa-edit"></i></button> <button type="button" class="btn btn-danger" onclick="deleteEdu(<?php echo $e->id; ?>)"><i class="fas fa-trash"></i></button></td>
                        </tr>
                        <div class="modal fade" id="modaledu<?php echo $e->id ?>">
                          <div class="modal-dialog modal-xl">
                            <div class="modal-content">
                              <div class="modal-header bg-navy">
                                <h4 class="modal-title">Edit <?php echo $e->name; ?></h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true" class="text-white">&times;</span>
                                </button>
                              </div>
                              <form class="form-horizontal" autocomplete="on" id="educationformedit<?php echo $e->id ?>" name="educationformedit<?php echo $e->id ?>">
                                <div class="modal-body">
                                  <div class="row">
                                    <div class="col-lg-3">
                                        <label for="eduname">Education Name</label>
                                        <input type="text" class="form-control" id="edunameedit<?php echo $e->id ?>" name="edunameedit<?php echo $e->id ?>" placeholder="Enter education name" value="<?php echo $e->name; ?>">
                                    </div>
                                    <div class="col-lg-3">
                                        <label for="eduplace">Institution Name</label>
                                        <input type="text" class="form-control" id="eduplaceedit<?php echo $e->id ?>" name="eduplaceedit<?php echo $e->id ?>" placeholder="Enter institution name" value="<?php echo $e->institution; ?>">
                                    </div>
                                    <div class="col-lg-2">
                                        <label for="eduyearstart">Year Start</label>
                                        <input type="text" class="form-control" id="eduyearstartedit<?php echo $e->id ?>" name="eduyearstartedit<?php echo $e->id ?>" placeholder="Enter year start" value="<?php echo $e->yearstart; ?>">
                                    </div>
                                    <div class="col-lg-2">
                                        <label for="eduyearend">Year End</label>
                                        <input type="text" class="form-control" id="eduyearendedit<?php echo $e->id ?>" name="eduyearendedit<?php echo $e->id ?>" placeholder="Enter year end" value="<?php echo $e->yearend; ?>">
                                    </div>
                                    <div class="col-lg-2">
                                        <label for="edulevel">Level</label>
                                        <select class="form-control select2" id="eduleveledit<?php echo $e->id ?>" name="eduleveledit<?php echo $e->id ?>">
                                          <option value="-" selected>Select level</option>
                                          <?php if(!empty($level)){ ?>
                                            <?php foreach($level as $l){ ?>
                                              <option value="<?php echo $l->id; ?>" <?php if($l->id == $e->levelid){echo "selected"; } ?>><?php echo $l->level; ?></option>
                                            <?php } ?>
                                          <?php } ?>
                                        </select>
                                    </div>
                                    <!-- <div class="col-lg-1 mt-4 pt-2">                    
                                      <div class="float-right"><a href="#" class="btn btn-success btn-flat add-btn"><i class="fas fa-plus"></i></a></div>
                                    </div> -->
                                  </div> <!-- /end row -->
                                </div>
                                <div class="modal-footer justify-content-between">
                                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn bg-navy" onclick="editEdu(<?php echo $e->id ?>)">Update</button>
                                </div>
                              </form>
                            </div>
                            <!-- /.modal-content -->
                          </div>
                          <!-- /.modal-dialog -->
                        </div>
                      <?php } ?>
                    <?php }else{ ?>
                      <tr>
                        <td colspan="5" align="center">Uh Ohh. Please insert 1 education first.</td>
                        <td style="display: none"></td>
                        <td style="display: none"></td>
                        <td style="display: none"></td>
                        <td style="display: none"></td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div> <!-- /end row -->
        <div class="row">
          <div class="col-lg-12">
            <div class="card card-navy">
              <div class="card-header">
                <h3 class="card-title">Educations</h3>
              </div>
              <form class="form-horizontal" autocomplete="on" id="educationform" name="educationform">
                <div class="card-body">
                  <div class="edu-wrapper">
                    <div class="row">
                      <div class="col-lg-3">
                          <label for="eduname">Education Name</label>
                          <input type="text" class="form-control" id="eduname" name="eduname" placeholder="Enter education name">
                      </div>
                      <div class="col-lg-3">
                          <label for="eduplace">Institution Name</label>
                          <input type="text" class="form-control" id="eduplace" name="eduplace" placeholder="Enter institution name">
                      </div>
                      <div class="col-lg-2">
                          <label for="eduyearstart">Year Start</label>
                          <input type="text" class="form-control" id="eduyearstart" name="eduyearstart" placeholder="Enter year start">
                          <span class="fas fa-calendar-alt datepicker"></span>
                      </div>
                      <div class="col-lg-2">
                          <label for="eduyearend">Year End</label>
                          <input type="text" class="form-control" id="eduyearend" name="eduyearend" placeholder="Enter year end">
                          <span class="fas fa-calendar-alt datepicker"></span>
                      </div>
                      <div class="col-lg-2">
                          <label for="edulevel">Level</label>
                          <select class="form-control select2" id="edulevel" name="edulevel">
                            <option value="-" selected>Select level</option>
                            <?php if(!empty($level)){ ?>
                              <?php foreach($level as $l){ ?>
                                <option value="<?php echo $l->id; ?>"><?php echo $l->level; ?></option>
                              <?php } ?>
                            <?php } ?>
                          </select>
                      </div>
                      <!-- <div class="col-lg-1 mt-4 pt-2">                    
                        <div class="float-right"><a href="#" class="btn btn-success btn-flat add-btn"><i class="fas fa-plus"></i></a></div>
                      </div> -->
                    </div> <!-- /end row -->
                  </div> <!-- /end edu wrapper -->
                </div> <!-- /end card body -->
                <div class="card-footer">
                  <button type="submit" class="btn bg-navy btn-flat">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/templates/footer"); ?>