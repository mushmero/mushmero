<?php $this->load->view("admin/templates/header"); ?>
<?php $this->load->view("admin/templates/navigation"); ?>
<?php $this->load->view("admin/templates/sidebar"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Personal</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Personal</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
        	<div class="col-lg-4 col-sm-12">
        		<!-- Profile Image -->
	            <div class="card card-navy card-outline">
	              <div class="card-body box-profile">
	                <div class="text-center">
	                  <img class="profile-user-img img-fluid img-circle"
	                       src="<?php echo empty($picture) ? getenv("APP_URL")."/".("assets/images/no_image.png") : $picture; ?>"
	                       alt="<?php echo empty($altname) ? "No Name" : $altname; ?>">
	                </div>

	                <h3 class="profile-username text-center"><?php echo empty($fullname) ? "Fullname" : $fullname; ?></h3>

	                <p class="text-muted text-center"><?php echo empty($tagline) ? "Tagline" : $tagline; ?></p>

	                <ul class="list-group list-group-unbordered mb-4">
	                  <li class="list-group-item">
	                    <b>Email</b> <a href="<?php echo empty($email) ? "#" : "mailto:".$email; ?>" class="float-right"><?php echo empty($email) ? "Email Unavailable" : $email; ?></a>
	                  </li>
	                  <li class="list-group-item">
	                    <b>Phone</b> <a href="<?php echo empty($phone) ? "#" : "tel:".$phone; ?>" class="float-right"><?php echo empty($phone) ? "Phone No Unavailable" : $phone; ?></a>
	                  </li>
	                  <li class="list-group-item">
	                    <b>Website</b> <a href="<?php echo empty($website) ? "#" : $website; ?>" target="_blank" class="float-right"><?php echo empty($website) ? "Website Unavailable" : $website; ?></a>
	                  </li>
	                </ul>
	              </div>
	              <!-- /.card-body -->
	            </div>
	            <!-- /.card -->
        	</div>
        	<div class="col-lg-8 col-sm-12">
        		<div class="card card-outline card-navy">
        			<div class="card-body">
        				<table class="table table-bordered" id="personalTable">
		        			<thead class="bg-navy">
		        				<tr>
		        					<th>Profile</th>
		        					<th>Status</th>
		        					<th>Action</th>
		        				</tr>
		        			</thead>
		        			<tbody>
		        				<?php if(!empty($user)){ ?>
		        					<?php foreach($user as $u){ ?>
		        						<tr>
			        						<td>
			        							<div class="row">
			        								<div class="col-2">
			        									<img class="profile-user-img img-fluid img-circle img-100" src="<?php echo getenv("APP_URL").$u->filepath.$u->filename; ?>" alt="<?php echo $u->altname; ?>">
			        								</div>
			        								<div class="col-10">
			        									<?php echo $u->fullname; ?> (<?php echo $u->tagline; ?>)<br><i class="fas fa-mobile-alt"></i> <?php echo str_replace(' ','',$u->phone); ?> <i class="fas fa-envelope"></i> <?php echo $u->email; ?><br><?php if(!empty($u->website)){ echo '<i class="fas fa-globe"></i> '.$u->website;} ?>
			        								</div>
			        							</div>			        							
			        						</td>
			        						<td align="center">
			        							<div class="form-group">
					                    <div class="custom-control custom-switch">
					                      <input type="checkbox" class="custom-control-input" id="activeSwitch<?php echo $u->id; ?>" name="activeSwitch<?php echo $u->id; ?>" <?php if($u->status == 1){echo "checked";} ?> onchange="switchAjax(<?php echo $u->id; ?>)">
					                      <label class="custom-control-label" for="activeSwitch<?php echo $u->id; ?>"><?php echo ($u->status != 0) ? "Active" : "Inactive"; ?></label>
					                    </div>
					                  </div>
			        						</td>
			        						<td align="center"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalProfile<?php echo $u->id ?>" onclick="editProfile(<?php echo $u->id ?>)"><i class="fas fa-edit"></i></button> <button type="button" class="btn btn-danger" onclick="deleteProfile(<?php echo $u->id; ?>)"><i class="fas fa-trash"></i></button></td>
			        					</tr>
                        <div class="modal fade" id="modalProfile<?php echo $u->id ?>">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                              <div class="modal-header bg-navy">
                                <h4 class="modal-title">Edit <?php echo $u->fullname; ?></h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true" class="text-white">&times;</span>
                                </button>
                              </div>
                              <form class="form-horizontal" autocomplete="on" id="profileformedit<?php echo $u->id ?>" name="profileformedit<?php echo $u->id ?>" enctype="multipart/form-data">
                                <div class="modal-body">
									              	<div class="row">
									              		<div class="col-lg-6">
																			<div class="form-group">
																			    <label for="uploadprofilephotoedit<?php echo $u->id ?>">Upload Photo</label>
																			    <div class="input-group">
																			      <div class="custom-file">
																			        <input type="file" class="custom-file-input" id="uploadprofilephotoedit<?php echo $u->id ?>" name="uploadprofilephotoedit<?php echo $u->id ?>" accept="image/jpeg, image/png">
																			        <label class="custom-file-label" for="uploadprofilephotoedit<?php echo $u->id ?>">Choose photo</label>
																			      </div>
																			    </div>
																			</div>
																			<div class="form-group">
																			    <label for="fullname">FullName</label>
																			    <input type="text" class="form-control" id="fullnameedit<?php echo $u->id ?>" name="fullnameedit<?php echo $u->id ?>" placeholder="Enter fullname" value="<?php echo $u->fullname ?>" required>
																			</div>
																			<div class="form-group">
																			    <label for="tagline">Tagline</label>
																			    <input type="text" class="form-control" id="taglineedit<?php echo $u->id ?>" name="taglineedit<?php echo $u->id ?>" placeholder="Enter tagline" value="<?php echo $u->tagline ?>" required>
																			</div>      			
									              		</div>
									              		<div class="col-lg-6">
																			<div class="form-group">
																			    <label for="email">Email address</label>
																			    <input type="email" class="form-control" id="emailedit<?php echo $u->id ?>" name="emailedit<?php echo $u->id ?>" placeholder="Enter email" value="<?php echo $u->email ?>" required>
																			</div>
																			<div class="form-group">
																			    <label for="phone">Phone</label>
																			    <input type="text" class="form-control" id="phoneedit<?php echo $u->id ?>" name="phoneedit<?php echo $u->id ?>" placeholder="Enter phone" value="<?php echo $u->phone ?>" required>
																			</div>
																			<div class="form-group">
																			    <label for="website">Website</label>
																			    <input type="text" class="form-control" id="websiteedit<?php echo $u->id ?>" name="websiteedit<?php echo $u->id ?>" placeholder="Enter website" value="<?php echo $u->website ?>">
																			</div>            			
									              		</div>
									              	</div>
                                </div>
                                <div class="modal-footer justify-content-between">
                                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                  <button type="submit" class="btn bg-navy">Update</button>
                                </div>
                              </form>
                            </div>
                            <!-- /.modal-content -->
                          </div>
                          <!-- /.modal-dialog -->
                        </div>
		        					<?php } ?>
		        				<?php } else { ?>
			        				<tr>
			        					<td colspan="3" align="center">Uh No. Key in your profile first</td>
			        					<td style="display: none"></td>
			        					<td style="display: none"></td>
			        				</tr>
			        			<?php } ?>
		        			</tbody>
		        		</table>
        			</div>
        		</div>        		
        	</div>
        	<div class="col-12">
        		<div class="card card-navy">
	              <div class="card-header">
	              	<h3 class="card-title">Informations</h3>
	              </div><!-- /.card-header -->
	              <form autocomplete="on" class="form" id="profileform" name="profileform" enctype="multipart/form-data">
		              <div class="card-body">
		              	<div class="row">
		              		<div class="col-lg-6">
												<div class="form-group">
												    <label for="uploadprofilephoto">Upload Photo</label>
												    <div class="input-group">
												      <div class="custom-file">
												        <input type="file" class="custom-file-input" id="uploadprofilephoto" name="uploadprofilephoto" accept="image/jpeg, image/png" required>
												        <label class="custom-file-label" for="uploadprofilephoto">Choose photo</label>
												      </div>
												    </div>
												</div>
												<div class="form-group">
												    <label for="fullname">FullName</label>
												    <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Enter fullname" required>
												</div>
												<div class="form-group">
												    <label for="tagline">Tagline</label>
												    <input type="text" class="form-control" id="tagline" name="tagline" placeholder="Enter tagline" required>
												</div>      			
		              		</div>
		              		<div class="col-lg-6">
												<div class="form-group">
												    <label for="email">Email address</label>
												    <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" required>
												</div>
												<div class="form-group">
												    <label for="phone">Phone</label>
												    <input type="text" class="form-control" id="phone" name="phone" placeholder="Enter phone" required>
												</div>
												<div class="form-group">
												    <label for="website">Website</label>
												    <input type="text" class="form-control" id="website" name="website" placeholder="Enter website">
												</div>            			
		              		</div>
		              	</div>
			          	</div><!-- /.card-body -->
				          <div class="card-footer">
		                 <button type="submit" class="btn bg-navy btn-flat"><i class="fas fa-check"></i> Save</button>
		              </div>
		            </form>
		        </div>
	            <!-- /.card -->
        	</div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/templates/footer"); ?>