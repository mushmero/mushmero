<?php $this->load->view("admin/templates/header"); ?>
<?php $this->load->view("admin/templates/navigation"); ?>
<?php $this->load->view("admin/templates/sidebar"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Skills</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Skills</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">        
        <div class="row">
          <div class="col-lg-6">
            <div class="card card-default">
              <div class="card-body">
                <table id="skilltable" class="table table-bordered">
                  <thead class="bg-navy">
                  <tr>
                    <th>Skill</th>
                    <th>Percentage</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php if(!empty($skills)){ ?>
                      <?php foreach($skills as $s){ ?>
                        <tr>
                          <td><?php echo $s->name; ?></td>
                          <td><?php echo $s->percentage; ?></td>
                          <td align="center"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalskill<?php echo $s->id ?>"><i class="fas fa-edit"></i></button> <button type="button" class="btn btn-danger" onclick="deleteSkill(<?php echo $s->id; ?>)"><i class="fas fa-trash"></i></button></td>
                        </tr>
                        <div class="modal fade" id="modalskill<?php echo $s->id ?>">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                              <div class="modal-header bg-navy">
                                <h4 class="modal-title">Edit <?php echo $s->name; ?></h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true" class="text-white">&times;</span>
                                </button>
                              </div>
                              <form class="form-horizontal" autocomplete="on" id="skillformedit<?php echo $s->id ?>" name="skillformedit<?php echo $s->id ?>">
                                <div class="modal-body">
                                  <div class="row">
                                    <div class="col-lg-6">
                                        <label for="skillname">Skill</label>
                                        <input type="text" class="form-control" id="skillnameedit<?php echo $s->id ?>" name="skillnameedit<?php echo $s->id ?>" placeholder="Enter skill" value="<?php echo $s->name; ?>">
                                    </div>
                                    <div class="col-lg-6">
                                        <label for="skillpercentage">Percentage</label>
                                        <input type="number" class="form-control" id="skillpercentageedit<?php echo $s->id ?>" name="skillpercentageedit<?php echo $s->id ?>" placeholder="Enter skill percentage" value="<?php echo $s->percentage; ?>" max="100">
                                    </div>
                                  </div> <!-- /end row -->
                                </div>
                                <div class="modal-footer justify-content-between">
                                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn bg-navy" onclick="editSkill(<?php echo $s->id ?>)">Update</button>
                                </div>
                              </form>
                            </div>
                            <!-- /.modal-content -->
                          </div>
                          <!-- /.modal-dialog -->
                        </div>
                      <?php } ?>
                    <?php } else { ?>
                      <tr>
                        <td colspan="3" align="center">Uh Ohh. Please insert 1 skill first.</td>
                        <td style="display: none"></td>
                        <td style="display: none"></td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>           
          </div>
          <div class="col-lg-6">
            <div class="card card-navy">
              <div class="card-header">
                <h3 class="card-title">Skills</h3>
              </div>
              <form class="form-horizontal" autocomplete="on" id="skillform" name="skillform">
                <div class="card-body">
                  <div class="skill-wrapper">
                    <div class="row">
                      <div class="col-lg-6">
                          <label for="skillname">Skill</label>
                          <input type="text" class="form-control" id="skillname" name="skillname" placeholder="Enter skill">
                      </div>
                      <div class="col-lg-6">
                          <label for="skillpercentage">Percentage</label>
                          <input type="number" class="form-control" id="skillpercentage" name="skillpercentage" placeholder="Enter skill percentage" max="100">
                      </div>
                      <!-- <div class="col-lg-1 mt-4 pt-2">                    
                        <div class="float-right"><a href="#" class="btn btn-success btn-flat add-btn"><i class="fas fa-plus"></i></a></div>
                      </div> -->
                    </div> <!-- /end row -->
                  </div>
                </div> <!-- /end card body -->
                <div class="card-footer">
                  <button type="submit" class="btn bg-navy btn-flat">Save</button>
                </div>
              </form>
            </div>
          </div>
        </div> <!-- /end row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/templates/footer"); ?>