<?php $this->load->view("admin/templates/header"); ?>
<?php $this->load->view("admin/templates/navigation"); ?>
<?php $this->load->view("admin/templates/sidebar"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Experiences</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Experiences</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">        
        <div class="row">
          <div class="col-lg-12">
            <div class="card card-default">
              <div class="card-body">
                <table id="exptable" class="table table-bordered">
                  <thead class="bg-navy">
                  <tr>
                    <th>Experience</th>
                    <th>Experience Length</th>
                    <th>Employer</th>
                    <th>Description</th>
                    <th width="100px">Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php if(!empty($experiences)){ ?>
                      <?php foreach($experiences as $e){ ?>
                        <tr>
                          <td><?php echo $e->name; ?></td>
                          <td><?php echo $e->startdate; ?> - <?php echo ($e->enddate != 'current') ? $e->enddate : ucfirst($e->enddate); ?></td>
                          <td><?php echo $e->employer; ?></td>
                          <td><?php echo $e->description; ?></td>
                          <td align="center"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalexp<?php echo $e->id ?>" onclick="popupExp(<?php echo $e->id ?>)"><i class="fas fa-edit"></i></button> <button type="button" class="btn btn-danger" onclick="deleteExperience(<?php echo $e->id; ?>)"><i class="fas fa-trash"></i></button></td>
                        </tr>
                        <div class="modal fade" id="modalexp<?php echo $e->id ?>">
                          <div class="modal-dialog modal-xl">
                            <div class="modal-content">
                              <div class="modal-header bg-navy">
                                <h4 class="modal-title">Edit <?php echo $e->name; ?></h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true" class="text-white">&times;</span>
                                </button>
                              </div>
                              <form class="form-horizontal" autocomplete="on" id="expformedit<?php echo $e->id ?>" name="expformedit<?php echo $e->id ?>">
                                <div class="modal-body">
                                  <div class="row">
                                    <div class="col-4">
                                      <div class="row">
                                        <div class="col-12">
                                            <label for="expname">Experience</label>
                                            <input type="text" class="form-control" id="expnameedit<?php echo $e->id ?>" name="expnameedit<?php echo $e->id ?>" placeholder="Enter experience" value="<?php echo $e->name; ?>">
                                        </div>
                                        <div class="col-12">
                                            <label for="explength">Experience Length</label>
                                              <!-- <input type="text" class="form-control float-right" id="explengthedit<?php echo $e->id ?>" name="explengthedit<?php echo $e->id ?>" value="<?php echo $e->startdate." - ".$e->enddate; ?>">
                                              <span class="fas fa-calendar-alt datepicker"></span> -->
                                            <div class="form-group row" id="explengthstartedit<?php echo $e->id ?>">
                                              <label for="explengthstartedit<?php echo $e->id ?>">From</label>
                                              <div class="col-10">
                                                <div class="input-group date" id="explengthstartedit<?php echo $e->id ?>" data-target-input="nearest">
                                                  <input type="text" class="form-control datetimepicker-input" id="explengthstartedit<?php echo $e->id ?>" data-target="#explengthstartedit<?php echo $e->id ?>" data-toggle="datetimepicker" value="<?php echo $e->startdate; ?>"/>                                
                                                  <div class="input-group-append">
                                                      <div class="input-group-text"><span class="fas fa-calendar-alt"></span></div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="form-group row" id="explengthendedit<?php echo $e->id ?>">
                                              <label for="explengthendedit<?php echo $e->id ?>">Until</label>
                                              <div class="col-10">
                                                <div class="input-group date" id="explengthendedit<?php echo $e->id ?>" data-target-input="nearest">
                                                  <input type="text" class="form-control datetimepicker-input" id="explengthendedit<?php echo $e->id ?>" data-target="#explengthendedit<?php echo $e->id ?>" data-toggle="datetimepicker" value="<?php echo ($e->enddate != 'current') ? $e->enddate : date('m/Y'); ?>"/>                                
                                                  <div class="input-group-append">
                                                      <div class="input-group-text"><span class="fas fa-calendar-alt"></span></div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          <div class="form-group">
                                            <div class="form-check">
                                              <input type="checkbox" class="form-check-input" id="currentEmpedit<?php echo $e->id ?>"name="currentEmpedit<?php echo $e->id ?>"> <label class="form-check-label"> Currently working here</label>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="col-12">
                                            <label for="expemployer">Employer</label>
                                            <input type="text" class="form-control" id="expemployeredit<?php echo $e->id ?>" name="expemployeredit<?php echo $e->id ?>" placeholder="Enter your employer" value="<?php echo $e->employer ?>">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-8">
                                      <div class="row">
                                        <div class="col-12">
                                            <label for="expdesc">Description</label>
                                            <textarea class="form-control" id="expdescedit<?php echo $e->id ?>" name="expdescedit<?php echo $e->id ?>"><?php echo $e->description ?></textarea>
                                        </div>
                                      </div>
                                    </div>
                                  </div> <!-- /end row -->
                                </div>
                                <div class="modal-footer justify-content-between">
                                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn bg-navy" id="expmodalsubmit<?php echo $e->id ?>" name="expmodalsubmit<?php echo $e->id ?>" >Update</button>
                                </div>
                              </form>
                            </div>
                            <!-- /.modal-content -->
                          </div>
                          <!-- /.modal-dialog -->
                        </div>
                      <?php } ?>
                    <?php } else { ?>
                      <tr>
                        <td colspan="5" align="center">Uh Ohh. Please insert 1 experience first.</td>
                        <td style="display: none"></td>
                        <td style="display: none"></td>
                        <td style="display: none"></td>
                        <td style="display: none"></td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>           
          </div>
        </div> <!-- /end row -->
        <div class="row">
          <div class="col-lg-12">
            <div class="card card-navy">
              <div class="card-header">
                <h3 class="card-title">Experiences</h3>
              </div>
              <form class="form-horizontal" autocomplete="on" id="expform" name="expform">
                <div class="card-body">
                  <div class="exp-wrapper">
                    <div class="row">
                      <div class="col-lg-3">
                          <label for="expname">Experience</label>
                          <input type="text" class="form-control" id="expname" name="expname" placeholder="Enter experience">
                      </div>
                      <div class="col-lg-2">
                        <label for="explength">Experience Length</label>
                        <div class="form-group row" id="explengthstart">
                          <label for="explengthstart">From</label>
                          <div class="col-10">
                            <div class="input-group date" id="explengthstart" data-target-input="nearest">
                              <input type="text" class="form-control datetimepicker-input" id="explengthstart" data-target="#explengthstart" data-toggle="datetimepicker"/>                                
                              <div class="input-group-append">
                                  <div class="input-group-text"><span class="fas fa-calendar-alt"></span></div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="form-group row" id="explengthend">
                          <label for="explengthend">Until</label>
                          <div class="col-10">
                            <div class="input-group date" id="explengthend" data-target-input="nearest">
                              <input type="text" class="form-control datetimepicker-input" id="explengthend" data-target="#explengthend" data-toggle="datetimepicker"/>                                
                              <div class="input-group-append">
                                  <div class="input-group-text"><span class="fas fa-calendar-alt"></span></div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="currentEmp"name="currentEmp"> <label class="form-check-label"> Currently working here</label>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-2">
                          <label for="expemployer">Employer</label>
                          <input type="text" class="form-control" id="expemployer" name="expemployer" placeholder="Enter your employer">
                      </div>
                      <div class="col-lg-5">
                          <label for="expdesc">Description</label>
                          <textarea class="form-control" id="expdesc" name="expdesc"></textarea>
                      </div>
                    </div> <!-- /end row -->
                  </div> <!-- /end social wrapper -->
                </div> <!-- /end card body -->
                <div class="card-footer">
                  <button type="submit" class="btn bg-navy btn-flat">Save</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/templates/footer"); ?>