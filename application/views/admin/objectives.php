<?php $this->load->view("admin/templates/header"); ?>
<?php $this->load->view("admin/templates/navigation"); ?>
<?php $this->load->view("admin/templates/sidebar"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Objectives</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Objectives</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card card-navy">
              <div class="card-header">
                <h3 class="card-title">Objectives</h3>
              </div>
              <form class="form" autocomplete="on" id="objectiveform" name="objectiveform">
                <div class="card-body">
                  <div class="row">
                    <div class="col-lg-12">
                      <h4>Current:</h4>
                      <?php if(empty($objective)){ ?>
                        <p align="justify"><i>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</i></p>
                      <?php } else { ?>
                        <p align="justify"><i><?php echo $objective->data; ?></i></p>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <?php if(!empty($objective)){ ?>
                          <input type="hidden" id="objectiveid" name="objectiveid" value="<?php echo $objective->id ?>">
                        <?php } ?>
                        <label for="objective">Objective</label>
                        <textarea class="form-control" id="objective" name="objective" rows="5">Enter your resume objective</textarea>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn bg-navy btn-flat">Save</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/templates/footer"); ?>