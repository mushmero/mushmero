<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Resume of Muhd Amirul Hakim Zailan.">
    <meta name="author" content="Amirul Hakim">
	<title><?php echo getenv("APP_NAME"); ?></title>
    <link rel="shortcut icon" href="<?php echo getenv("APP__URL").("assets/images/favicon.ico"); ?>">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	<link rel="stylesheet" type="text/css" href="<?php echo getenv("APP_URL").("vendor/twbs/bootstrap/dist/css/bootstrap.css"); ?>">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
    <!-- Custom template CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo getenv("APP_URL").("assets/css/colors.css"); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo getenv("APP_URL").("assets/css/pdf.css"); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo getenv("APP_URL").("assets/css/template.css"); ?>">
</head>