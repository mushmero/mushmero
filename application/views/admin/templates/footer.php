<?php $uri = $this->uri->segment(1); ?>
<?php if ($uri != 'login' && $uri != 'register' && $uri != 'logout' && $uri != 'noaccess'){ ?>
  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">      
      <strong> Version <?php echo getenv("APP_VERSION"); ?> </strong>
    </div>
    <!-- Default to the left -->
    <?php echo date("Y"); ?><a href="https://mushmero.com" target="_blank"> <i class="fas fa-heart"></i> Mushmero</a>. All rights reserved.
  </footer>
</div>
<?php } ?>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo getenv("APP_URL").('vendor/twbs/bootstrap/dist/js/bootstrap.bundle.js'); ?>"></script>
<!-- daterange -->
<script src="<?php echo getenv("APP_URL").('vendor/almasaeed2010/adminlte/plugins/moment/moment.min.js') ?>"></script>
<script src="<?php echo getenv("APP_URL").('vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') ?>"></script>
<script src="<?php echo getenv("APP_URL").('vendor/almasaeed2010/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.js') ?>"></script>
<!-- JQuery validate -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js" integrity="sha512-37T7leoNS06R80c8Ulq7cdCDU5MNQBwlYoy1TX/WUsLFC2eYNqtKlV0QjH7r8JpG/S0GUMZwebnVFLPd6SU5yg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<!-- Datatables BS4 -->
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.24/r-2.2.7/sb-1.0.1/datatables.min.js"></script>
<!-- CodeMirror -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.62.0/codemirror.min.js" integrity="sha512-i9pd5Q6ntCp6LwSgAZDzsrsOlE8SN+H5E0T5oumSXWQz5l1Oc4Kb5ZrXASfyjjqtc6Mg6xWbu+ePbbmiEPJlDg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.62.0/mode/xml/xml.min.js" integrity="sha512-XPih7uxiYsO+igRn/NA2A56REKF3igCp5t0W1yYhddwHsk70rN1bbbMzYkxrvjQ6uk+W3m+qExHIJlFzE6m5eg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<!-- Summernote editor -->
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
<!-- SweetAlert2 -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.0.16/dist/sweetalert2.all.min.js"></script>
<!-- bs-custom-file-input -->
<script src="<?php echo getenv("APP_URL").('vendor/almasaeed2010/adminlte/plugins/bs-custom-file-input/bs-custom-file-input.min.js'); ?>"></script>
<!-- select2js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.full.min.js"></script>
<!-- RaphaelJS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.3.0/raphael.min.js" integrity="sha512-tBzZQxySO5q5lqwLWfu8Q+o4VkTcRGOeQGVQ0ueJga4A1RKuzmAu5HXDOXLEjpbKyV7ow9ympVoa6wZLEzRzDg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<!-- JQuery-Mapael -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mapael/2.2.0/js/jquery.mapael.min.js" integrity="sha512-+iXNzFArGbqxdmbClb1f6MKIiZASR7H8ep6rS1ZFn2I7tRX400ApvS0nsG8/v1+F7RoGU2shMDTl/gZ5lZF1iw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mapael/2.2.0/js/maps/world_countries.min.js" integrity="sha512-QGmaAYAgVbqkUFtLzXmKhaP52gAePFwe50bNkE0SbflQ4sm6mmdvufVnKnb5CNgRP2nW4ondofrZ++1dTEAJ4Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<!-- AdminLTE App -->
<script src="<?php echo getenv("APP_URL").('vendor/almasaeed2010/adminlte/dist/js/adminlte.js') ?>"></script>
<!-- <script src="<?php //echo getenv("APP_URL").('vendor/almasaeed2010/adminlte/dist/js/demo.js') ?>"></script> -->
<script src="<?php echo getenv("APP_URL").('assets/js/custom.js') ?>"></script>
<script src="<?php echo getenv("APP_URL").('assets/js/mapael_data.js') ?>"></script>
</body>
</html>