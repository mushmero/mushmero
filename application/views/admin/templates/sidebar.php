  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="<?php echo getenv("APP_URL")."/".("vendor/almasaeed2010/adminlte/dist/img/AdminLTELogo.png") ?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><?php echo getenv("APP_NAME") ?></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo empty($picture) ? getenv("APP_URL")."/".("assets/images/no_image.png") : $picture; ?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a class="d-block"><?php echo empty($fullname) ? "Short Name" : $fullname; ?></a>       
        </div>
        <span class="info">
          <a href="<?php echo getenv("APP_URL").("logout"); ?>" class="btn bg-danger btn-tool"><i class="fas fa-sign-out-alt"></i> Logout</a>
        </span>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="<?php echo getenv("APP_URL")."admin"; ?>" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i> 
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo getenv("APP_URL")."setting"; ?>" class="nav-link">
              <i class="nav-icon fas fa-cog"></i> 
              <p>Setting</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo getenv("APP_URL"); ?>" class="nav-link" target="_blank">
              <i class="nav-icon fas fa-file"></i> 
              <p>Resume</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo getenv("APP_URL")."template/exportPDF"; ?>" class="nav-link" target="_blank">
              <i class="nav-icon fas fa-file-pdf"></i> 
              <p>Generate PDF</p>
            </a>
          </li>
          <li class="nav-header">PROFILE</li>
          <li class="nav-item">
            <a href="<?php echo getenv("APP_URL")."personal"; ?>" class="nav-link">
              <i class="nav-icon fas fa-user"></i> 
              <p>Personal</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo getenv("APP_URL")."socials"; ?>" class="nav-link">
              <i class="nav-icon fas fa-users"></i> 
              <p>Socials</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo getenv("APP_URL")."educations"; ?>" class="nav-link">
              <i class="nav-icon fas fa-university"></i> 
              <p>Educations</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo getenv("APP_URL")."languages"; ?>" class="nav-link">
              <i class="nav-icon fas fa-language"></i> 
              <p>Languages</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo getenv("APP_URL")."interests"; ?>" class="nav-link">
              <i class="nav-icon fas fa-thumbs-up"></i> 
              <p>Interests</p>
            </a>
          </li>
          <li class="nav-header">INFO</li>
          <li class="nav-item">
            <a href="<?php echo getenv("APP_URL")."objectives"; ?>" class="nav-link">
              <i class="nav-icon fas fa-bullseye"></i> 
              <p>Objectives</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo getenv("APP_URL")."experiences"; ?>" class="nav-link">
              <i class="nav-icon fas fa-briefcase"></i> 
              <p>Experiences</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo getenv("APP_URL")."projects"; ?>" class="nav-link">
              <i class="nav-icon fas fa-archive"></i> 
              <p>Projects</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo getenv("APP_URL")."skills"; ?>" class="nav-link">
              <i class="nav-icon fas fa-rocket"></i> 
              <p>Skills</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>