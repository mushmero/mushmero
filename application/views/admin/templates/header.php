<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<title><?php echo getenv("APP_NAME"); ?></title>
    <link rel="shortcut icon" href="<?php echo getenv("APP__URL").("assets/images/favicon.ico"); ?>">

	<!-- Google Font: Source Sans Pro -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	<link rel="stylesheet" href="<?php echo getenv("APP_URL").("vendor/twbs/bootstrap/dist/css/bootstrap.min.css"); ?>" type="text/css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
	<!-- <link rel="stylesheet" href="<?php //echo getenv("APP_URL").('vendor/almasaeed2010/adminlte/plugins/jqvmap/jqvmap.min.css') ?>" type="text/css"> -->
	<link rel="stylesheet" href="<?php echo getenv("APP_URL").('vendor/almasaeed2010/adminlte/dist/css/adminlte.css') ?>" type="text/css">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="<?php echo getenv("APP_URL").('vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.css') ?>" type="text/css">
	<link rel="stylesheet" href="<?php echo getenv("APP_URL").('vendor/almasaeed2010/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.css') ?>" type="text/css">
	<!-- Datatables BS4 -->
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.24/r-2.2.7/sb-1.0.1/datatables.min.css"/>
	<!-- codemirror -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.62.0/codemirror.min.css" integrity="sha512-xIf9AdJauwKIVtrVRZ0i4nHP61Ogx9fSRAkCLecmE2dL/U8ioWpDvFCAy4dcfecN72HHB9+7FfQj3aiO68aaaw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.62.0/theme/monokai.min.css" integrity="sha512-R6PH4vSzF2Yxjdvb2p2FA06yWul+U0PDDav4b/od/oXf9Iw37zl10plvwOXelrjV2Ai7Eo3vyHeyFUjhXdBCVQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<!-- Summernote editor -->
	<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
	<!-- select2js -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" integrity="sha512-kq3FES+RuuGoBW3a9R2ELYKRywUEQv0wvPTItv3DSGqjpbNtGWVdvT8qwdKkqvPzT93jp8tSF4+oN4IeTEIlQA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<!-- Custom CSS -->
	<link rel="stylesheet" href="<?php echo getenv("APP_URL").('assets/css/custom.css');?>">
</head>
<?php $uri = $this->uri->segment(1); ?>
<?php if($uri == 'login' || $uri == 'logout' || $uri == 'noaccess'){ ?>
<body class="hold-transition login-page">
<?php } else if($uri == 'register'){ ?>
<body class="hold-transition register-page">
<?php } else { ?>
<body class="hold-transition sidebar-mini sidebar-collapse layout-navbar-fixed">
	<div class="wrapper">
<?php } ?>
