<?php $this->load->view("admin/templates/header_template"); ?>
<body>
	<div class="wrapper">
		<?php echo $template; ?>
	</div>
	<br>
	<div class="row">
		<div class="col-4"></div>
		<div class="col-4 text-center">Made with <i class="fas fa-heart"></i> <?php echo date('Y'); ?> | <a class="text-dark" href="<?php echo getenv("APP_URL").("login"); ?>">Control Panel</a></div>
		<div class="col-4"></div>
	</div>
</body>
<?php $this->load->view("admin/templates/footer_template"); ?>