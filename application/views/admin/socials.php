<?php $this->load->view("admin/templates/header"); ?>
<?php $this->load->view("admin/templates/navigation"); ?>
<?php $this->load->view("admin/templates/sidebar"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Socials</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Socials</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card card-default">
              <div class="card-body">
                <table id="socialtable" class="table table-bordered">
                  <thead class="bg-navy">
                  <tr>
                    <th>Social Name</th>
                    <th>Social Link</th>
                    <th>Social Icon</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php if(!empty($social)){ ?>
                      <?php foreach($social as $s){ ?>
                        <tr>
                          <td><?php echo $s->name; ?></td>
                          <td><a href="<?php echo $s->link; ?>" target="_blank"><?php echo $s->link; ?></a></td>
                          <td><?php echo $s->iconname; ?> <i class="<?php echo $s->iconname; ?>"></i></td>
                           <td align="center"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalsocial<?php echo $s->id ?>"><i class="fas fa-edit"></i></button> <button type="button" class="btn btn-danger" onclick="deleteSocial(<?php echo $s->id; ?>)"><i class="fas fa-trash"></i></button></td>
                        </tr>
                        <div class="modal fade" id="modalsocial<?php echo $s->id ?>">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                              <div class="modal-header bg-navy">
                                <h4 class="modal-title">Edit <?php echo $s->name; ?></h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true" class="text-white">&times;</span>
                                </button>
                              </div>
                              <form class="form-horizontal" autocomplete="on" id="socialformedit<?php echo $s->id ?>" name="socialformedit<?php echo $s->id ?>">
                                <div class="modal-body">
                                    <div class="row">
                                      <div class="col-lg-4">
                                          <label for="socialname">Socials</label>
                                          <input type="text" class="form-control" id="socialnameedit<?php echo $s->id ?>" name="socialnameedit<?php echo $s->id ?>" placeholder="Enter socials name" value="<?php echo $s->name; ?>">
                                      </div>
                                      <div class="col-lg-4">
                                          <label for="sociallink">Socials Link</label>
                                          <input type="text" class="form-control" id="sociallinkedit<?php echo $s->id ?>" name="sociallinkedit<?php echo $s->id ?>" placeholder="Enter socials link including http:// or https://" value="<?php echo $s->link; ?>">
                                      </div>
                                      <div class="col-lg-4">
                                          <label for="socialicon">Socials Icon</label>
                                          <select class="form-control select2" id="socialiconedit<?php echo $s->id ?>" name="socialiconedit<?php echo $s->id ?>">
                                            <option value="-">Select Icon</option>
                                            <?php if(!empty($socialicon)){ ?>
                                              <?php foreach($socialicon as $si){ ?>
                                                <option value="<?php echo $si->id; ?>" <?php if($si->id == $s->icon){echo "selected";} ?>><?php echo $si->iconname; ?></option>
                                              <?php } ?>
                                            <?php } ?>
                                          </select>
                                      </div>
                                    </div>
                                </div>
                                <div class="modal-footer justify-content-between">
                                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn bg-navy" onclick="editSocial(<?php echo $s->id ?>)">Update</button>
                                </div>
                              </form>
                            </div>
                            <!-- /.modal-content -->
                          </div>
                          <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                      <?php } ?>
                    <?php }else{ ?>
                    <tr>
                      <td colspan="4" align="center">Uh Ohh. Please insert 1 social network first.</td>
                      <td style="display: none"></td>
                      <td style="display: none"></td>
                      <td style="display: none"></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>           
          </div>
        </div> <!-- /end row -->
        <div class="row">
          <div class="col-lg-12">
            <div class="card card-navy">
              <div class="card-header">
                <h3 class="card-title">Socials Network</h3>
              </div>
              <form class="form-horizontal" autocomplete="on" id="socialform" name="socialform">
                <div class="card-body">
                  <div class="social-wrapper">
                    <div class="row">
                      <div class="col-lg-4">
                          <label for="socialname">Socials</label>
                          <input type="text" class="form-control" id="socialname" name="socialname" placeholder="Enter socials name">
                      </div>
                      <div class="col-lg-4">
                          <label for="sociallink">Socials Link</label>
                          <input type="text" class="form-control" id="sociallink" name="sociallink" placeholder="Enter socials link including http:// or https://">
                      </div>
                      <div class="col-lg-4">
                          <label for="socialicon">Socials Icon</label>
                          <select class="form-control select2" id="socialicon" name="socialicon">
                            <option value="-" class="selected">Select Icon</option>
                            <?php if(!empty($socialicon)){ ?>
                              <?php foreach($socialicon as $si){ ?>
                                <option value="<?php echo $si->id; ?>"><?php echo $si->iconname; ?></option>
                              <?php } ?>
                            <?php } ?>
                          </select>
                      </div>
                      <!-- <div class="col-lg-1 mt-4 pt-2">                    
                        <div class="float-right"><a href="#" class="btn btn-success btn-flat add-btn"><i class="fas fa-plus"></i></a></div>
                      </div> -->
                    </div> <!-- /end row -->
                  </div> <!-- /end social wrapper -->
                </div> <!-- /end card body -->
                <div class="card-footer">
                  <button type="submit" class="btn bg-navy btn-flat">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/templates/footer"); ?>