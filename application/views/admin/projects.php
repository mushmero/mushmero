<?php $this->load->view("admin/templates/header"); ?>
<?php $this->load->view("admin/templates/navigation"); ?>
<?php $this->load->view("admin/templates/sidebar"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Projects</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Projects</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">        
        <div class="row">
          <div class="col-lg-12">
            <div class="card card-default">
              <div class="card-body">
                <table id="projtable" class="table table-bordered">
                  <thead class="bg-navy">
                  <tr>
                    <th>Projects</th>
                    <th>Status</th>
                    <th>Description</th>
                    <th width="70px">Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php if(!empty($projects)){ ?>
                      <?php foreach($projects as $p){ ?>
                        <tr>
                          <td><?php echo $p->name ?></td>
                          <td><?php echo $p->status ?></td>
                          <td><?php echo $p->description ?></td>
                          <td align="center"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalproj<?php echo $p->id ?>" onclick="popupProj(<?php echo $p->id ?>)"><i class="fas fa-edit"></i></button> <button type="button" class="btn btn-danger" onclick="deleteProject(<?php echo $p->id; ?>)"><i class="fas fa-trash"></i></button></td>
                        </tr>
                        <div class="modal fade" id="modalproj<?php echo $p->id ?>">
                          <div class="modal-dialog modal-xl">
                            <div class="modal-content">
                              <div class="modal-header bg-navy">
                                <h4 class="modal-title">Edit <?php echo $p->name; ?></h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true" class="text-white">&times;</span>
                                </button>
                              </div>
                              <form class="form-horizontal" autocomplete="on" id="projectformedit<?php echo $p->id ?>" name="projectformedit<?php echo $p->id ?>">
                                <div class="modal-body">
                                  <div class="row">
                                    <div class="col-4">
                                      <div class="row">
                                        <div class="col-12">
                                            <label for="projname">Project</label>
                                            <input type="text" class="form-control" id="projnameedit<?php echo $p->id ?>" name="projnameedit<?php echo $p->id ?>" placeholder="Enter project" value="<?php echo $p->name ?>">
                                        </div>
                                        <div class="col-12">
                                            <label for="projstatus">Status</label>
                                              <select class="form-control select2" id="projstatusedit<?php echo $p->id ?>" name="projstatusedit<?php echo $p->id ?>">
                                                <option value="-" class="selected">Select Type</option>
                                                  <?php if(!empty($status)) { ?>
                                                    <?php foreach($status as $s){ ?>
                                                      <option value="<?php echo $s->id ?>" <?php if($s->id == $p->statusid){echo "selected";} ?>><?php echo $s->status ?></option>
                                                    <?php } ?>
                                                  <?php } ?>
                                              </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-8">
                                      <div class="col-12">
                                          <label for="projdesc">Description</label>
                                          <textarea class="form-control" id="projdescedit<?php echo $p->id ?>" name="projdescedit<?php echo $p->id ?>"><?php echo $p->description; ?></textarea>
                                      </div>
                                    </div>
                                  </div> <!-- /end row -->
                                </div>
                                <div class="modal-footer justify-content-between">
                                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn bg-navy" onclick="editProject(<?php echo $p->id ?>)">Update</button>
                                </div>
                              </form>
                            </div>
                            <!-- /.modal-content -->
                          </div>
                          <!-- /.modal-dialog -->
                        </div>
                      <?php } ?>
                    <?php } else { ?>
                      <tr>
                        <td colspan="4" align="center">Uh Ohh. Please insert 1 project first.</td>
                        <td style="display: none"></td>
                        <td style="display: none"></td>
                        <td style="display: none"></td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>           
          </div>
        </div> <!-- /end row -->
        <div class="row">
          <div class="col-lg-12">
            <div class="card card-navy">
              <div class="card-header">
                <h3 class="card-title">Projects</h3>
              </div>
              <form class="form-horizontal" autocomplete="on" id="projectform" name="projectform">
                <div class="card-body">
                  <div class="row">
                    <div class="col-4">
                      <div class="row">
                        <div class="col-12">
                            <label for="projname">Project</label>
                            <input type="text" class="form-control" id="projname" name="projname" placeholder="Enter project">
                        </div>
                        <div class="col-12">
                            <label for="projstatus">Status</label>
                              <select class="form-control select2" id="projstatus" name="projstatus">
                                <option value="-" class="selected">Select Type</option>
                                <?php if(!empty($status)) { ?>
                                  <?php foreach($status as $s){ ?>
                                    <option value="<?php echo $s->id ?>"><?php echo $s->status ?></option>
                                  <?php } ?>
                                <?php } ?>
                              </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-8">
                      <div class="col-12">
                          <label for="projdesc">Description</label>
                          <textarea class="form-control" id="projdesc" name="projdesc"></textarea>
                      </div>
                    </div>
                  </div> 
                  </div> <!-- /end row -->
                </div> <!-- /end card body -->
                <div class="card-footer">
                  <button type="submit" class="btn bg-navy btn-flat">Save</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/templates/footer"); ?>