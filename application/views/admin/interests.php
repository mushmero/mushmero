<?php $this->load->view("admin/templates/header"); ?>
<?php $this->load->view("admin/templates/navigation"); ?>
<?php $this->load->view("admin/templates/sidebar"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Interests</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Interests</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6">
            <div class="card card-default">
              <div class="card-body">
                <table id="interesttable" class="table table-bordered">
                  <thead class="bg-navy">
                  <tr>
                    <th>Interest Name</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php if(!empty($interest)){ ?>
                      <?php foreach($interest as $i){ ?>
                        <tr>
                          <td><?php echo $i->name; ?></td>
                          <td align="center"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalinterest<?php echo $i->id ?>"><i class="fas fa-edit"></i></button> <button type="button" class="btn btn-danger" onclick="deleteInterest(<?php echo $i->id; ?>)"><i class="fas fa-trash"></i></button></td>
                        </tr>
                        <div class="modal fade" id="modalinterest<?php echo $i->id ?>">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                              <div class="modal-header bg-navy">
                                <h4 class="modal-title">Edit <?php echo $i->name; ?></h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true" class="text-white">&times;</span>
                                </button>
                              </div>
                              <form class="form-horizontal" autocomplete="on" id="interestformedit<?php echo $i->id ?>" name="interestformedit<?php echo $i->id ?>">
                                <div class="modal-body">
                                  <div class="row">
                                    <div class="col-lg-12">
                                        <label for="interestname">Interest Name</label>
                                        <input type="text" class="form-control" id="interestnameedit<?php echo $i->id ?>" name="interestnameedit<?php echo $i->id ?>" placeholder="Enter interest name" value="<?php echo $i->name; ?>">
                                    </div>
                                  </div> <!-- /end row -->
                                </div>
                                <div class="modal-footer justify-content-between">
                                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn bg-navy" onclick="editInterest(<?php echo $i->id ?>)">Update</button>
                                </div>
                              </form>
                            </div>
                            <!-- /.modal-content -->
                          </div>
                          <!-- /.modal-dialog -->
                        </div>
                      <?php } ?>
                    <?php } else { ?>
                      <tr>
                        <td colspan="2" align="center">Uh Ohh. Please insert 1 interest first.</td>
                        <td style="display: none"></td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>          
          </div>
          <div class="col-lg-6">
            <div class="card card-navy">
              <div class="card-header">
                <h3 class="card-title">Interests</h3>
              </div>
              <form class="form-horizontal" autocomplete="on" id="interestform" name="interestform">
                <div class="card-body">
                  <div class="interest-wrapper">
                    <div class="row">
                      <div class="col-lg-12">
                          <label for="interestname">Interest Name</label>
                          <input type="text" class="form-control" id="interestname" name="interestname" placeholder="Enter interest name">
                      </div>
                      <!-- <div class="col-lg-2 mt-4 pt-2">                    
                        <div class="float-right"><a href="#" class="btn btn-success btn-flat add-btn"><i class="fas fa-plus"></i></a></div>
                      </div> -->
                    </div> <!-- /end row -->
                  </div> <!-- /end interest wrapper -->
                </div> <!-- /end card body -->
                <div class="card-footer">
                  <button type="submit" class="btn bg-navy btn-flat">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/templates/footer"); ?>