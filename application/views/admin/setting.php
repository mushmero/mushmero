<?php $this->load->view("admin/templates/header"); ?>
<?php $this->load->view("admin/templates/navigation"); ?>
<?php $this->load->view("admin/templates/sidebar"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Setting</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Setting</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card card-navy card-outline card-outline-tabs">
              <div class="card-header p-0 border-bottom-0">
                <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active text-navy" id="social-tabs" data-toggle="pill" href="#social-tabs-custom" role="tab" aria-controls="social-tabs-custom" aria-selected="true">Social</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-navy" id="education-tabs" data-toggle="pill" href="#education-tabs-custom" role="tab" aria-controls="education-tabs-custom" aria-selected="false">Education</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-navy" id="language-tabs" data-toggle="pill" href="#language-tabs-custom" role="tab" aria-controls="language-tabs-custom" aria-selected="false">Language</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-navy" id="project-tabs" data-toggle="pill" href="#project-tabs-custom" role="tab" aria-controls="project-tabs-custom" aria-selected="false">Project</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-navy" id="todo-tabs" data-toggle="pill" href="#todo-tabs-custom" role="tab" aria-controls="todo-tabs-custom" aria-selected="false">ToDo List</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-navy" id="admin-tabs" data-toggle="pill" href="#admin-tabs-custom" role="tab" aria-controls="admin-tabs-custom" aria-selected="false">Admin</a>
                  </li>
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-four-tabContent">
                  <div class="tab-pane fade show active" id="social-tabs-custom" role="tabpanel" aria-labelledby="social-tabs">
                    <div class="row">
                      <div class="col-12 col-sm-6">
                        <table id="socialsetting" class="table table-bordered">
                          <thead class="bg-navy">
                            <tr>
                              <th>Icon</th>
                              <th>Type</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php if(!empty($social)){ ?>
                            <?php $i = 0; foreach($social as $s) { ?>
                              <tr>
                                <td><i class="<?php echo $s->iconname; ?>"></i> <?php echo $s->iconname; ?></td>
                                <td><?php echo $s->icontype; ?></td>
                                <td align="center"><button type="button" class="btn btn-info" onclick="editAlertSocial(<?php echo $s->id; ?>)"><i class="fas fa-edit"></i></button> <button type="button" class="btn btn-danger" onclick="deleteAlertSocial(<?php echo $s->id; ?>)"><i class="fas fa-trash"></i></button></td>
                              </tr>
                            <?php $i++; } ?>
                          <?php } else { ?>
                            <tr>
                              <td colspan="3" align="center">Add new social first</td>
                              <td style="display: none"></td>
                              <td style="display: none"></td>
                            </tr>
                          <?php } ?>
                          </tbody>
                        </table>
                      </div>
                      <div class="col-12 col-sm-6">
                        <form class="form form-horizontal" id="socialsettingform" name="socialsettingform">
                          <div class="form-group row">
                            <label for="inputIconName" class="col-sm-2 col-form-label">Icon Name</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="inputIconName" name="inputIconName" placeholder="Icon Name">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputType" class="col-sm-2 col-form-label">Type</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="inputType" name="inputType" placeholder="Type">
                            </div>
                          </div>
                            <button type="submit" class="btn bg-navy btn-block"><i class="fas fa-save"></i> Save</button>
                        </form>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="education-tabs-custom" role="tabpanel" aria-labelledby="education-tabs">
                    <div class="row">
                      <div class="col-12 col-sm-6">
                        <table id="educationsetting" class="table table-bordered">
                          <thead class="bg-navy">
                            <tr>
                              <th>Level</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php if(!empty($education)){ ?>
                            <?php $i = 0; foreach($education as $e) { ?>
                              <tr>
                                <td><?php echo $e->level; ?></td>
                                <td align="center"><button type="button" class="btn btn-info" onclick="editAlertEdu(<?php echo $e->id; ?>)"><i class="fas fa-edit"></i></button> <button type="button" class="btn btn-danger" onclick="deleteAlertEdu(<?php echo $e->id; ?>)"><i class="fas fa-trash"></i></button></td>
                              </tr>
                            <?php $i++; } ?>
                            <?php } else { ?>
                              <tr>
                                <td colspan="2" align="center">Add new education level first</td>
                                <td style="display: none"></td>
                              </tr>
                            <?php } ?>
                          </tbody>
                        </table>
                      </div>
                      <div class="col-12 col-sm-6">
                        <form class="form form-horizontal" id="educationsettingform" name="educationsettingform">
                          <div class="form-group row">
                            <label for="inputLevel" class="col-sm-2 col-form-label">Level</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="inputLevel" name="inputLevel" placeholder="Education level">
                            </div>
                          </div>
                            <button type="submit" class="btn bg-navy btn-block"><i class="fas fa-save"></i> Save</button>
                        </form>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="language-tabs-custom" role="tabpanel" aria-labelledby="language-tabs">
                    <div class="row">
                      <div class="col-12 col-sm-6">
                        <table id="languagesetting" class="table table-bordered">
                          <thead class="bg-navy">
                            <tr>
                              <th>Proficiency</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php if(!empty($language)){ ?>
                            <?php $i = 0; foreach($language as $l) { ?>
                              <tr>
                                <td><?php echo $l->proficiency; ?></td>
                                <td align="center"><button type="button" class="btn btn-info" onclick="editAlertLang(<?php echo $l->id; ?>)"><i class="fas fa-edit"></i></button> <button type="button" class="btn btn-danger" onclick="deleteAlertLang(<?php echo $l->id; ?>)"><i class="fas fa-trash"></i></button></td>
                              </tr>
                            <?php $i++; } ?>
                            <?php } else { ?>
                              <tr>
                                <td colspan="2" align="center">Add new proficiency first</td>
                                <td style="display: none"></td>
                              </tr>
                            <?php } ?>
                          </tbody>
                        </table>
                      </div>
                      <div class="col-12 col-sm-6">
                        <form class="form form-horizontal" id="languagesettingform" name="languagesettingform">
                          <div class="form-group row">
                            <label for="inputProficiency" class="col-sm-2 col-form-label">Proficiency</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="inputProficiency" name="inputProficiency" placeholder="Language Proficiency">
                            </div>
                          </div>
                            <button type="submit" class="btn bg-navy btn-block"><i class="fas fa-save"></i> Save</button>
                        </form>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="project-tabs-custom" role="tabpanel" aria-labelledby="project-tabs">
                    <div class="row">
                      <div class="col-12 col-sm-6">
                        <table id="projectsetting" class="table table-bordered">
                          <thead class="bg-navy">
                            <tr>
                              <th>Status</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php if(!empty($project)){ ?>
                            <?php $i = 0; foreach($project as $p) { ?>
                              <tr>
                                <td><?php echo $p->status; ?></td>
                                <td align="center"><button type="button" class="btn btn-info" onclick="editAlertProj(<?php echo $p->id; ?>)"><i class="fas fa-edit"></i></button> <button type="button" class="btn btn-danger" onclick="deleteAlertProj(<?php echo $p->id; ?>)"><i class="fas fa-trash"></i></button></td>
                              </tr>
                            <?php $i++; } ?>
                            <?php } else { ?>
                              <tr>
                                <td colspan="2" align="center">Add new status first</td>
                                <td style="display: none"></td>
                              </tr>
                            <?php } ?>
                          </tbody>
                        </table>
                      </div>
                      <div class="col-12 col-sm-6">
                        <form class="form form-horizontal" id="projectsettingform" name="projectsettingform">
                          <div class="form-group row">
                            <label for="inputStatus" class="col-sm-2 col-form-label">Status</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="inputStatus" name="inputStatus" placeholder="Project Status">
                            </div>
                          </div>
                            <button type="submit" class="btn bg-navy btn-block"><i class="fas fa-save"></i> Save</button>
                        </form>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="todo-tabs-custom" role="tabpanel" aria-labelledby="todo-tabs">
                    <div class="row">
                      <div class="col-12">
                        <form class="form form-horizontal" id="todoformsetting" name="todoformsetting">
                          <div class="form-group row">
                            <div class="col-6">
                              <div class="row">
                                <label for="inputToDo" class="col-sm-2 col-form-label">ToDo</label>
                                <div class="col-sm-4">
                                  <input type="text" class="form-control" id="inputToDo" name="inputToDo" placeholder="ToDo">
                                </div>
                                <label for="tododuedate" class="col-sm-2 col-form-label">Due Date</label>
                                <div class="col-sm-4">
                                  <input type="text" class="form-control" id="tododuedate" name="tododuedate" placeholder="Due Date">
                                  <span class="fas fa-calendar-alt datepicker"></span>
                                </div>
                              </div>                              
                            </div>
                            <div class="col-2">
                              <button type="submit" class="btn bg-navy btn-block"><i class="fas fa-save"></i> Save</button>
                            </div>
                          </div>
                        </form>
                      </div>
                      <div class="col-12">
                        <table id="todosetting" class="table table-bordered">
                          <thead class="bg-navy">
                            <tr>
                              <th>Activity</th>
                              <th>Date</th>
                              <th>Status</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php if(!empty($todo)){ ?>
                              <?php foreach($todo as $td){ ?>
                                <tr>
                                  <td><?php echo $td->activity; ?></td>
                                  <td>Due Date: <?php echo $td->due; ?><br>Completed Date: <?php echo $td->complete; ?></td>
                                  <td><span class="badge <?php if($td->status == 'New'){echo "badge-success";}elseif($td->status == 'Updated'){ echo "badge-warning";}else{ echo "badge-info";} ?>"><?php echo $td->status; ?></span></td>
                                  <td align="center"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#modaltodo<?php echo $td->id ?>" onclick="popupTodo(<?php echo $td->id; ?>)"><i class="fas fa-edit"></i></button> <button type="button" class="btn btn-danger" onclick="deleteAlertToDo(<?php echo $td->id; ?>)"><i class="fas fa-trash"></i></button></td>
                                </tr>
                                <div class="modal fade" id="modaltodo<?php echo $td->id ?>">
                                  <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                      <div class="modal-header bg-navy">
                                        <h4 class="modal-title">Edit <?php echo $td->activity; ?></h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true" class="text-white">&times;</span>
                                        </button>
                                      </div>
                                      <form class="form-horizontal" autocomplete="on" id="todoformsettingedit<?php echo $td->id ?>" name="todoformsettingedit<?php echo $td->id ?>">
                                        <div class="modal-body">
                                          <div class="row">
                                            <div class="col-12">
                                              <div class="form-group row">
                                                <label for="inputToDo" class="col-sm-2 col-form-label">ToDo</label>
                                                <div class="col-sm-4">
                                                  <input type="text" class="form-control" id="inputToDoedit<?php echo $td->id; ?>" name="inputToDoedit<?php echo $td->id; ?>" placeholder="ToDo" value="<?php echo $td->activity ?>">
                                                </div>
                                                <label for="tododuedate" class="col-sm-2 col-form-label">Due Date</label>
                                                <div class="col-sm-4">
                                                  <input type="text" class="form-control" id="tododuedateedit<?php echo $td->id; ?>" name="tododuedateedit<?php echo $td->id; ?>" placeholder="Due Date" value="<?php echo $td->due; ?>">
                                                  <span class="fas fa-calendar-alt datepicker"></span>
                                                </div>
                                              </div>
                                            </div>
                                          </div> <!-- /end row -->
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                          <button type="button" class="btn bg-navy" onclick="editTodo(<?php echo $td->id ?>)">Update</button>
                                        </div>
                                      </form>
                                    </div>
                                    <!-- /.modal-content -->
                                  </div>
                                  <!-- /.modal-dialog -->
                                </div>
                              <?php } ?>
                            <?php } else { ?>
                              <tr>
                                <td colspan="4" align="center">Add new todo activity</td>
                                <td style="display: none"></td>
                                <td style="display: none"></td>
                                <td style="display: none"></td>
                              </tr>
                            <?php } ?>                            
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="admin-tabs-custom" role="tabpanel" aria-labelledby="admin-tabs">
                    <div class="row">
                      <div class="col-12 col-sm-6 col-md-3">
                        <div class="row">
                          <div class="col-12">
                            <div id="bgslider" class="carousel slide" data-ride="carousel">
                              <?php if(!empty($background)){ ?>
                                <ul class="carousel-indicators">
                                  <?php for($i = 0; $i < count($background); $i++){ ?>
                                    <li data-target="#bgslider" data-slide-to="<?php echo $i ?>"></li>
                                  <?php } ?>
                                </ul>
                                <div class="carousel-inner">
                                <?php foreach($background as $bg){ ?>
                                  <div class="carousel-item active">
                                    <img src="<?php echo getenv("APP_URL").$bg->filepath.$bg->filename ?>" alt="<?php echo $bg->altname; ?>" width="100%" height="120">
                                  </div>
                                <?php } ?>
                                </div>
                                <a class="carousel-control-prev" href="#bgslider" data-slide="prev">
                                  <span class="carousel-control-prev-icon"></span>
                                </a>
                                <a class="carousel-control-next" href="#bgslider" data-slide="next">
                                  <span class="carousel-control-next-icon"></span>
                                </a>
                              <?php } else { ?>
                                <ul class="carousel-indicators">
                                  <li data-target="#bgslider" data-slide-to="0" class="active"></li>
                                </ul>
                                <div class="carousel-inner">
                                  <div class="carousel-item active">
                                    <img src="<?php echo getenv("APP_URL")."/".("vendor/almasaeed2010/adminlte/dist/img/photo4.jpg") ?>" alt="default" width="100%" height="120">
                                  </div>
                                </div>
                                <a class="carousel-control-prev" href="#bgslider" data-slide="prev">
                                  <span class="carousel-control-prev-icon"></span>
                                </a>
                                <a class="carousel-control-next" href="#bgslider" data-slide="next">
                                  <span class="carousel-control-next-icon"></span>
                                </a>
                              <?php } ?>
                            </div>
                          </div>
                          <div class="col-12">
                            <form class="form form-horizontal" enctype="multipart/form-data" id="bgform" name="bgform">
                              <div class="form-group">
                                <label for="uploadbg">Upload Background</label>
                                <div class="input-group">
                                  <div class="custom-file">
                                    <input type="file" class="custom-file-input" aria-describedby="bghelp" id="uploadbg" name="uploadbg" accept="image/jpeg, image/png">
                                    <label class="custom-file-label" for="uploadbg">Choose background</label>
                                  </div>                              
                                </div>
                                <small id="bghelp" class="form-text text-muted">Upload only w x h size.</small>
                              </div>
                                <button type="submit" class="btn bg-navy btn-block"><i class="fas fa-save"></i> Save</button>
                            </form>
                          </div>
                        </div>                        
                      </div>
                      <div class="col-12 col-sm-6 col-md-9">
                        <div class="row">
                          <div class="col-12">
                            <div class="card">
                              <div class="card-header bg-navy">Announcements</div>
                              <div class="card-body">
                                <div class="col-12">
                                  <form class="form form-horizontal" id="announcementsform" name="announcementsform">
                                    <div class="form-group row">
                                      <div class="col-10">
                                        <div class="row">
                                          <label for="announcements" class="col-sm-2 col-form-label">Announcements</label>
                                          <div class="col-sm-9">
                                            <input type="text" class="form-control" id="announcements" name="announcements" placeholder="Do you have announcements?">
                                          </div>
                                          <?php if(!empty($announcement)){ ?>
                                            <input type="hidden" id="announcementid" name="announcementid" value="<?php echo $announcement->id ?>">
                                          <?php } ?>
                                        </div>                              
                                      </div>
                                      <div class="col-2">
                                        <button type="submit" class="btn bg-navy btn-block"><i class="fas fa-save"></i> Save</button>
                                      </div>
                                    </div>
                                  </form>
                                </div>                               
                              </div>
                            </div>
                          </div>
                        </div>             
                      </div>
                    </div>
                    <hr>
                    <div class="row mt-4">
                      <div class="col-6">
                        <div class="card">
                          <div class="card-header bg-navy">PDF Settings</div>
                          <form class="form" id="pdfsettings" name="pdfsettings">
                            <div class="card-body">
                              <div class="row">
                                <div class="col-4">
                                  <label for="margin-top" class="form-label">Margin Top</label>
                                  <input type="text" class="form-control" id="margin-top" name="margin-top" value="<?php if(!empty($pdfsetting)){ echo $pdfsetting->margin_top;}else{ echo 0;} ?>">
                                </div>
                                <div class="col-4">
                                  <label for="margin-bottom" class="form-label">Margin Bottom</label>
                                  <input type="text" class="form-control" id="margin-bottom" name="margin-bottom" value="<?php if(!empty($pdfsetting)){ echo $pdfsetting->margin_bottom;}else{ echo 0;} ?>">
                                </div>
                                <div class="col-4">
                                  <label for="margin-left" class="form-label">Margin Left</label>
                                  <input type="text" class="form-control" id="margin-left" name="margin-left" value="<?php if(!empty($pdfsetting)){ echo $pdfsetting->margin_left;}else{ echo 0;} ?>">
                                </div>
                              </div>
                              <hr>
                              <div class="row">
                                <div class="col-4">
                                  <label for="header" class="form-label">Header</label>
                                  <input type="text" class="form-control" id="header" name="header" value="<?php if(!empty($pdfsetting)){ echo $pdfsetting->header;}else{ echo 0;} ?>">
                                </div>
                                <div class="col-4">
                                  <label for="footer" class="form-label">Footer</label>
                                  <input type="text" class="form-control" id="footer" name="footer" value="<?php if(!empty($pdfsetting)){ echo $pdfsetting->footer;}else{ echo 0;} ?>">
                                </div>
                                <div class="col-4">
                                  <label for="margin-right" class="form-label">Margin Right</label>
                                  <input type="text" class="form-control" id="margin-right" name="margin-right" value="<?php if(!empty($pdfsetting)){ echo $pdfsetting->margin_right;}else{ echo 0;} ?>">
                                </div>
                              </div>
                            </div>
                            <div class="card-footer">
                              <div class="row">
                                <div class="col-2">
                                  <button type="submit" class="btn btn-block bg-navy">Update</button>
                                </div>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>                      
                      <div class="col-6">
                        <div class="card">
                          <div class="card-header bg-navy">Template</div>
                          <div class="card-body">
                            <div class="col-12 row">
                              <div class="col-4">
                                <label for="activetemplate">Active Template</label>
                              </div>
                              <div class="col-8">
                                <div><?php echo empty($activetemplate) ? "None" : $activetemplate->name; ?></div>
                              </div>
                            </div>
                            <hr>
                            <div class="col-12">
                              <div class="form-group row">
                                <div class="col-8">
                                  <div class="row">
                                    <div class="col-4">
                                      <label for="selecttemplate">Select Template</label>
                                    </div>
                                    <div class="col-6">
                                      <select id="selecttemplate" name="selecttemplate" class="form-control select2">
                                        <option value="-">Select Template</option>
                                        <?php if(!empty($template)){ ?>
                                          <?php foreach($template as $t){ ?>
                                            <option value="<?php echo $t->id ?>"><?php echo $t->name; ?></option>
                                          <?php } ?>
                                        <?php } ?>
                                      </select>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-1">
                                  <button type="button" class="btn bg-success btn-block btn-sm" id="templateactivate" name="templateactivate" data-toggle="tooltip" data-placement="top" title="Activate" onclick="activateTemplate()"><i class="fas fa-power-off"></i></button>
                                </div>
                                <div class="col-1">
                                  <button type="button" class="btn bg-info btn-block btn-sm" data-toggle="tooltip" data-placement="top" title="Edit" onclick="popupTemplate()"><i class="fas fa-edit"></i> </button>
                                </div>
                                <div class="modal fade" id="modaltemplate">
                                  <div class="modal-dialog modal-xl">
                                    <div class="modal-content">
                                      <div class="modal-header bg-navy">
                                        <h4 class="modal-title" name="modaltitle" id="modaltitle"></h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true" class="text-white">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <div class="form-group row">
                                          <div class="col-4">
                                            <label for="templatenameedit" class="form-label">Template Name</label>
                                          </div>
                                          <div class="col-8">
                                            <input type="text" class="form-control" name="templatenameedit" id="templatenameedit" placeholder="Enter template name">
                                          </div>
                                        </div>
                                        <div class="form-group row">
                                          <div class="col-4">
                                            <label for="fieldoptionedit" class="form-label">Template Fields</label>
                                            <select class="form-control select2" id="fieldoptionedit" name="fieldoptionedit">
                                              <option value="-">Select Field</option>
                                              <?php foreach($optionvalue as $ov){
                                                echo $ov;
                                              } ?>
                                            </select>
                                          </div>
                                          <div class="col-4">    
                                            <label for="bgcoloredit" class="form-label">Main Color</label>
                                            <select class="form-control select2" id="bgcoloredit" name="bgcoloredit">
                                              <option value="-">Select Color</option>
                                              <?php foreach($bgcolor as $bgkey => $bgval){ ?>
                                                <option value="<?php echo $bgkey ?>" class="<?php echo $bgkey ?>"><?php echo $bgval ?></option>
                                              <?php } ?>
                                            </select>                              
                                          </div>
                                          <div class="col-4">
                                            <label for="accentcoloredit" class="form-label">Accent Color</label>
                                            <select class="form-control select2" id="accentcoloredit" name="accentcoloredit">
                                              <option value="-">Select Color</option>
                                              <?php foreach($bgcolor as $bgkey => $bgval){ ?>
                                                <option value="<?php echo $bgkey ?>" class="<?php echo $bgkey ?>"><?php echo $bgval ?></option>
                                              <?php } ?>
                                            </select>
                                          </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="templatedesignedit" class="form-label">Template Design</label>
                                            <textarea class="form-control" id="templatedesignedit" name="templatedesignedit"></textarea>
                                        </div>
                                      </div>
                                      <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn bg-navy" id="templateupdatebtn" name="templateupdatebtn">Update</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-1">
                                  <button type="button" class="btn bg-danger btn-block btn-sm" data-toggle="tooltip" data-placement="top" title="Remove" onclick="deleteTemplate()"><i class="fas fa-times"></i> </button>
                                </div>
                              </div>
                            </div>
                            <hr>
                            <form class="form form-horizontal" id="templateform" name="templateform">
                              <div class="form-group row">
                                <div class="col-4">
                                  <label for="templatename" class="form-label">Template Name</label>
                                </div>
                                <div class="col-8">
                                  <input type="text" class="form-control" name="templatename" id="templatename" placeholder="Enter template name">
                                </div>
                              </div>
                              <div class="form-group row">
                                <div class="col-4">
                                  <label for="fieldoption" class="form-label">Template Fields</label>
                                  <select class="form-control select2" id="fieldoption" name="fieldoption">
                                    <option value="-">Select Field</option>
                                    <?php foreach($optionvalue as $ov){
                                      echo $ov;
                                    } ?>
                                  </select>
                                </div>
                                <div class="col-4">    
                                  <label for="bgcolor" class="form-label">Main Color</label>
                                  <select class="form-control select2" id="bgcolor" name="bgcolor">
                                    <option value="-">Select Color</option>
                                    <?php foreach($bgcolor as $bgkey => $bgval){ ?>
                                      <option value="<?php echo $bgkey ?>" class="<?php echo $bgkey ?>"><?php echo $bgval ?></option>
                                    <?php } ?>
                                  </select>                              
                                </div>
                                <div class="col-4">
                                  <label for="accentcolor" class="form-label">Accent Color</label>
                                  <select class="form-control select2" id="accentcolor" name="accentcolor">
                                    <option value="-">Select Color</option>
                                    <?php foreach($bgcolor as $bgkey => $bgval){ ?>
                                      <option value="<?php echo $bgkey ?>" class="<?php echo $bgkey ?>"><?php echo $bgval ?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                              </div>
                              <div class="form-group">
                                  <label for="templatedesign" class="form-label">Template Design</label>
                                  <textarea class="form-control" id="templatedesign" name="templatedesign"></textarea>
                              </div>
                              <div class="form-group">
                                <button type="submit" class="btn bg-navy btn-block"><i class="fas fa-save"></i> Save</button>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/templates/footer"); ?>