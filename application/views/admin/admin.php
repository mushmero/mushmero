<?php $this->load->view("admin/templates/header"); ?>
<?php $this->load->view("admin/templates/navigation"); ?>
<?php $this->load->view("admin/templates/sidebar"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12 col-sm-8">
            <div class="row">
              <div class="col-12">
                <div class="callout callout-navy shadow">
                  <p><i class="fas fa-bullhorn text-danger"></i> <?php echo $announcement; ?></p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-6 col-sm-3">
                <div class="info-box bg-info shadow">
                  <span class="info-box-icon"><i class="fas fa-users"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Social</span>
                    <span class="info-box-number"><?php echo $socialcount; ?></span>
                  </div>
                </div>
              </div>
              <div class="col-6 col-sm-3">
                <div class="info-box bg-info shadow">
                  <span class="info-box-icon"><i class="fas fa-university"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Education</span>
                    <span class="info-box-number"><?php echo $educount; ?></span>
                  </div>
                </div>
              </div>
              <div class="col-6 col-sm-3">
                <div class="info-box bg-info shadow">
                  <span class="info-box-icon"><i class="fas fa-language"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Language</span>
                    <span class="info-box-number"><?php echo $langcount; ?></span>
                  </div>
                </div>
              </div>
              <div class="col-6 col-sm-3">
                <div class="info-box bg-info shadow">
                  <span class="info-box-icon"><i class="fas fa-thumbs-up"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Interest</span>
                    <span class="info-box-number"><?php echo $intcount; ?></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-6 col-sm-3">
                <div class="info-box bg-success shadow">
                  <span class="info-box-icon"><i class="fas fa-bullseye"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Objective</span>
                    <span class="info-box-number"><?php echo $objcount; ?></span>
                  </div>
                </div>
              </div>
              <div class="col-6 col-sm-3">
                <div class="info-box bg-success shadow">
                  <span class="info-box-icon"><i class="fas fa-briefcase"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Experience</span>
                    <span class="info-box-number"><?php echo $expcount; ?></span>
                  </div>
                </div>
              </div>
              <div class="col-6 col-sm-3">
                <div class="info-box bg-success shadow">
                  <span class="info-box-icon"><i class="fas fa-archive"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Project</span>
                    <span class="info-box-number"><?php echo $projcount; ?></span>
                  </div>
                </div>
              </div>
              <div class="col-6 col-sm-3">
                <div class="info-box bg-success shadow">
                  <span class="info-box-icon"><i class="fas fa-rocket"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Skill</span>
                    <span class="info-box-number"><?php echo $skillcount; ?></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-4">
            <div class="card card-widget widget-user shadow-lg">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header text-white" style="background: url('<?php echo $bg; ?>') center center;">
                <h3 class="widget-user-username text-right"><?php echo empty($fullname) ? "Name" : $fullname; ?></h3>
                <h5 class="widget-user-desc text-right"><?php echo empty($tagline) ? "Position" : $tagline; ?></h5>
              </div>
              <div class="widget-user-image">
                <img class="img-circle elevation-2" src="<?php echo empty($picture) ? getenv("APP_URL")."/".("assets/images/no_image.png") : $picture; ?>" alt="User Avatar">
              </div>
              <div class="card-footer">
                <div class="row">
                  <div class="col-sm-4 border-right">
                    <div class="description-block">
                      <h5 class="description-header"><?php echo $visitor ?></h5>
                      <span class="description-text">VISITORS</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-4 border-right">
                    <div class="description-block">
                      <h5 class="description-header"><?php echo $career ?></h5>
                      <span class="description-text">YEARS EXP</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-4">
                    <div class="description-block">
                      <h5 class="description-header"><?php echo $totalemployer; ?></h5>
                      <span class="description-text">EMPLOYERs</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12 col-sm-8">
            <div class="card card-navy">
              <div class="card-header">
                <h3 class="card-title">Visitor Map</h3>
              </div>
              <div class="card-body">
                <div class="worldmap">
                    <div class="map"></div>
                    <div class="plotLegend"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-4">
            <div class="card card-navy">
              <div class="card-header">                
                <h3 class="card-title">
                  <i class="fas fa-clipboard"></i>
                  To Do List
                </h3>
              </div>
              <div class="card-body">
                <ul class="todo-list" data-widget="todo-list">
                  <?php if(!empty($activity)){ ?>
                  <?php foreach($activity as $a){ ?>
                    <li>
                    <!-- drag handle -->
                    <span class="handle">
                      <i class="fas fa-ellipsis-v"></i>
                      <i class="fas fa-ellipsis-v"></i>
                    </span>
                    <!-- checkbox -->
                    <div  class="icheck-primary d-inline ml-2">
                      <input type="checkbox" name="todoCheck<?php echo $a->id; ?>" id="todoCheck<?php echo $a->id; ?>" onchange="popConfirm(<?php echo $a->id; ?>)">
                      <label for="todoCheck<?php echo $a->id; ?>"></label>
                    </div>
                    <!-- todo text -->
                    <span class="text"><?php echo $a->activity ?></span>
                    <small class="badge <?php if($a->status == 'New'){echo "badge-success";}elseif($a->status == 'Updated'){ echo "badge-warning";}else{ echo "badge-info";} ?>"><?php echo $a->status; ?></small>
                    <span class="float-right"><small class="badge <?php if($a->due > date('d/m/Y')){ echo "badge-success";}elseif($a->due == date('d/m/Y')){ echo "badge-warning";}elseif($a->due < date('d/m/Y')){ echo "badge-danger";}?>"><?php echo $a->due; ?></small></span>
                    <!-- Emphasis label -->
                    <!-- <small class="badge badge-danger"><i class="far fa-clock"></i> 2 mins</small> -->
                    <!-- General tools such as edit or delete-->
                    <!-- <div class="tools">
                      <i class="fas fa-edit"></i>
                      <i class="fas fa-trash-o"></i>
                    </div> -->
                  </li>
                  <?php } ?>
                  <?php } else { ?>
                    <li>
                    <!-- drag handle -->
                    <span class="handle">
                      <i class="fas fa-ellipsis-v"></i>
                      <i class="fas fa-ellipsis-v"></i>
                    </span>
                    <!-- checkbox -->
                    <!-- <div  class="icheck-primary d-inline ml-2">
                      <input type="checkbox" value="" name="todo1" id="todoCheck1">
                      <label for="todoCheck1"></label>
                    </div> -->
                    <!-- todo text -->
                    <span class="text">No new todo list</span>
                    <!-- Emphasis label -->
                    <!-- <small class="badge badge-danger"><i class="far fa-clock"></i> 2 mins</small> -->
                    <!-- General tools such as edit or delete-->
                    <!-- <div class="tools">
                      <i class="fas fa-edit"></i>
                      <i class="fas fa-trash-o"></i>
                    </div> -->
                  </li>
                  <?php } ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/templates/footer"); ?>