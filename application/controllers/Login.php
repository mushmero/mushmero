<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller {
	var $data = array();

	function __construct(){
		parent::__construct();
		date_default_timezone_set("Asia/Kuala_Lumpur");
		$this->load->library('userlib');
	}

	public function index(){
		if ($this->userlib->checkLogin()) {
			redirect(getenv("APP_URL").'admin');
		}
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run()) {
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			if ($username == 'superadmin' && $password == 'sup3r@dm!n') {
				$this->userlib->setLogin($username);
				redirect(getenv("APP_URL").'admin');
			} else {
				$this->data['error'] = "Unable to login";
			}
		}
		$this->load->view('main/login',$this->data);
	}
	public function logout() {
		$this->userlib->removeLogin();
		$this->data['success'] = 'You have successfully logout from system';
		$this->load->view('main/login', $this->data);
	}
	public function noaccess() {
		$this->data['error'] = 'You have no access or your login have expired';
		$this->load->view('main/login', $this->data);
	}
	public function checklogin(){
		echo $this->userlib->checkLogin();
	}
}