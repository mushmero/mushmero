<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Register extends CI_Controller {
	var $data = array();

	function __construct(){
		parent::__construct();
	}

	public function index(){
		$this->load->view('main/register',$this->data);
	}
}