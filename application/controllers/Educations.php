<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Educations extends CI_Controller {
	var $data = array();
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Kuala_Lumpur');
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$this->load->model('usermodel');
		$this->load->model('educationmodel');
		$this->load->library('userlib');
		if ($this->userlib->checkLogin() === FALSE) {
			$this->userlib->removeLogin();
			redirect(getenv("APP_URL").'noaccess');
		} else {
			date_default_timezone_set("Asia/Kuala_Lumpur");
			$this->data['user'] = $this->session->userdata('user');
		}
	}
	public function index()
	{
		$level = $this->educationmodel->getEducationLevel();
		$education = $this->educationmodel->getEducation();
		$active = $this->usermodel->getActiveProfile();

		if(empty($active)){
			$picture = '';
			$altname = '';
			$fullname = '';
			$tagline = '';
			$email = '';
			$phone = '';
			$website = '';
		}else{
			$picture = getenv("APP_URL").$active->filepath."/".$active->filename;
			$altname = $active->altname;
			$fullname = $active->fullname;
			$tagline = $active->tagline;
			$email = $active->email;
			$phone = str_replace(" ","",$active->phone);
			$website = $active->website;
		}

		$this->data['picture'] = $picture;
		$this->data['altname'] = $altname;
		$this->data['fullname'] = $fullname;
		$this->data['tagline'] = $tagline;
		$this->data['email'] = $email;
		$this->data['phone'] = $phone;
		$this->data['website'] = $website;
		$this->data['level'] = $level;
		$this->data['education'] = $education;
		$this->load->view('admin/educations',$this->data);
	}
	public function save(){
		$name = $this->input->post('eduname');
		$place = $this->input->post('eduplace');
		$yearstart = $this->input->post('eduyearstart');
		$yearend = $this->input->post('eduyearend');
		$level = $this->input->post('edulevel');

		if(empty($name)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Name is empty',
			);
			echo json_encode($msg);
		}
		if(empty($place)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Institution is empty',
			);
			echo json_encode($msg);
		}
		if(empty($yearstart)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Start year is empty',
			);
			echo json_encode($msg);
		}
		if(empty($yearend)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'End year is empty',
			);
			echo json_encode($msg);
		}		
		if(empty($level)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Level is empty',
			);
			echo json_encode($msg);
		}
		if(!empty($name) && !empty($place) && !empty($yearstart) && !empty($yearend) && !empty($level)){
			$data = array(
				'name' => $name,
				'institution' => $place,
				'yearstart' => $yearstart,
				'yearend' => $yearend,
				'level' => $level,
				'createdtime' => date('Y-m-d H:i:s'),
			);
			$save = $this->educationmodel->save($data);
			if($save){
				$msg = array(
					'icon' => 'success',
					'msg' => 'Record saved!',
				);
				echo json_encode($msg);
			}else{
				$msg = array(
					'icon' => 'error',
					'msg' => 'Unable to save',
				);
				echo json_encode($msg);
			}
		}
	}
	public function remove($id){
		$this->educationmodel->remove($id);
		return true;
	}
	public function get($id){
		$result = $this->educationmodel->getEducationSingleData($id);
		if($result){
			echo json_encode($result);
		}return false;
	}
	public function update($id){
		$params = array(
			'name' => $this->input->post('edunameedit'.$id),
			'institution' => $this->input->post('eduplaceedit'.$id),
			'yearstart' => $this->input->post('eduyearstartedit'.$id),
			'yearend' => $this->input->post('eduyearendedit'.$id),
			'level' => $this->input->post('eduleveledit'.$id),
			'modifiedtime' => date('Y-m-d H:i:s'),
		);
		$this->educationmodel->updateEducationById($id,$params);
		return true;
	}
}