<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Interests extends CI_Controller {
	var $data = array();
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Kuala_Lumpur');
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$this->load->model('usermodel');
		$this->load->model('interestmodel');
		$this->load->library('userlib');
		if ($this->userlib->checkLogin() === FALSE) {
			$this->userlib->removeLogin();
			redirect(getenv("APP_URL").'noaccess');
		} else {
			date_default_timezone_set("Asia/Kuala_Lumpur");
			$this->data['user'] = $this->session->userdata('user');
		}
	}
	public function index()
	{
		$interest = $this->interestmodel->getInterest();
		$active = $this->usermodel->getActiveProfile();

		if(empty($active)){
			$picture = '';
			$altname = '';
			$fullname = '';
			$tagline = '';
			$email = '';
			$phone = '';
			$website = '';
		}else{
			$picture = getenv("APP_URL").$active->filepath."/".$active->filename;
			$altname = $active->altname;
			$fullname = $active->fullname;
			$tagline = $active->tagline;
			$email = $active->email;
			$phone = str_replace(" ","",$active->phone);
			$website = $active->website;
		}

		$this->data['picture'] = $picture;
		$this->data['altname'] = $altname;
		$this->data['fullname'] = $fullname;
		$this->data['tagline'] = $tagline;
		$this->data['email'] = $email;
		$this->data['phone'] = $phone;
		$this->data['website'] = $website;
		$this->data['interest'] = $interest;
		$this->load->view('admin/interests',$this->data);
	}
	public function save(){
		$name = $this->input->post('interestname');

		if(empty($name)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Name is empty',
			);
			echo json_encode($msg);
		}
		if(!empty($name) ){
			$data = array(
				'name' => $name,
				'createdtime' => date('Y-m-d H:i:s'),
			);
			$save = $this->interestmodel->save($data);
			if($save){
				$msg = array(
					'icon' => 'success',
					'msg' => 'Record saved!',
				);
				echo json_encode($msg);
			}else{
				$msg = array(
					'icon' => 'error',
					'msg' => 'Unable to save',
				);
				echo json_encode($msg);
			}
		}
	}
	public function remove($id){
		$this->interestmodel->remove($id);
		return true;
	}
	public function get($id){
		$result = $this->interestmodel->getInterestSingleData($id);
		if($result){
			echo json_encode($result);
		}return false;
	}
	public function update($id){
		$params = array(
			'name' => $this->input->post('interestnameedit'.$id),
			'modifiedtime' => date('Y-m-d H:i:s'),
		);
		$this->interestmodel->updateInterestById($id,$params);
		return true;
	}
}