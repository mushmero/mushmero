<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
	var $data = array();
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Kuala_Lumpur');
		$this->load->library('systools');
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
	}
	public function index()
	{
		$this->systools->saveVisitor();
		$this->data['profilepic'] = $this->getImage();
		$this->data['fullname'] = $this->getName();
		$this->data['tagline'] = $this->getTagline();
		$this->data['email'] = $this->getEmail();
		$this->data['phone'] = $this->getPhone();
		$this->data['social'] = $this->getSocial();
		$this->data['socialIcon'] = $this->getSocialIcon();
		$this->data['socialLinks'] = $this->getSocialLinks();
		$this->data['socialText'] = $this->getSocialText();
		$this->data['eduTitle'] = $this->getEduTitle();
		$this->data['eduPlace'] = $this->getEduPlace();
		$this->data['eduTime'] = $this->getEduTime();
		$this->data['langTitle'] = $this->getLangTitle();
		$this->data['langDesc'] = $this->getLangDesc();
		$this->data['interest'] = $this->getInterest();

		$this->data['objective'] = $this->getObjective();
		$this->data['jobTitle'] = $this->getJobTitle();
		$this->data['jobTime'] = $this->getJobTime();
		$this->data['jobCompany'] = $this->getJobCompany();
		$this->data['jobDesc'] = $this->getJobDesc();
		$this->data['projectLink'] = $this->getProjectLink();
		$this->data['projectName'] = $this->getProjectName();
		$this->data['projectDesc'] = $this->getProjectDesc();
		$this->data['skillsTitle'] = $this->getSkillsTitle();
		$this->data['skillsLevel'] = $this->getSkillsLevel();

		$this->load->view('main/main',$this->data);
	}

	function getImage(){
		return getenv("APP_URL")."/".("assets/images/amirul.png");
	}

	function getName(){
		return "Amirul Hakim";
	}

	function getTagline(){
		return "Software Engineer";
	}

	function getEmail(){
		return "amirulhakimzailan@gmail.com";
	}

	function getPhone(){
		return "+60 13 671 5075";
	}

	function getSocial(){
		$socials = array("website","linkedin","github","gitlab","bitbucket","twitter");
		return $socials;
	}

	function getSocialIcon(){
		$socialsIcon = array("fa fa-globe","fab fa-linkedin","fab fa-github","fab fa-gitlab","fab fa-bitbucket","fab fa-twitter");
		return $socialsIcon;
	}

	function getSocialLinks(){
		$links = array("https://mushmero.com", "https://www.linkedin.com/in/muhdamirulhakim/", "https://github.com/mushmero", "https://gitlab.com/mushmero", "https://bitbucket.org/mushmero", "https://twitter.com/mushmero");
		return $links;
	}

	function getSocialText(){
		$text = array("mushmero.com", "@mushmero", "@mushmero", "@mushmero", "@mushmero", "@mushmero");
		return $text;
	}

	function getEduTitle(){
		$eduTitle = array("B. of Computer Science (Computer System)");
		return $eduTitle;
	}

	function getEduPlace(){
		$eduPlace = array("Universiti Putra Malaysia");
		return $eduPlace;
	}

	function getEduTime(){
		$eduTime = array("2011 - 2015");
		return $eduTime;
	}

	function getLangTitle(){
		$langTitle = array("Malay", "English");
		return $langTitle;
	}

	function getLangDesc(){
		$langDesc = array("(Native)", "(Professional)");
		return $langDesc;
	}

	function getInterest(){
		$interest = array("Travel", "Food", "Photography", "Explore new things");
		return $interest;
	}

	function getObjective(){
		return "The purpose of writing this resume is to acquire the suitable job in information & technology field with experiences & skills. I am graduated from Universiti Putra Malaysia (UPM) in B. Computer Science in 2015. Since 2015 I have been developing some mini projects regarding web applications using Codeigniter, setup DO VPS, using Git for my code repo and now exploring Alibaba Cloud.";
	}

	function getJobTitle(){
		$jobTitle = array("Software Engineer", "Programmer", "IT Support Technician", "Internship Student");
		return $jobTitle;
	}

	function getJobTime(){
		$jobTime = array("Jan 2019 (current)", "June 2016", "March 2016", "July 2014 - February 2015");
		return $jobTime;
	}

	function getJobCompany(){
		$jobCompany = array("Asiankom Communication (M) Sdn Bhd, Cheras", "Ministry of Education, Putrajaya", "HRA Solutions, Seri Kembangan", "Xentral Methods, Cyberjaya");
		return $jobCompany;
	}

	function getJobDesc(){
		$jobDesc = array(
			[
				"Develop new inhouse recording webapps to play voice recording from Asterisk PABX platform using PHP Codeigniter Framework, Admin Bootstrap template, Javascript and MySQL",
				"Develop new callcenter wallboard using PHP Codeigniter framework, MySQL and Bootstrap4",
				"Manage & deploy LAMP stack on Ubuntu & Centos Server",
				"Maintain inhouse softphone by upgrading with new SDK engine using C#",
				"Maintain MySQL DB on CallCenter system based on customer request",
				"Develop & customize inhouse reporting manager based on customer request using PHP & MySQL",
				"Maintain inhouse CRMS application develop using PHP & MySQL",
				"Maintain & do patch work to inhouse callcenter application",
				"Upgrading company websites to Wordpress (<a href='https://asiankom.com' target='_blank'>Asiankom.com</a>) & (<a href='https://Centiumsoft.com' target='_blank'>Centiumsoft.com</a>)",
				"Configuring Sangfor HyperConverged Infrastructure (HCI) for production",
				"Troubleshoot issues on end user site in callcenter environment",
				"Engage and educate end user to solve issues that occured during operation"
			],
			["Assist existing development team in developing a system that manage teacher to apply position for school headmaster or school principal using inhouse php framework and oracle DB. The system will be use by entire national school teachers and also staff on state education department."],
			["Assist and support IT staff on CitiBank Tower as an support technician by helping them migrating and transferring old PC to new PC as well as implementing software backup and restore."],
			["As an internship student, helping them convert conventional format ebook which is (PDF) into ebook format (EPUB). Propose an idea by having multiple checkbox for their main site."]
		);
		return $jobDesc;
	}

	function getProjectLink(){
		$projectLink = array("https://ncov19.mushmero.com", "https://bitbucket.org/mushmero/seller-pro/src/master/", "https://bitbucket.org/mushmero/attendance-pro/src/master/", "https://github.com/mushmero/Arduino_Gardening_Wireless_MongoDB", "https://github.com/mushmero/Roboshop");
		return $projectLink;
	}

	function getProjectName(){
		$projectName = array("2019-nCoV Dashboard", "Proseller", "Attendance-Pro</a><small> in progress</small>", "Humidity, Temperature & UV sensor</a><small> in progress</small>", "Roboshop");
		return $projectName;
	}

	function getProjectDesc(){
		$projectDesc = array(
			"This is a dashboard that show information on coronavirus cases worldwide & especially in Malaysia. This project in use an API to obtain the data and built uisng CodeIgniter framework, Bootstrap 4, Datatables and ChartJs.",
			"A project that assist a standalone online seller to manage their inventory and financial flow. Develop by using CodeIgniter framework, Bootstrap framework, AdminLTE css, dompdf and chartjs",
			"A simple system that will be use to track staff attendance on a company and generate report based on month or department. Develop by using CodeIgniter framework, Bootstrap framework, AdminLTE css and dompdf.",
			"An IoT project that will read humidity, temperature and UV reading from environment and transmit the data into online database over wireless network. Develop by using Arduino board, UV sensors, DHT sensors, ESP8266 Wifi Module.",
			"A FYP project for B. Computer Science. A system to go shoppign using robot that control using Xbox Kinect Sensor. The robot is build using LEGO Mindstorm & Tetrix. The program is code in JAVA."
		);
		return $projectDesc;
	}

	function getSkillsTitle(){
		$skillsTitle = array("PHP", "HTML5 &amp; CSS", "Javascript &amp; jQuery", "Git", "SVN", "DigitalOcean Cloud Server", "Alibaba Cloud");
		return $skillsTitle;
	}

	function getSkillsLevel(){
		$skillsLevel = array("85%", "85%", "20%", "40%", "30%", "15%", "5%");
		return $skillsLevel;
	}
}