<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Template extends CI_Controller {
	var $data = array();
	function __construct(){
		parent::__construct();
		$this->load->model('templatemodel');
		$this->load->model('usermodel');
		$this->load->model('socialmodel');
		$this->load->model('educationmodel');
		$this->load->model('languagemodel');
		$this->load->model('interestmodel');
		$this->load->model('objectivemodel');
		$this->load->model('experiencemodel');
		$this->load->model('projectmodel');
		$this->load->model('skillmodel');
		$this->load->model('settingmodel');
		$this->load->library('systools');
		$this->load->library('colors');
		$this->load->library('pdf');
		date_default_timezone_set('Asia/Kuala_Lumpur');
		/*header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$this->load->library('userlib');
		if ($this->userlib->checkLogin() === FALSE) {
			$this->userlib->removeLogin();
			redirect(getenv("APP_URL").'login/noaccess');
		} else {
			date_default_timezone_set("Asia/Kuala_Lumpur");
			$this->data['user'] = $this->session->userdata('user');
		}*/
	}
	public function index(){
		$this->systools->saveVisitor();
		$body = $this->templatemodel->getActiveTemplate();
		if(!empty($body)){
			$template = $this->completetemplate($body->design,$body->maincolor,$body->accentcolor);
		}else{
			$template = $this->notemplate();
		}
		$this->data['template'] = $template;
		$this->load->view('admin/templates/template',$this->data);
	}
	function notemplate(){
		$notemplate = file_get_contents($this->systools->getviewtemplate('admin/templates/','notemplate'));
		return $notemplate;
	}
	function getresumetemplate(){
		$body = $this->templatemodel->getActiveTemplate();
		$template = $this->completetemplate($body->design,$body->maincolor,$body->accentcolor);
		$this->data['template'] = $template;
		$html = $this->load->view('admin/pdf/template',$this->data,true);
		return $html;
	}
	function exportPDF(){		
		$user = $this->usermodel->getActiveProfile();
		$filename = "Resumed_".str_replace(' ','_',$user->fullname)."_".date('Ymd').".pdf";
		$html = html_entity_decode($this->getresumetemplate());
		
		//MPDF		
		//$config = $this->getpdfsetting();
		//$this->pdf->generateMPDF($config,$filename);
		// DOMPDF
		//$this->pdf->generateDomPDF($html,'A4','Portrait',$filename);
		
		/*wkhtmltopdf*/
		$config = $this->wkhtmlsetting();
		$this->pdf->generatewkhtmltopdf($config,$html,$filename);
	}
	function getpdfsetting(){
		$pdfsetting = $this->settingmodel->getpdfsetting();
		$config = array(
			'margin_left' => $pdfsetting->margin_left,
			'margin_right' => $pdfsetting->margin_right,
			'margin_top' => $pdfsetting->margin_top,
			'margin_bottom' => $pdfsetting->margin_bottom,
			'margin_header' => $pdfsetting->header,
			'margin_footer' => $pdfsetting->footer,
			'orientation' => 'P',
			'tempDir' => $this->pdf->tempdir(),
			'mode' => 'c',
		);
		return $config;
	}
	function wkhtmlsetting(){
		$pdfsetting = $this->settingmodel->getpdfsetting();
		$setting = array(
			'no-outline',         // Make Chrome not complain
		    'margin-top'    => $pdfsetting->margin_top,
		    'margin-right'  => $pdfsetting->margin_right,
		    'margin-bottom' => $pdfsetting->margin_bottom,
		    'margin-left'   => $pdfsetting->margin_left,
		    'header-spacing' => $pdfsetting->header,
		    'footer-spacing' => $pdfsetting->footer, 
		);
		return $setting;
	}
	function getImage($img){
		$image = '';
		if(!empty($img)){
			$image = "<div class=\"widget-user-image\">
                <img class=\"img-circle elevation-2\" src=\"".getenv("APP_URL").$img->filepath."/".$img->filename."\" alt=\"$img->altname\">
              </div>";
         }		
        return $image;
	}
	function createLink($link,$linkname,$type){
		$link = "<a class=\"text-color-primary\" href=\"".$type.$link."\" target=\"_blank\">".$linkname."</a>";
		return $link;
	}
	function getSocial($social){
		$socials = '';
		if(!empty($social)){
			for($i = 0; $i < count($social); $i++){
				$socials .= "<li class=\"social text-color-primary\"><i class=\"".$social[$i]->iconname."\"></i><a class=\"text-color-primary\" href=\"".$social[$i]->link."\" target=\"_blank\"> ".$social[$i]->name."</a></li>";
			}
		}		
		return $socials;
	}
	function getEducation($edu){
		$education = '';
		if(!empty($edu)){
			for($i = 0; $i < count($edu); $i++){
			$education .= "<div class=\"item\">
                            <div class=\"degree text-color-primary\">".$edu[$i]->name."</div>
                            <div class=\"institution text-color-primary\">".$edu[$i]->institution."</div>
                            <div class=\"year text-color-primary\">".$edu[$i]->yearstart."-".$edu[$i]->yearend."</div>
	                        </div>";
			}
		}		
		return $education;
	}
	function getLanguage($lang){
		$language = '';
		if(!empty($lang)){
			for($i = 0; $i < count($lang); $i++){
				$language .= "<li class=\"text-color-primary\">".$lang[$i]->name." <span class=\"lang-proficiency\">".$lang[$i]->proficiency."</span></li>";
			}
		}
		return $language;
	}
	function getInterest($interest){
		$interests = '';
		if(!empty($interest)){
			for($i = 0; $i < count($interest); $i++){
				$interests .= "<li class=\"text-color-primary\">".$interest[$i]->name."</li>";
			}
		}
		return $interests;
	}
	function getExperience($exp){
		$experience = '';
		if(!empty($exp)){
			for($i = 0; $i < count($exp); $i++){
				if($exp[$i]->enddate != 'current'){
					$enddate = $exp[$i]->enddate;
				}else{
					$enddate = ucfirst($exp[$i]->enddate);
				}
				$experience .= "<div class=\"item\">
                            <div class=\"meta text-color-secondary\">
                                <div class=\"upper-row\">
                                    <h3 class=\"job-title\"><strong>".$exp[$i]->name."</strong></h3>
                                    <div class=\"time\">".$exp[$i]->startdate." - ".$enddate."</div>
                                </div>
                                <div class=\"company text-color-secondary\">".$exp[$i]->employer."</div>
                            </div>
                            <div class=\"details text-color-secondary\">".$exp[$i]->description."</div>
                        </div>";
			}
		}
		return $experience;
	}
	function getProject($proj){
		$project = '';
		if(!empty($proj)){
			for($i = 0; $i < count($proj); $i++){
				$project .= "<div class=\"item\">
                            <span class=\"project-title\"><strong>".$proj[$i]->name."</strong></span> - (<small>".$proj[$i]->status."</small>)<span class=\"project-tagline\">".$proj[$i]->description."</span>
                        </div>";
			}
		}
		return $project;
	}
	function getSkill($skill){
		$skills = '';
		if(!empty($skill)){
			for($i = 0; $i < count($skill); $i++){
				$skills .= "<div class=\"item\">
                                <h3 class=\"skill-title\">".$skill[$i]->name."</h3>
                                <div class=\"skill-level bg-light\">
                                    <div class=\"level-bar-inner main-color\" data-level=\"".$skill[$i]->percentage."%\" style=\"width:".$skill[$i]->percentage."%\"></div>
                                </div>
                            </div>";
			}
		}
		return $skills;
	}
	function completetemplate($design,$maincolor = '',$accentcolor = ''){
		$fields = $this->systools->templatefields();
		$profile = $this->usermodel->getActiveProfile();
		$socials = $this->socialmodel->getSocial();
		$education = $this->educationmodel->getEducation();
		$language = $this->languagemodel->getLanguage();
		$interest = $this->interestmodel->getInterest();
		$objective = $this->objectivemodel->getActiveObjective();
		$experience = $this->experiencemodel->getExperience();
		$project = $this->projectmodel->getProject();
		$skill = $this->skillmodel->getSkill();
		foreach($fields as $key => $value){
			if($value == 'profile-image'){
				$image = $this->getImage($profile);
				$design = $this->systools->replacevar($value,$image,$design);
			}
			if($value == 'profile-name'){
				$design = $this->systools->replacevar($value,$profile->fullname,$design);
			}
			if($value == 'profile-tagline'){
				$design = $this->systools->replacevar($value,$profile->tagline,$design);
			}
			if($value == 'profile-email'){
				$email = $this->createLink($profile->email,$profile->email,'mailto:');
				$design = $this->systools->replacevar($value,$email,$design);
			}
			if($value == 'profile-phone'){
				$phone = $this->createLink($profile->phone,$profile->phone,'tel:');
				$design = $this->systools->replacevar($value,$phone,$design);
			}
			if($value == 'profile-website'){
				$website = $this->createLink($profile->website,$profile->website,'');
				$design = $this->systools->replacevar($value,$website,$design);
			}
			if($value == 'social-info'){
				$social = $this->getSocial($socials);
				$design = $this->systools->replacevar($value,$social,$design);
			}
			if($value == 'education-info'){
				$edu = $this->getEducation($education);
				$design = $this->systools->replacevar($value,$edu,$design);
			}
			if($value == 'language-info'){
				$lang = $this->getLanguage($language);
				$design = $this->systools->replacevar($value,$lang,$design);
			}
			if($value == 'interest-info'){
				$interestinfo = $this->getInterest($interest);
				$design = $this->systools->replacevar($value,$interestinfo,$design);
			}
			if($value == 'objective-info'){
				$design = $this->systools->replacevar($value,$objective->data,$design);
			}
			if($value == 'experience-info'){
				$exp = $this->getExperience($experience);
				$design = $this->systools->replacevar($value,$exp,$design);
			}
			if($value == 'project-info'){
				$proj = $this->getProject($project);
				$design = $this->systools->replacevar($value,$proj,$design);
			}
			if($value == 'skill-info'){
				$skills = $this->GetSkill($skill);
				$design = $this->systools->replacevar($value,$skills,$design);
			}
			if($value == 'main-color'){
				$design = $this->systools->replacevar($value,$maincolor,$design);
			}
			if($value == 'accent-color'){
				$design = $this->systools->replacevar($value,$accentcolor,$design);
			}
			if($value == 'text-color-primary'){
				$primary = $this->colors->gettextcolor($maincolor);
				$design = $this->systools->replacevar($value,$primary,$design);				
			}
			if($value == 'text-color-secondary'){
				$secondary = $this->colors->gettextcolor($accentcolor);
				$design = $this->systools->replacevar($value,$secondary,$design);
			}
		}
		return $design;
	}
}