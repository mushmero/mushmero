<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skills extends CI_Controller {
	var $data = array();
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Kuala_Lumpur');
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$this->load->model('usermodel');
		$this->load->model('skillmodel');
		$this->load->library('userlib');
		if ($this->userlib->checkLogin() === FALSE) {
			$this->userlib->removeLogin();
			redirect(getenv("APP_URL").'noaccess');
		} else {
			date_default_timezone_set("Asia/Kuala_Lumpur");
			$this->data['user'] = $this->session->userdata('user');
		}
	}
	public function index()
	{
		$skill = $this->skillmodel->getSkill();
		$active = $this->usermodel->getActiveProfile();

		if(empty($active)){
			$picture = '';
			$altname = '';
			$fullname = '';
			$tagline = '';
			$email = '';
			$phone = '';
			$website = '';
		}else{
			$picture = getenv("APP_URL").$active->filepath."/".$active->filename;
			$altname = $active->altname;
			$fullname = $active->fullname;
			$tagline = $active->tagline;
			$email = $active->email;
			$phone = str_replace(" ","",$active->phone);
			$website = $active->website;
		}

		$this->data['picture'] = $picture;
		$this->data['altname'] = $altname;
		$this->data['fullname'] = $fullname;
		$this->data['tagline'] = $tagline;
		$this->data['email'] = $email;
		$this->data['phone'] = $phone;
		$this->data['website'] = $website;
		$this->data['skills'] = $skill;
		$this->load->view('admin/skills',$this->data);
	}
	public function save(){
		$name = $this->input->post('skillname');
		$percentage = $this->input->post('skillpercentage');

		if(empty($name)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Skill is empty',
			);
			echo json_encode($msg);
		}
		if(empty($percentage)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Percentage is empty',
			);
			echo json_encode($msg);
		}
		if(!empty($name) && !empty($percentage)){
			$data = array(
				'name' => $name,
				'percentage' => $percentage,
				'createdtime' => date('Y-m-d H:i:s'),
			);
			$save = $this->skillmodel->save($data);
			if($save){
				$msg = array(
					'icon' => 'success',
					'msg' => 'Record saved!',
				);
				echo json_encode($msg);
			}else{
				$msg = array(
					'icon' => 'error',
					'msg' => 'Unable to save',
				);
				echo json_encode($msg);
			}
		}
	}
	public function remove($id){
		$this->skillmodel->remove($id);
		return true;
	}
	public function get($id){
		$result = $this->skillmodel->getSkillSingleData($id);
		if($result){
			echo json_encode($result);
		}return false;
	}
	public function update($id){
		$params = array(
			'name' => $this->input->post('skillnameedit'.$id),
			'percentage' => $this->input->post('skillpercentageedit'.$id),
			'modifiedtime' => date('Y-m-d H:i:s'),
		);
		$this->skillmodel->updateSkillById($id,$params);
		return true;
	}
}