<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personal extends CI_Controller {
	var $data = array();

	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Kuala_Lumpur');
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$this->load->model('usermodel');
		$this->load->model('attachmentmodel');
		$this->load->library('upload');
		$this->load->library('systools');
		$this->load->library('userlib');
		if ($this->userlib->checkLogin() === FALSE) {
			$this->userlib->removeLogin();
			redirect(getenv("APP_URL").'noaccess');
		} else {
			date_default_timezone_set("Asia/Kuala_Lumpur");
			$this->data['user'] = $this->session->userdata('user');
		}
	}
	public function index()
	{
		$user = $this->usermodel->getData();
		$active = $this->usermodel->getActiveProfile();
		if(empty($active)){
			$picture = '';
			$altname = '';
			$fullname = '';
			$tagline = '';
			$email = '';
			$phone = '';
			$website = '';
		}else{
			$picture = getenv("APP_URL").$active->filepath."/".$active->filename;
			$altname = $active->altname;
			$fullname = $active->fullname;
			$tagline = $active->tagline;
			$email = $active->email;
			$phone = str_replace(" ","",$active->phone);
			$website = $active->website;
		}

		$this->data['picture'] = $picture;
		$this->data['altname'] = $altname;
		$this->data['fullname'] = $fullname;
		$this->data['tagline'] = $tagline;
		$this->data['email'] = $email;
		$this->data['phone'] = $phone;
		$this->data['website'] = $website;
		$this->data['user'] = $user;
		$this->data['active'] = $active;
		$this->load->view('admin/personal',$this->data);
	}

	public function save(){
		$photo = $_FILES['uploadprofilephoto'];
		$photofield = 'uploadprofilephoto';
		$name = $this->input->post('fullname');
		$tagline = $this->input->post('tagline');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$website = $this->input->post('website');
		if(empty($name)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Name is empty',
			);
			echo json_encode($msg);
		}
		if(empty($tagline)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Tagline is empty',
			);
			echo json_encode($msg);
		}
		if(empty($email)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Email is empty',
			);
			echo json_encode($msg);
		}
		if(empty($phone)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Phone is empty',
			);
			echo json_encode($msg);
		}
		if(!empty($photo) && !empty($name) && !empty($tagline) && !empty($email) && !empty($phone)){	
			$config = array(
				'upload_path' 	=> $this->systools->getUploadPath(),
				'allowed_types' => 'gif|jpg|jpeg|png',
				'max_size'	 	=> '1024',
				'file_name'		=> date('ymd').'_'.date('His').'_profile_'.strtolower(str_replace(" ","",$name)),
			);
			$this->upload->initialize($config);
			if(!$this->upload->do_upload($photofield)){
				$msg = array(
					'icon' => 'error',
					'msg' => strip_tags($this->upload->display_errors()),
				);
				echo json_encode($msg);
			}else{
				$data = $this->upload->data();
				$attachment = array(
					'altname'	=> $data['raw_name'],
					'filename'	=> $data['file_name'],
					'filepath' 	=> $config['upload_path'],
					'filetype'	=> $data['file_type'],
					'tag'	=> 'profile',
					'createdtime' => date('Y-m-d H:i:s'),
				);
				$attachmentid = $this->attachmentmodel->save($attachment);
			}
			$userdata = array(
				'fullname'	=>	$name,
				'tagline'	=>	$tagline,
				'email'		=>	$email,
				'phone'		=>	$phone,
				'website' 	=>	$website,
				'attachmentid'	=>	$attachmentid,
				'createdtime' => date('Y-m-d H:i:s'),
			);
			$save = $this->usermodel->save($userdata);
			if($save){
				$msg = array(
					'icon' => 'success',
					'msg' => 'Record saved!',
				);
				echo json_encode($msg);
			}else{
				$msg = array(
					'icon' => 'error',
					'msg' => 'Unable to save',
				);
				echo json_encode($msg);
			}
		}
	}

	public function remove($id){
		$this->usermodel->remove($id);
		return true;
	}

	public function update($id){
		$photoform = 'uploadprofilephotoedit'.$id;
		$photo = $_FILES['uploadprofilephotoedit'.$id];
		$name = $this->input->post('fullnameedit'.$id);
		$tagline = $this->input->post('taglineedit'.$id);
		$email = $this->input->post('emailedit'.$id);
		$phone = $this->input->post('phoneedit'.$id);
		$website = $this->input->post('websiteedit'.$id);

		if(!empty($photo)){
			$config = array(
				'upload_path' 	=> $this->systools->getUploadPath(),
				'allowed_types' => 'gif|jpg|jpeg|png',
				'max_size'	 	=> '1024',
				'file_name'		=> date('ymd').'_'.date('His').'_profile_'.strtolower(str_replace(" ","",$name)),
			);
			$this->upload->initialize($config);
			if(!$this->upload->do_upload($photoform)){
				$msg = array(
					'icon' => 'error',
					'msg' => strip_tags($this->upload->display_errors()),
				);
				echo json_encode($msg);
			}else{
				$data = $this->upload->data();
				$attachment = array(
					'altname'	=> $data['raw_name'],
					'filename'	=> $data['file_name'],
					'filepath' 	=> $config['upload_path'],
					'filetype'	=> $data['file_type'],
					'tag'	=> 'profile',
					'createdtime' => date('Y-m-d H:i:s'),
				);
				$attachmentid = $this->attachmentmodel->save($attachment);
				$userdata = array(
					'fullname'	=>	$name,
					'tagline'	=>	$tagline,
					'email'		=>	$email,
					'phone'		=>	$phone,
					'website' 	=>	$website,
					'attachmentid'	=>	$attachmentid,
					'modifiedtime' => date('Y-m-d H:i:s'),
				);
				$this->usermodel->updateDataById($id,$userdata);
				$msg = array(
					'icon' => 'success',
					'msg'	=> 'Record was updated successfully',
				);
				echo json_encode($msg);
			}			
		}else{
			$userdata = array(
				'fullname'	=>	$name,
				'tagline'	=>	$tagline,
				'email'		=>	$email,
				'phone'		=>	$phone,
				'website' 	=>	$website,
				'modifiedtime' => date('Y-m-d H:i:s'),
			);
			$this->usermodel->updateDataById($id,$userdata);
			$msg = array(
				'icon' => 'success',
				'msg'	=> 'Record was updated successfully',
			);
			echo json_encode($msg);
		}	
	}

	public function countStatus(){
		$count = $this->usermodel->countActiveStatus();
		echo $count;
	}

	public function checkStatusById($id){
		$status = $this->usermodel->getStatusById($id);
		echo $status->status;
	}

	public function updateStatusById($id){
		$status = $this->input->post('status');
		$params = array(
			'status'	=> $status,
		);
		$this->usermodel->updateDataById($id,$params);
		return true;
	}
}