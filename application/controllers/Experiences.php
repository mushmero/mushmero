<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Experiences extends CI_Controller {
	var $data = array();
	function __construct(){
		parent::__construct();
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$this->load->model('usermodel');
		$this->load->model('experiencemodel');
		$this->load->library('userlib');
		if ($this->userlib->checkLogin() === FALSE) {
			$this->userlib->removeLogin();
			redirect(getenv("APP_URL").'noaccess');
		} else {
			date_default_timezone_set("Asia/Kuala_Lumpur");
			$this->data['user'] = $this->session->userdata('user');
		}
	}
	public function index()
	{
		$experience = $this->experiencemodel->getExperience();
		$active = $this->usermodel->getActiveProfile();

		if(empty($active)){
			$picture = '';
			$altname = '';
			$fullname = '';
			$tagline = '';
			$email = '';
			$phone = '';
			$website = '';
		}else{
			$picture = getenv("APP_URL").$active->filepath."/".$active->filename;
			$altname = $active->altname;
			$fullname = $active->fullname;
			$tagline = $active->tagline;
			$email = $active->email;
			$phone = str_replace(" ","",$active->phone);
			$website = $active->website;
		}

		$this->data['picture'] = $picture;
		$this->data['altname'] = $altname;
		$this->data['fullname'] = $fullname;
		$this->data['tagline'] = $tagline;
		$this->data['email'] = $email;
		$this->data['phone'] = $phone;
		$this->data['website'] = $website;
		$this->data['experiences'] = $experience;
		$this->load->view('admin/experiences',$this->data);
	}
	public function save(){
		$name = $this->input->post('expname');
		$expemployer = $this->input->post('expemployer');
		$expdesc = $this->input->post('expdesc');
		$expstart = $this->input->post('expstart');
		$expend = $this->input->post('expend');	

		if(empty($name)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Experience is empty',
			);
			echo json_encode($msg);
		}
		if(empty($expstart)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Experience length is empty',
			);
			echo json_encode($msg);
		}
		if(empty($expend)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Experience length is empty',
			);
			echo json_encode($msg);
		}
		if(empty($expemployer)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Employer length is empty',
			);
			echo json_encode($msg);
		}
		if(empty($expdesc)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Description is empty',
			);
			echo json_encode($msg);
		}
		if(!empty($name) && !empty($expstart) && !empty($expend) && !empty($expemployer) && !empty($expdesc)){
			$data = array(
				'name' => $name,
				'startdate' => $expstart,
				'enddate' => $expend,
				'employer' => $expemployer,
				'description' => $expdesc,
				'createdtime' => date('Y-m-d H:i:s'),
			);
			$save = $this->experiencemodel->save($data);
			if($save){
				$msg = array(
					'icon' => 'success',
					'msg' => 'Record saved!',
				);
				echo json_encode($msg);
			}else{
				$msg = array(
					'icon' => 'error',
					'msg' => 'Unable to save',
				);
				echo json_encode($msg);
			}
		}
	}
	public function remove($id){
		$this->experiencemodel->remove($id);
		return true;
	}
	public function get($id){
		$result = $this->experiencemodel->getExperienceSingleData($id);
		if($result){
			echo json_encode($result);
		}return false;
	}
	public function update($id){
		if(empty($this->input->post('expstart')) && empty($this->input->post('expend'))){
			$params = array(
				'name' => $this->input->post('expnameedit'),
				'employer' => $this->input->post('expemployeredit'),
				'description' => $this->input->post('expdescedit'),
				'modifiedtime' => date('Y-m-d H:i:s'),
			);
		}else{
			$params = array(
				'name' => $this->input->post('expnameedit'),
				'startdate' => $this->input->post('expstart'),
				'enddate' => $this->input->post('expend'),
				'employer' => $this->input->post('expemployeredit'),
				'description' => $this->input->post('expdescedit'),
				'modifiedtime' => date('Y-m-d H:i:s'),
			);
		}
		$this->experiencemodel->updateExperienceById($id,$params);
		return true;
	}
}