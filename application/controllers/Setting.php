<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {
	var $data = array();
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Kuala_Lumpur');
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$this->load->model('usermodel');
		$this->load->model('settingmodel');
		$this->load->model('attachmentmodel');
		$this->load->model('announcementmodel');
		$this->load->model('templatemodel');
		$this->load->library('upload');
		$this->load->library('systools');
		$this->load->library('colors');
		$this->load->library('userlib');
		if ($this->userlib->checkLogin() === FALSE) {
			$this->userlib->removeLogin();
			redirect(getenv("APP_URL").'noaccess');
		} else {
			date_default_timezone_set("Asia/Kuala_Lumpur");
			$this->data['user'] = $this->session->userdata('user');
		}
	}
	public function index()
	{
		$social = $this->settingmodel->getSocial();
		$education = $this->settingmodel->getEducation();
		$language = $this->settingmodel->getLanguage();
		$project = $this->settingmodel->getProject();
		$active = $this->usermodel->getActiveProfile();
		$todo = $this->settingmodel->getActivity();
		$bg = $this->attachmentmodel->getbackground();
		$announcement = $this->announcementmodel->getActiveInfo();
		$template = $this->templatemodel->getAllTemplate();
		$activetemplate = $this->templatemodel->getActiveTemplate();
		$templatefields = $this->systools->templatefields();
		$pdfsetting = $this->settingmodel->getpdfsetting();
		foreach($templatefields as $key => $value){
			$optionvalue[] = '<option value="'.$key.'">'.$value.'</option>';
		}

		if(empty($active)){
			$picture = '';
			$altname = '';
			$fullname = '';
			$tagline = '';
			$email = '';
			$phone = '';
			$website = '';
		}else{
			$picture = getenv("APP_URL").$active->filepath."/".$active->filename;
			$altname = $active->altname;
			$fullname = $active->fullname;
			$tagline = $active->tagline;
			$email = $active->email;
			$phone = str_replace(" ","",$active->phone);
			$website = $active->website;
		}

		$this->data['picture'] = $picture;
		$this->data['altname'] = $altname;
		$this->data['fullname'] = $fullname;
		$this->data['tagline'] = $tagline;
		$this->data['email'] = $email;
		$this->data['phone'] = $phone;
		$this->data['website'] = $website;
		$this->data['social'] = $social;
		$this->data['education'] = $education;
		$this->data['language'] = $language;
		$this->data['project'] = $project;
		$this->data['todo'] = $todo;
		$this->data['background'] = $bg;
		$this->data['announcement'] = $announcement;
		$this->data['template'] = $template;
		$this->data['activetemplate'] = $activetemplate;
		$this->data['optionvalue'] = $optionvalue;
		$this->data['bgcolor'] = $this->colors->bgcolor();
		$this->data['pdfsetting'] = $pdfsetting;
		$this->load->view('admin/setting',$this->data);
	}
	public function social(){
		$iconname = $this->input->post('inputIconName');
		$icontype = $this->input->post('inputType');

		if(empty($iconname)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Icon Name is empty',
			);
			echo json_encode($msg);
		}
		if(empty($icontype)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Icon Type is empty',
			);
			echo json_encode($msg);
		}
		if(!empty($iconname) && !empty($icontype)){
			$data = array(
				'iconname' => $iconname,
				'icontype' => $icontype,
				'createdtime' => date('Y-m-d H:i:s'),
			);
			$save = $this->settingmodel->saveSocial($data);
			if($save){
				$msg = array(
					'icon' => 'success',
					'msg' => 'Record saved!',
				);
				echo json_encode($msg);
			}else{
				$msg = array(
					'icon' => 'error',
					'msg' => 'Unable to save',
				);
				echo json_encode($msg);
			}
		}
	}
	public function removesocial($id){
		$this->settingmodel->removeSocial($id);
		return true;
	}
	public function getsocial($id){
		$result = $this->settingmodel->getSocialSingleData($id);
		if($result){
			echo json_encode($result);
		}return false;
	}
	public function updatesocial($id){
		$params = array(
			'iconname' => $this->input->post('iconname'),
			'icontype' => $this->input->post('icontype'),
			'modifiedtime' => date('Y-m-d H:i:s'),
		);
		$this->settingmodel->updateSocialById($id,$params);
		return true;
	}
	/*education*/
	public function education(){
		$level = $this->input->post('inputLevel');

		if(empty($level)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Level is empty',
			);
			echo json_encode($msg);
		}
		if(!empty($level)){
			$data = array(
				'level' => $level,
				'createdtime' => date('Y-m-d H:i:s'),
			);
			$save = $this->settingmodel->saveEducation($data);
			if($save){
				$msg = array(
					'icon' => 'success',
					'msg' => 'Record saved!',
				);
				echo json_encode($msg);
			}else{
				$msg = array(
					'icon' => 'error',
					'msg' => 'Unable to save',
				);
				echo json_encode($msg);
			}
		}
	}
	public function removeeducation($id){
		$this->settingmodel->removeEducation($id);
		return true;
	}
	public function geteducation($id){
		$result = $this->settingmodel->getEducationSingleData($id);
		if($result){
			echo json_encode($result);
		}return false;
	}
	public function updateeducation($id){
		$params = array(
			'level' => $this->input->post('level'),
			'modifiedtime' => date('Y-m-d H:i:s'),
		);
		$this->settingmodel->updateEducationById($id,$params);
		return true;
	}
	/* language */
	public function language(){
		$proficiency = $this->input->post('inputProficiency');

		if(empty($proficiency)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Proficiency is empty',
			);
			echo json_encode($msg);
		}
		if(!empty($proficiency)){
			$data = array(
				'proficiency' => $proficiency,
				'createdtime' => date('Y-m-d H:i:s'),
			);
			$save = $this->settingmodel->saveLanguage($data);
			if($save){
				$msg = array(
					'icon' => 'success',
					'msg' => 'Record saved!',
				);
				echo json_encode($msg);
			}else{
				$msg = array(
					'icon' => 'error',
					'msg' => 'Unable to save',
				);
				echo json_encode($msg);
			}
		}
	}
	public function removelanguage($id){
		$this->settingmodel->removeLanguage($id);
		return true;
	}
	public function getlanguage($id){
		$result = $this->settingmodel->getLanguageSingleData($id);
		if($result){
			echo json_encode($result);
		}return false;
	}
	public function updatelanguage($id){
		$params = array(
			'proficiency' => $this->input->post('proficiency'),
			'modifiedtime' => date('Y-m-d H:i:s'),
		);
		$this->settingmodel->updateLanguageById($id,$params);
		return true;
	}
	/* project */
	public function project(){
		$project = $this->input->post('inputStatus');

		if(empty($project)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Status is empty',
			);
			echo json_encode($msg);
		}
		if(!empty($project)){
			$data = array(
				'status' => $project,
				'createdtime' => date('Y-m-d H:i:s'),
			);
			$save = $this->settingmodel->saveProject($data);
			if($save){
				$msg = array(
					'icon' => 'success',
					'msg' => 'Record saved!',
				);
				echo json_encode($msg);
			}else{
				$msg = array(
					'icon' => 'error',
					'msg' => 'Unable to save',
				);
				echo json_encode($msg);
			}
		}
	}
	public function removeproject($id){
		$this->settingmodel->removeProject($id);
		return true;
	}
	public function getproject($id){
		$result = $this->settingmodel->getProjectSingleData($id);
		if($result){
			echo json_encode($result);
		}return false;
	}
	public function updateproject($id){
		$params = array(
			'status' => $this->input->post('status'),
			'modifiedtime' => date('Y-m-d H:i:s'),
		);
		$this->settingmodel->updateProjectById($id,$params);
		return true;
	}
	/* todo */
	public function activity(){
		$inputToDo = $this->input->post('inputToDo');
		$tododuedate = $this->input->post('tododuedate');

		if(empty($inputToDo)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Activity is empty',
			);
			echo json_encode($msg);
		}
		if(empty($tododuedate)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Due date is empty',
			);
			echo json_encode($msg);
		}
		if(!empty($inputToDo) && !empty($tododuedate)){
			$data = array(
				'activity' => $inputToDo,
				'due' => $tododuedate,
				'status' => 'New',
				'createdtime' => date('Y-m-d H:i:s'),
			);
			$save = $this->settingmodel->saveActivity($data);
			if($save){
				$msg = array(
					'icon' => 'success',
					'msg' => 'Record saved!',
				);
				echo json_encode($msg);
			}else{
				$msg = array(
					'icon' => 'error',
					'msg' => 'Unable to save',
				);
				echo json_encode($msg);
			}
		}
	}
	public function removeactivity($id){
		$this->settingmodel->removeActivity($id);
		return true;
	}
	public function getactivity($id){
		$result = $this->settingmodel->getActivitySingleData($id);
		if($result){
			echo json_encode($result);
		}return false;
	}
	public function updateactivity($id){
		$params = array(
			'activity' => $this->input->post('inputToDoedit'.$id),
			'due' => $this->input->post('tododuedateedit'.$id),
			'status' => 'Updated',
			'modifiedtime' => date('Y-m-d H:i:s'),
		);
		$this->settingmodel->updateActivityById($id,$params);
		return true;
	}
	/* upload background */
	public function uploadbg(){
		$bg = $_FILES['uploadbg'];
		$uploadbg = 'uploadbg';
		if(!empty($bg)){
			$config = array(
				'upload_path' 	=> $this->systools->getUploadPath(),
				'allowed_types' => 'gif|jpg|jpeg|png',
				'max_size'	 	=> '4096',
				'file_name'		=> date('ymd').'_'.date('His').'_bg_'.mt_rand(100,10000),
			);
			$this->upload->initialize($config);
			if(!$this->upload->do_upload($uploadbg)){
				$msg = array(
					'icon' => 'error',
					'msg' => strip_tags($this->upload->display_errors()),
				);
				echo json_encode($msg);
			}else{
				$data = $this->upload->data();
				$attachment = array(
					'altname'	=> $data['raw_name'],
					'filename'	=> $data['file_name'],
					'filepath' 	=> $config['upload_path'],
					'filetype'	=> $data['file_type'],
					'tag'	=> 'bg',
					'createdtime' => date('Y-m-d H:i:s'),
				);
				$save = $this->attachmentmodel->save($attachment);
				if($save){
					$msg = array(
						'icon' => 'success',
						'msg' => 'Record saved!',
					);
					echo json_encode($msg);
				}else{
					$msg = array(
						'icon' => 'error',
						'msg' => 'Unable to save',
					);
					echo json_encode($msg);
				}
			}
		}
	}
	/* announcement */
	public function announcement(){
		$announcement = $this->input->post('announcement');
		$id = $this->input->post('announcementid');
		if(!empty($id)){
			$data = array(
				'status' => 'retired',
				'modifiedtime' => date('Y-m-d H:i:s'),
			);
			$this->announcemenmtmodel->updateExistingInfo($id,$data);
		}
		if(empty($announcement)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Announcement is empty',
			);
			echo json_encode($msg);
		}
		if(!empty($announcement)){
			$data = array(
				'info' => $announcement,
				'status' => 'active',
				'createdtime' => date('Y-m-d H:i:s'),
			);
			$save = $this->announcemenmtmodel->save($data);
			if($save){
				$msg = array(
					'icon' => 'success',
					'msg' => 'Record saved!',
				);
				echo json_encode($msg);
			}else{
				$msg = array(
					'icon' => 'error',
					'msg' => 'Unable to save',
				);
				echo json_encode($msg);
			}
		}
	}
	/* templates */
	public function savetemplate(){
		$name = $this->input->post('templatename');
		$design = $this->input->post('templatedesign');
		$maincolor = $this->input->post('bgcolor');
		$accentcolor = $this->input->post('accentcolor');
		if(empty($name)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Template name is empty',
			);
			echo json_encode($msg);
		}
		if(empty($design)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Template design is empty',
			);
			echo json_encode($msg);
		}
		if(!empty($name) && !empty($design)){
			$data = array(
				'name' => $name,
				'design' => $design,
				'maincolor' => $maincolor,
				'accentcolor' => $accentcolor,
				'status' => 'new',
				'createdtime' => date('Y-m-d H:i:s'),
			);
			$save = $this->templatemodel->save($data);
			if($save){
				$msg = array(
					'icon' => 'success',
					'msg' => 'Record saved!',
				);
				echo json_encode($msg);
			}else{
				$msg = array(
					'icon' => 'error',
					'msg' => 'Unable to save',
				);
				echo json_encode($msg);
			}
		}
	}
	public function removetemplate($id){
		$this->templatemodel->remove($id);
		return true;
	}
	public function gettemplatename($id){
		$template = $this->templatemodel->getTemplate($id);
		if(!empty($template)){
			echo json_encode($template);
		}
	}
	public function updatetemplate($id){
		$existtemplate = $this->templatemodel->getTemplate($id);
		$name = $this->input->post('name');
		$design = $this->input->post('design');
		$maincolor = $this->input->post('maincolor');
		$accentcolor = $this->input->post('accentcolor');
		if($existtemplate->status == 'active'){
			$data = array(
				'name' => $name,
				'design' => $design,
				'maincolor' => $maincolor,
				'accentcolor' => $accentcolor,
				'modifiedtime' => date('Y-m-d H:i:s'),
			);
			$this->templatemodel->update($id,$data);
			return true;
		}else{
			$data = array(
				'name' => $name,
				'design' => $design,
				'maincolor' => $maincolor,
				'accentcolor' => $accentcolor,
				'status' => 'updated',
				'modifiedtime' => date('Y-m-d H:i:s'),
			);
			$this->templatemodel->update($id,$data);
			return true;
		}	
	}
	public function setactivetemplate($id){
		$activeid = $this->templatemodel->getActiveTemplate();
		if(!empty($activeid)){
			if($activeid->id == $id){
				$data = array(
					'status' => 'inactive',
					'modifiedtime' => date('Y-m-d H:i:s'),
				);
				$this->templatemodel->update($id,$data);
				return true;
			}else{
				$data = array(
					'status' => 'inactive',
					'modifiedtime' => date('Y-m-d H:i:s'),
				);
				$update = $this->templatemodel->update($activeid->id,$data);
				if($update){
					$params = array(
						'status' => 'active',
						'modifiedtime' => date('Y-m-d H:i:s'),
					);
					$this->templatemodel->update($id,$params);
					return true;
				}			
			}
		}else{
			$params = array(
				'status' => 'active',
				'modifiedtime' => date('Y-m-d H:i:s'),
			);
			$this->templatemodel->update($id,$params);
			return true;
		}		
	}
	public function getactivetemplateid($id){
		$activeid = $this->templatemodel->getActiveTemplateById($id);
		if(!empty($activeid)){
			echo json_encode($activeid);
		}else{
			echo 0;
		}		
	}
	public function pdfsetting(){
		$mtop = $this->input->post('margin-top');
		$mbottom = $this->input->post('margin-bottom');
		$mleft = $this->input->post('margin-left');
		$mright = $this->input->post('margin-right');
		$header = $this->input->post('header');
		$footer = $this->input->post('footer');

		if(!isset($mtop)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Margin Top is empty',
			);
			echo json_encode($msg);
		}
		if(!isset($mbottom)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Margin Bottom is empty',
			);
			echo json_encode($msg);
		}
		if(!isset($mleft)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Margin Left is empty',
			);
			echo json_encode($msg);
		}
		if(!isset($mright)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Margin Right is empty',
			);
			echo json_encode($msg);
		}
		if(!isset($header)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Header is empty',
			);
			echo json_encode($msg);
		}
		if(!isset($footer)){
			$msg = array(
				'icon' => 'error',
				'msg' => 'Footer is empty',
			);
			echo json_encode($msg);
		}
		if(isset($mtop) && isset($mbottom) && isset($mleft) && isset($mright) && isset($header) && isset($footer)){
			$data = array(
				'margin_top' => $mtop,
				'margin_bottom' => $mbottom,
				'margin_left' => $mleft,
				'margin_right' => $mright,
				'header' => $header,
				'footer' => $footer,
			);
			$save = $this->settingmodel->savepdfsetting($data);
			if($save){
				$msg = array(
					'icon' => 'success',
					'msg' => 'Record saved!',
				);
				echo json_encode($msg);
			}else{
				$msg = array(
					'icon' => 'error',
					'msg' => 'Unable to save',
				);
				echo json_encode($msg);
			}
		}		
	}
}