<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	var $data = array();
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Kuala_Lumpur');
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$this->load->model('usermodel');
		$this->load->model('visitormodel');
		$this->load->model('educationmodel');
		$this->load->model('experiencemodel');
		$this->load->model('interestmodel');
		$this->load->model('languagemodel');
		$this->load->model('objectivemodel');
		$this->load->model('projectmodel');
		$this->load->model('skillmodel');
		$this->load->model('socialmodel');
		$this->load->model('settingmodel');
		$this->load->model('announcementmodel');
		$this->load->library('systools');
		$this->load->library('userlib');
		if ($this->userlib->checkLogin() === FALSE) {
			$this->userlib->removeLogin();
			redirect(getenv("APP_URL").'noaccess');
		} else {
			date_default_timezone_set("Asia/Kuala_Lumpur");
			$this->data['user'] = $this->session->userdata('user');
		}
	}
	public function index()
	{
		$active = $this->usermodel->getActiveProfile();
		$visitorcount = $this->visitormodel->countAll();
		$career = $this->experiencemodel->getMinYear();
		$totalemployer = $this->experiencemodel->countEmployer();
		if(empty($active)){
			$picture = '';
			$altname = '';
			$fullname = '';
			$tagline = '';
			$email = '';
			$phone = '';
			$website = '';
		}else{
			$picture = getenv("APP_URL").$active->filepath."/".$active->filename;
			$altname = $active->altname;
			$fullname = $active->fullname;
			$tagline = $active->tagline;
			$email = $active->email;
			$phone = str_replace(" ","",$active->phone);
			$website = $active->website;
		}
		$bg = empty($bg) ? getenv("APP_URL")."/".("vendor/almasaeed2010/adminlte/dist/img/photo4.jpg") : $bg;

		$this->data['picture'] = $picture;
		$this->data['altname'] = $altname;
		$this->data['fullname'] = $fullname;
		$this->data['tagline'] = $tagline;
		$this->data['email'] = $email;
		$this->data['phone'] = $phone;
		$this->data['website'] = $website;
		$this->data['visitor'] = $visitorcount;
		$this->data['bg'] = $bg;
		$this->data['educount'] = empty($this->educationmodel->countEducation()) ? 0 : $this->educationmodel->countEducation();
		$this->data['expcount'] = empty($this->experiencemodel->countExperience()) ? 0 : $this->experiencemodel->countExperience();
		$this->data['intcount'] = empty($this->interestmodel->countInterest()) ? 0 : $this->interestmodel->countInterest();
		$this->data['langcount'] = empty($this->languagemodel->countLanguage()) ? 0 : $this->languagemodel->countLanguage();
		$this->data['objcount'] = empty($this->objectivemodel->countObjective()) ? 0 : $this->objectivemodel->countObjective();
		$this->data['projcount'] = empty($this->projectmodel->countProject()) ? 0 : $this->projectmodel->countProject();
		$this->data['skillcount'] = empty($this->skillmodel->countSkill()) ? 0 : $this->skillmodel->countSkill();
		$this->data['socialcount'] = empty($this->socialmodel->countSocial()) ? 0 : $this->socialmodel->countSocial();
		$this->data['activity'] = $this->settingmodel->getActiveActivity();
		$this->data['career'] = date('Y') - (!empty($career->mindate) ? date('Y', strtotime($career->mindate)) : date('Y'));
		$this->data['announcement'] = empty($this->announcementmodel->getActiveInfo()) ? 'No Announcement' : $this->announcementmodel->getActiveInfo();
		$this->data['totalemployer'] = empty($totalemployer) ? 0 : $totalemployer;
		$this->load->view('admin/admin',$this->data);
	}
	public function updateToDo($id){
		$params = array(
			'complete' => date('d/m/Y'),
			'status' => 'Completed',
			'modifiedtime' => date('Y-m-d H:i:s'),
		);
		$this->settingmodel->updateActivityById($id,$params);
		return true;
	}
	public function visitordata(){
		$data = $this->visitormodel->getVsitiorByCountry();
		echo json_encode($data);
	}
	public function visitorinfo(){
		$data = $this->visitormodel->getVsitiorByCountry();
		foreach($data as $key => $value){
			$visitor[] = (int)$value->visitor;
			$country[] = $value->country;
			$countrycode[] = $value->countrycode;
			$latitude[] = $value->latitude;
			$longitude[] = $value->longitude;
		}
		$result = array(
			array('visitor' => $visitor),
			array('country' => $country),
			array('countrycode' => $countrycode),
			array('latitude' => $latitude),
			array('longitude' => $longitude),
		);
		echo json_encode($result);
	}
}