<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Colors {
	function __construct(){
		$instance = &get_instance();
	}
	function bgcolor(){
		$color = array(
			'bg-navy' => 'bg-navy',
			'bg-olive' => 'bg-olive',
			'bg-lime' => 'bg-lime',
			'bg-fuchsia' => 'bg-fuchsia',
			'bg-maroon' => 'bg-maroon',
			'bg-blue' => 'bg-blue',
			'bg-indigo' => 'bg-indigo',
			'bg-purple' => 'bg-purple',
			'bg-pink' => 'bg-pink',
			'bg-red' => 'bg-red',
			'bg-orange' => 'bg-orange',
			'bg-yellow' => 'bg-yellow',
			'bg-green' => 'bg-green',
			'bg-teal' => 'bg-teal',
			'bg-cyan' => 'bg-cyan',
			'bg-white' => 'bg-white',
			'bg-gray' => 'bg-gray',
			'bg-gray-dark' => 'bg-gray-dark',
			'bg-gray-light' => 'bg-gray-light',
			'bg-black' => 'bg-black',
			'bg-success' => 'bg-success',
			'bg-danger' => 'bg-danger',
			'bg-info' => 'bg-info',
			'bg-warning' => 'bg-warning',
			'bg-light' => 'bg-light',
			'bg-dark' => 'bg-dark',
			'bg-primary' => 'bg-primary',
			'bg-secondary' => 'bg-secondary',
		);
		return $color;
	}
	function textcolor(){
		$color = array(
			'text-white' => 'text-white',
			'text-primary' => 'text-primary',
			'text-secondary' => 'text-secondary',
			'text-success' => 'text-success',
			'text-info' => 'text-info',
			'text-warning' => 'text-warning',
			'text-danger' => 'text-danger',
			'text-light' => 'text-light',
			'text-dark' => 'text-dark',
			'text-body' => 'text-body',
			'text-muted' => 'text-muted',
			'text-black-50' => 'text-black-50',
			'text-white-50' => 'text-white-50',
		);
		return $color;
	}
	function gettextcolor($color){
		if($color == 'bg-lime' || $color == 'bg-orange' || $color == 'bg-yellow' || $color == 'bg-white' || $color == 'bg-gray-light' || $color == 'bg-warning' || $color == 'bg-light'){
				$textcolor = 'text-dark';
		}else{
			$textcolor = 'text-white';
		}	
		return $textcolor;
	}
}