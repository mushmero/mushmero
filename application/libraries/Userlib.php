<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userlib {
	protected $instance;
	var $max = 28800;
	function __construct(){
		$this->instance = &get_instance();
	}
	function setLogin($data) {
		$this->instance->session->set_userdata(array('last_activity' => time(), 'logged_in' => 'yes', 'user' => $data));
	}
	function removeLogin() {
		$this->instance->session->unset_userdata('last_activity');
		$this->instance->session->unset_userdata('logged_in');
		$this->instance->session->unset_userdata('user');
	}
	function checkLogin() {
		$last_activity = $this->instance->session->userdata('last_activity');
		$logged_in = $this->instance->session->userdata('logged_in');
		$user = $this->instance->session->userdata('user');
		if ($logged_in == 'yes' && (time() - $last_activity) < $this->max) {
			$this->setLogin($user);
			return true;
		} else {
			$this->removeLogin();
			return false;
		}
	}
}