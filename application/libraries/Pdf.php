<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pdf {
	protected $instance;
	public function __construct(){
		$this->instance = &get_instance();
		$this->instance->load->library('systools');
	}
	function generateMPDF($config,$filename){
		$bootstrap = file_get_contents($this->instance->systools->getfile('vendor/twbs/bootstrap/dist/css/','bootstrap.min.css'));
		$css1 = file_get_contents($this->instance->systools->getfile('assets/css/','colors.css'));
		$css2 = file_get_contents($this->instance->systools->getfile('assets/css/','base.css'));
		$css3 = file_get_contents($this->instance->systools->getfile('assets/css/','template.css'));
		
		$data = array(
			'bootstrap' => $bootstrap,
			'css1'	=> $css1,
			'css2'	=> $css2,
			'css3'	=> $css3,
			'html' => $html,
		);

		$pdf = new \Mpdf\Mpdf($config);
		$pdf->WriteHTML($data['bootstrap'],\Mpdf\HTMLParserMode::HEADER_CSS);
		$pdf->WriteHTML($data['css1'],\Mpdf\HTMLParserMode::HEADER_CSS);
		$pdf->WriteHTML($data['css2'],\Mpdf\HTMLParserMode::HEADER_CSS);
		$pdf->WriteHTML($data['css3'],\Mpdf\HTMLParserMode::HEADER_CSS);
		$pdf->WriteHTML($data['html'],\Mpdf\HTMLParserMode::HTML_BODY);
		$pdf->Output($filename,"D");
	}
	function tempdir(){
		$temp = 'assets/temp/';
		$this->instance->systools->createDir($temp);
		return $temp;
	}
	function generateDomPDF($html,$size,$orientation,$filename,$stream=true){
		$pdf = new \Dompdf\Dompdf(array('isRemoteEnabled' => true));
		$pdf->loadHtml($html);
		$pdf->setPaper($size,$orientation);
		$pdf->render();
		if ($stream) {
	        $pdf->stream($filename, array("Attachment" => false));
	    } else {
	        return $pdf->output();
	    }
	}
	function generatewkhtmltopdf($config,$html,$filename){
		if(getenv('WKHTMLTOPDF_ENVIRONMENT') == 'windows'){
			$pdf = new \mikehaertl\wkhtmlto\Pdf(array(
				'binary' => getenv("WKHTMLTOPDF_BINPATH"),
			    'tmpDir' => $this->tempdir(),
			    'ignoreWarnings' => true,
			    'commandOptions' => array(
			        'useExec' => true,      // Can help on Windows systems
			        'procEnv' => array(
			            // Check the output of 'locale -a' on your system to find supported languages
			            'LANG' => 'en_US.utf-8',
			        ),
			        'escapeArgs' => false,
			        'procOptions' => array(
			            // This will bypass the cmd.exe which seems to be recommended on Windows
			            'bypass_shell' => true,
			            // Also worth a try if you get unexplainable errors
			            'suppress_errors' => true,
			        ),
			    ),
			));
		}else{
			$pdf = new \mikehaertl\wkhtmlto\Pdf();
		}		
		$pdf->setOptions($config);
		$pdf->addPage($html);
		if (!$pdf->send($filename)) {
		   throw new Exception('Could not create PDF: '.$pdf->getError());
		}
	}
}