<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Systools {
	function __construct(){
		$instance = &get_instance();
		$instance->load->library('upload');
		$instance->load->model('visitormodel');
	}

	function createDir($dir){
		if(!file_exists($dir)){
			mkdir($dir, 0755, true);
		}
	}

	function getUploadPath(){
		$path = 'assets/uploads/';
		$yearDir = $path.date('Y');
		$monthDir = $yearDir.'/'.date('m');
		$this->createDir($yearDir);
		$this->createDir($monthDir);
		$uploadPath = $monthDir.'/';
		return $uploadPath;
	}

	function ipLocation($ip){
		$geoip = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
		$location = array(
			'userip' => $geoip['geoplugin_request'],
			'city' => $geoip['geoplugin_city'],
			'country' => $geoip['geoplugin_countryName'],
			'countryCode' => $geoip['geoplugin_countryCode'],
			'continent'	=> $geoip['geoplugin_continentName'],
			'continentCode' => $geoip['geoplugin_continentCode'],
			'timezone'	=> $geoip['geoplugin_timezone'],
			'latitude' => $geoip['geoplugin_latitude'],
			'longitude' => $geoip['geoplugin_longitude'],
		);
		return $location;
	}

	function getVisitor($ip = NULL, $deepdetect = TRUE){
		if(filter_var($ip,FILTER_VALIDATE_IP) === FALSE){
			$ip = $_SERVER['REMOTE_ADDR'];
			if($deepdetect){
				if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP)){
                	$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
				}
	            if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP)){
	                $ip = $_SERVER['HTTP_CLIENT_IP'];
	            }
			}
		}
		if(isset($_SERVER['HTTP_REFERER'])){
			$data = array(
				'ip'	=>	$ip,
				'referrer'	=> $_SERVER['HTTP_REFERER'],
				'useragent'	=>	$_SERVER['HTTP_USER_AGENT'],
				'timestamp'	=> date('Y-m-d H:i:s a e'),
			);
		}else{
			$data = array(
				'ip'	=>	$ip,
				'referrer'	=> '',
				'useragent'	=>	$_SERVER['HTTP_USER_AGENT'],
				'timestamp'	=> date('Y-m-d H:i:s a e'),
			);
		}
		
		return $data;
	}
	function visitor(){
		$visitor = $this->getVisitor($_SERVER['REMOTE_ADDR']);
		$geoip = $this->ipLocation($visitor['ip']);
		$data = array_merge($visitor,$geoip);
		return $data;
	}
	function saveVisitor(){
		$instance = &get_instance();
		$visitor = $this->visitor();
		$data = array(
			'ipaddress'	=> $visitor['ip'],
			'referrer'	=> $visitor['referrer'],
			'useragent'	=> $visitor['useragent'],
			'datetime'	=> $visitor['timestamp'],
			'city'	=> $visitor['city'],
			'country'	=> $visitor['country'],
			'countrycode'	=> $visitor['countryCode'],
			'continent'	=> $visitor['continent'],
			'continentcode'	=> $visitor['continentCode'],
			'timezone'	=> $visitor['timezone'],
			'latitude'	=> $visitor['latitude'],
			'longitude'	=> $visitor['longitude'],
		);
		$instance->visitormodel->save($data);
	}
	function replacevar($find, $replace, $variable){
		return str_replace($find, $replace, $variable);
	}
	function templatefields(){
		$fields = array(
			'profile-image' => 'profile-image',
			'profile-name' => 'profile-name',
			'profile-tagline' => 'profile-tagline',
			'profile-email' => 'profile-email',
			'profile-phone' => 'profile-phone',
			'profile-website' => 'profile-website',
			'social-info' => 'social-info',
			'education-info' => 'education-info',
			'language-info' => 'language-info',
			'interest-info' => 'interest-info',
			'objective-info' => 'objective-info',
			'experience-info' => 'experience-info',
			'project-info' => 'project-info',
			'skill-info' => 'skill-info',
			'main-color' => 'main-color',
			'accent-color' => 'accent-color',
			'text-color-primary' => 'text-color-primary',
			'text-color-secondary' => 'text-color-secondary',
		);
		return $fields;
	}
	function getviewtemplate($path,$view_name){
		$target_file=APPPATH.'views/'.$path.$view_name.'.php';
		if(file_exists($target_file)) return $target_file;
	}
	function getfile($path,$file){
		$target_file = $path.$file;
		if(file_exists($target_file)) return $target_file;
	}
	
}