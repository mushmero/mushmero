<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Languagemodel extends CI_Model {
	var $language = 'language';
	var $settinglanguage = 'setting_language';

	function __construct(){
		parent::__construct();		
		$this->load->database();
	}

	public function getLanguageLevel(){
		$query = $this->db->from($this->settinglanguage)->order_by('id','ASC')->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	public function save($data){
		$query = $this->db->insert($this->language,$data);
		return $this->db->insert_id();
	}

	public function remove($id){
		$this->db->delete($this->language, array('id' => $id));
		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}

	public function getLanguage(){		
		$this->db->select('language.id, language.name, language.proficiency as proficiencyid, setting_language.proficiency');
		$this->db->from($this->language);
		$this->db->join($this->settinglanguage,'language.proficiency = setting_language.id','left');
		$query = $this->db->order_by('language.id','ASC')->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	public function getLanguageSingleData($id){
		$query = $this->db->get_where($this->language, array('id' => $id));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}
	public function updateLanguageById($id,$params){
		$this->db->set($params)->where('id',$id)->update($this->language);
		return $this->db->affected_rows();
	}
	public function countLanguage(){
		$this->db->from($this->language);
		return $this->db->count_all_results();
	}
}