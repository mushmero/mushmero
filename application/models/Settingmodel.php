<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Settingmodel extends CI_Model {
	var $social = 'setting_social';
	var $education = 'setting_education';
	var $language = 'setting_language';
	var $project = 'setting_project';
	var $activity = 'setting_activity';
	var $pdf = 'setting_pdf';

	function __construct(){
		parent::__construct();		
		$this->load->database();
	}
	public function saveSocial($data){
		$query = $this->db->insert($this->social,$data);
		return $this->db->insert_id();
	}
	public function getSocial(){
		$query = $this->db->from($this->social)->order_by('id','ASC')->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	public function removeSocial($id){
		$this->db->delete($this->social, array('id' => $id));
		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}
	public function getSocialSingleData($id){
		$query = $this->db->get_where($this->social, array('id' => $id));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}
	public function updateSocialById($id,$params){
		$this->db->set($params)->where('id',$id)->update($this->social);
		return $this->db->affected_rows();
	}
	/*education*/
	public function saveEducation($data){
		$query = $this->db->insert($this->education,$data);
		return $this->db->insert_id();
	}
	public function getEducation(){
		$query = $this->db->from($this->education)->order_by('id','ASC')->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	public function removeEducation($id){
		$this->db->delete($this->education, array('id' => $id));
		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}
	public function getEducationSingleData($id){
		$query = $this->db->get_where($this->education, array('id' => $id));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}
	public function updateEducationById($id,$params){
		$this->db->set($params)->where('id',$id)->update($this->education);
		return $this->db->affected_rows();
	}
	/* language */
	public function saveLanguage($data){
		$query = $this->db->insert($this->language,$data);
		return $this->db->insert_id();
	}
	
	public function getLanguage(){
		$query = $this->db->from($this->language)->order_by('id','ASC')->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	public function removeLanguage($id){
		$this->db->delete($this->language, array('id' => $id));
		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}
	public function getLanguageSingleData($id){
		$query = $this->db->get_where($this->language, array('id' => $id));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}
	public function updateLanguageById($id,$params){
		$this->db->set($params)->where('id',$id)->update($this->language);
		return $this->db->affected_rows();
	}
	/* project */	
	public function saveProject($data){
		$query = $this->db->insert($this->project,$data);
		return $this->db->insert_id();
	}
	public function getProject(){
		$query = $this->db->from($this->project)->order_by('id','ASC')->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	public function removeProject($id){
		$this->db->delete($this->project, array('id' => $id));
		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}
	public function getProjectSingleData($id){
		$query = $this->db->get_where($this->project, array('id' => $id));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}
	public function updateProjectById($id,$params){
		$this->db->set($params)->where('id',$id)->update($this->project);
		return $this->db->affected_rows();
	}
	/* todo */
	public function saveActivity($data){
		$query = $this->db->insert($this->activity,$data);
		return $this->db->insert_id();
	}
	public function getActivity(){
		$query = $this->db->from($this->activity)->order_by('id','ASC')->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	public function removeActivity($id){
		$this->db->delete($this->activity, array('id' => $id));
		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}
	public function getActivitySingleData($id){
		$query = $this->db->get_where($this->activity, array('id' => $id));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}
	public function updateActivityById($id,$params){
		$this->db->set($params)->where('id',$id)->update($this->activity);
		return $this->db->affected_rows();
	}
	public function getActiveActivity(){
		$this->db->select('*');
		$this->db->from($this->activity);
		$this->db->where('status != "Completed"');
		$query = $this->db->order_by('id','ASC')->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	public function allfields($table){
		$fields = $this->db->list_fields($table);
		return $fields;
	}
	public function savepdfsetting($data){
		$this->db->set($data)->update($this->pdf);
		return $this->db->affected_rows();
	}
	public function getpdfsetting(){
		$query = $this->db->from($this->pdf)->get();
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}
}