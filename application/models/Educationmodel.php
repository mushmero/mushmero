<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Educationmodel extends CI_Model {
	var $education = 'education';
	var $settingeducation = 'setting_education';

	function __construct(){
		parent::__construct();		
		$this->load->database();
	}

	public function getEducationLevel(){
		$query = $this->db->from($this->settingeducation)->order_by('id','ASC')->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	public function save($data){
		$query = $this->db->insert($this->education,$data);
		return $this->db->insert_id();
	}

	public function remove($id){
		$this->db->delete($this->education, array('id' => $id));
		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}

	public function getEducation(){		
		$this->db->select('education.id, education.name, education.institution, education.yearstart, education.yearend, education.level as levelid, setting_education.level');
		$this->db->from($this->education);
		$this->db->join($this->settingeducation,'education.level = setting_education.id','left');
		$query = $this->db->order_by('education.id','ASC')->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	public function getEducationSingleData($id){
		$query = $this->db->get_where($this->education, array('id' => $id));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}
	public function updateEducationById($id,$params){
		$this->db->set($params)->where('id',$id)->update($this->education);
		return $this->db->affected_rows();
	}

	public function countEducation(){
		$this->db->from($this->education);
		return $this->db->count_all_results();
	}
}