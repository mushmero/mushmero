<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Interestmodel extends CI_Model {
	var $interest = 'interest';

	function __construct(){
		parent::__construct();		
		$this->load->database();
	}

	public function save($data){
		$query = $this->db->insert($this->interest,$data);
		return $this->db->insert_id();
	}

	public function remove($id){
		$this->db->delete($this->interest, array('id' => $id));
		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}

	public function getInterest(){
		$query = $this->db->from($this->interest)->order_by('id','ASC')->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	public function getInterestSingleData($id){
		$query = $this->db->get_where($this->interest, array('id' => $id));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}
	public function updateInterestById($id,$params){
		$this->db->set($params)->where('id',$id)->update($this->interest);
		return $this->db->affected_rows();
	}

	public function countInterest(){
		$this->db->from($this->interest);
		return $this->db->count_all_results();
	}
}