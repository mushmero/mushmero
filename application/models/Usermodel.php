<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Usermodel extends CI_Model {
	var $profile = 'profile';
	var $attachment = 'attachment';

	function __construct(){
		parent::__construct();		
		$this->load->database();
	}

	public function save($data){
		$query = $this->db->insert($this->profile,$data);
		return $this->db->insert_id();
	}

	public function remove($id){
		$this->db->delete($this->profile, array('id' => $id));
		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}

	public function updateDataById($id,$params){
		$this->db->set($params)->where('id',$id)->update($this->profile);
		return $this->db->affected_rows();
	}

	public function getData(){		
		$this->db->select('profile.id, profile.fullname, profile.tagline, profile.email, profile.phone, profile.status, profile.website, attachment.altname, attachment.filename, attachment.filepath');
		$this->db->from($this->profile);
		$this->db->join($this->attachment,'profile.attachmentid = attachment.id','left');
		$this->db->where('attachment.tag = "profile"');
		$query = $this->db->order_by('profile.id','ASC')->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	public function getActiveProfile(){
		$this->db->select('profile.id, profile.fullname, profile.tagline, profile.email, profile.phone, profile.status, profile.website, attachment.altname, attachment.filename, attachment.filepath');
		$this->db->from($this->profile);
		$this->db->join($this->attachment,'profile.attachmentid = attachment.id','left');
		$this->db->where('profile.status = 1 and attachment.tag = "profile"');
		$query = $this->db->order_by('profile.id','ASC')->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}
	}

	public function getStatusById($id){
		$this->db->select('status');
		$this->db->from($this->profile);
		$this->db->where(array('id' => $id));
		$query = $this->db->get();
		return $query->row();
	}

	public function countActiveStatus(){
		$this->db->from($this->profile);
		$this->db->where(array('status' => 1));
		return $this->db->count_all_results();
	}
}