<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Visitormodel extends CI_Model {
	var $visitor = 'visitor';

	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function save($data){
		$query = $this->db->insert($this->visitor,$data);
		return $this->db->insert_id();
	}

	public function countAll(){
		$this->db->from($this->visitor);
		return $this->db->count_all_results();
	}

	public function getVsitiorByCountry(){
		$this->db->select('count(*) as visitor, country, countrycode, MIN(latitude) as latitude, MIN(longitude) as longitude');
		$this->db->from($this->visitor);
		$this->db->where("country != '' and countrycode != '' and latitude != '' and longitude != ''");
		$query = $this->db->group_by('countrycode')->get();
		return $query->result();
	}
}