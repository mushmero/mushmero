<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Experiencemodel extends CI_Model {
	var $experience = 'experience';

	function __construct(){
		parent::__construct();		
		$this->load->database();
	}

	public function save($data){
		$query = $this->db->insert($this->experience,$data);
		return $this->db->insert_id();
	}

	public function remove($id){
		$this->db->delete($this->experience, array('id' => $id));
		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}

	public function getExperience(){
		$query = $this->db->from($this->experience)->order_by("str_to_date(startdate,'%m/%Y') DESC")->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	public function getExperienceSingleData($id){
		$query = $this->db->get_where($this->experience, array('id' => $id));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}
	public function updateExperienceById($id,$params){
		$this->db->set($params)->where('id',$id)->update($this->experience);
		return $this->db->affected_rows();
	}

	public function countExperience(){
		$this->db->from($this->experience);
		return $this->db->count_all_results();
	}

	public function getMinYear(){
		$this->db->select('min(str_to_date(startdate,"%m/%Y")) as mindate');
		$query = $this->db->get($this->experience);
		if($query->num_rows() > 0){
			return $query->row();
		}
	}

	public function countEmployer(){
		$this->db->select('employer');
		$this->db->distinct();
		return $this->db->count_all_results($this->experience,FALSE);
	}
}