<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Announcementmodel extends CI_Model {
	var $announcement = 'announcement';

	function __construct(){
		parent::__construct();		
		$this->load->database();
	}

	public function save($data){
		$query = $this->db->insert($this->announcement,$data);
		return $this->db->insert_id();
	}

	public function getActiveInfo(){
		$query = $this->db->from($this->announcement)->where('status = "active"')->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		} else {
			return false;
		}
	}

	public function updateExistingInfo($id,$params){
		$this->db->set($params)->where('id',$id)->update($this->announcement);
		return $this->db->affected_rows();
	}

	public function countInfo(){
		$this->db->from($this->announcement);
		return $this->db->count_all_results();
	}
}