<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Attachmentmodel extends CI_Model {
	var $attachment = 'attachment';

	function __construct(){
		parent::__construct();		
		$this->load->database();
	}

	public function save($data){
		$query = $this->db->insert($this->attachment,$data);
		return $this->db->insert_id();
	}

	public function getbackground(){
		$this->db->select('*');
		$this->db->from($this->attachment);
		$this->db->where('tag = "bg"');
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result();
		}
	}
}