<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Templatemodel extends CI_Model {
	var $template = 'template';

	function __construct(){
		parent::__construct();		
		$this->load->database();
	}

	public function save($data){
		$query = $this->db->insert($this->template,$data);
		return $this->db->insert_id();
	}

	public function updateExistingTemplate($id,$params){
		$this->db->set($params)->where('id',$id)->update($this->template);
		return $this->db->affected_rows();
	}
	public function getActiveTemplate(){
		$query = $this->db->from($this->template)->where('status = "active"')->get();
		return $query->row();
	}
	public function getActiveTemplateById($id){
		$query = $this->db->from($this->template)->where(array('status' => 'active', 'id' => $id))->get();
		return $query->row();
	}
	public function getAllTemplate(){
		$query = $this->db->from($this->template)->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	public function remove($id){		
		$this->db->delete($this->template, array('id' => $id));
		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}
	public function getTemplate($id){
		$this->db->select('id, name, design, status, maincolor, accentcolor');
		$this->db->from($this->template);
		$this->db->where(array('id' => $id));
		$query = $this->db->get();
		return $query->row();
	}
	public function update($id,$params){
		$this->db->set($params)->where('id',$id)->update($this->template);
		return $this->db->affected_rows();
	}
}