<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Skillmodel extends CI_Model {
	var $skill = 'skill';

	function __construct(){
		parent::__construct();		
		$this->load->database();
	}

	public function save($data){
		$query = $this->db->insert($this->skill,$data);
		return $this->db->insert_id();
	}

	public function remove($id){
		$this->db->delete($this->skill, array('id' => $id));
		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}

	public function getSkill(){
		$query = $this->db->from($this->skill)->order_by('id','ASC')->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	public function getSkillSingleData($id){
		$query = $this->db->get_where($this->skill, array('id' => $id));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}
	public function updateSkillById($id,$params){
		$this->db->set($params)->where('id',$id)->update($this->skill);
		return $this->db->affected_rows();
	}
	public function countSkill(){
		$this->db->from($this->skill);
		return $this->db->count_all_results();
	}
}