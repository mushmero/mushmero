<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Socialmodel extends CI_Model {
	var $social = 'social';
	var $settingsocial = 'setting_social';

	function __construct(){
		parent::__construct();		
		$this->load->database();
	}

	public function getSocialIcon(){
		$query = $this->db->from($this->settingsocial)->order_by('id','ASC')->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	public function save($data){
		$query = $this->db->insert($this->social,$data);
		return $this->db->insert_id();
	}

	public function remove($id){
		$this->db->delete($this->social, array('id' => $id));
		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}

	public function getSocial(){		
		$this->db->select('social.id, social.name, social.link, social.icon, setting_social.iconname, setting_social.icontype');
		$this->db->from($this->social);
		$this->db->join($this->settingsocial,'social.icon = setting_social.id','left');
		$query = $this->db->order_by('social.id','ASC')->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	public function getSocialSingleData($id){
		$query = $this->db->get_where($this->social, array('id' => $id));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}
	public function updateSocialById($id,$params){
		$this->db->set($params)->where('id',$id)->update($this->social);
		return $this->db->affected_rows();
	}
	public function countSocial(){
		$this->db->from($this->social);
		return $this->db->count_all_results();
	}
}