<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Objectivemodel extends CI_Model {
	var $objective = 'objective';

	function __construct(){
		parent::__construct();		
		$this->load->database();
	}

	public function save($data){
		$query = $this->db->insert($this->objective,$data);
		return $this->db->insert_id();
	}

	public function getActiveObjective(){
		$query = $this->db->from($this->objective)->where('status = "active"')->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		} else {
			return false;
		}
	}

	public function updateExistingObjective($id,$params){
		$this->db->set($params)->where('id',$id)->update($this->objective);
		return $this->db->affected_rows();
	}

	public function countObjective(){
		$this->db->from($this->objective);
		return $this->db->count_all_results();
	}
}