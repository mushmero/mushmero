<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Projectmodel extends CI_Model {
	var $project = 'project';
	var $settingproject = 'setting_project';

	function __construct(){
		parent::__construct();		
		$this->load->database();
	}

	public function getProjectStatus(){
		$query = $this->db->from($this->settingproject)->order_by('id','ASC')->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	public function save($data){
		$query = $this->db->insert($this->project,$data);
		return $this->db->insert_id();
	}

	public function remove($id){
		$this->db->delete($this->project, array('id' => $id));
		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}

	public function getProject(){		
		$this->db->select('project.id, project.name, project.status as statusid, project.description, setting_project.status');
		$this->db->from($this->project);
		$this->db->join($this->settingproject,'project.status = setting_project.id','left');
		$query = $this->db->order_by('project.id','ASC')->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	public function getProjectSingleData($id){
		$query = $this->db->get_where($this->project, array('id' => $id));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}
	public function updateProjectById($id,$params){
		$this->db->set($params)->where('id',$id)->update($this->project);
		return $this->db->affected_rows();
	}

	public function countProject(){
		$this->db->from($this->project);
		return $this->db->count_all_results();
	}
}